<?php

// Script fuellen mit Daten aus Inventar Empirum-DB nach Tabelle parameter in invDB
// schachi 2017-11-06 V1.0 - begin

  // requirements: php7.0-sybase
  $dbmy = mysqli_connect('localhost', 'debian-sys-maint', 'XvzleVoMaBMmdHJm', 'mpidb_it_inv');
  if ( !$dbmy ) trigger_error ('Failed to connect to MySQL database: '.mysqli_connect_error($dbmy)."\n");

  $hostname = "192.168.20.87";
  $port = 1433;
  $dbname = "empDatabase";
  $username = "mpidb";
  $pw = "d1CVl4ir";
  $debug = 0;
  $verbose = 0;

  try {
    $dbms = new PDO ("dblib:host=$hostname:$port;dbname=$dbname","$username","$pw");
  }
  catch (PDOException $pe) {
    trigger_error ("Failed to get DB handle：" . $pe->getMessage() . "\n");
  }
  $sqlpc = "SELECT Computername FROM dbo.InvComputer";
  // test nur einen pc importieren
  //$sqlpc = "SELECT Computername FROM dbo.InvComputer WHERE Computername = 'empirum'";
  // Prepare-Statement - wird spaeter mit Computer ergaenzt
  $sqlsw = <<<EOT
    SELECT
      cli.client_id AS id,
      ISNULL(cli.domain,'--') AS domain,
      ISNULL(inv.OperatingSystem,'--') AS os,
      inv.BitVersion AS "bit",
      inv.InvDate AS invDate,
      ISNULL(inv.DisplayAdapter,'--') AS display,
      inv.IPAddress AS ip,
      inv.MACAddress AS mac,
      inv.Memory as mem,
      inv.ProcessorName AS cpu,
      ISNULL(inv.OSLogin,'--') AS "login",
      ISNULL(wmic.Manufacturer,'--') AS made,
      ISNULL(wmic.Model,'--') AS model,
      wmib.SerialNumber AS serial
    FROM
      clients AS cli
    LEFT JOIN InvComputer AS inv ON cli.client_id = inv.client_id
	LEFT JOIN WMIComputersystem AS wmic ON cli.client_id = wmic.client_id
	LEFT JOIN WMIBios AS wmib ON cli.client_id = wmib.client_id
    WHERE
      cli.name = :computer;
EOT;

  $stpc = $dbms->query($sqlpc);
  $rowpc = $stpc->fetchAll(PDO::FETCH_ASSOC);
  $cntpc = count($rowpc);
  if ($verbose) echo "Anzahl Computer: $cntpc\n";
  if ( $cntpc > 0 ) {
    //print_r($rowpc);   //[643] => Array ( [Computername] => WIN7-TEST1 )

    $sonder = array(" ");
    // hole Systemdaten fuer jeden Computer
    foreach ($rowpc as $keypc => $column) {
      $comp = strtolower(preg_replace('/\s\s+/', '', $column['Computername']));
      if ($debug) echo $comp.' '.$keypc."\n";
      $stsw = $dbms->prepare($sqlsw);
      $stsw->execute(array(':computer' => $comp));

      // get tabID from device table invDB
      $result = mysqli_query($dbmy, "SELECT tabID FROM `mpi_geraete` WHERE name = '$comp' OR alias = '$comp' LIMIT 1") OR trigger_error ('Query tabID from device name failed: '.mysqli_error($dbmy)."\n");
      $cnt1 = mysqli_num_rows($result);
      if ( $cnt1 > 0 ) {
        $rowid = mysqli_fetch_assoc($result);
        $tabid = $rowid['tabID'];
        if ($debug) echo "Computer exists on invDB with tabID = $tabid\n";

        // get system data from empirumDB
        $rowsw = $stsw->fetch(PDO::FETCH_ASSOC);
        $cliid = $rowsw['id'];
        // filed 'note' must have ever same value OR NULL, otherwise it will be insert instead update
        $system = array (
          array("dom",  "Domain", str_replace($sonder, "", $rowsw['domain']), NULL, NULL),
          array("os",   "Betriebssytem", $rowsw['os'], NULL, NULL),
          array("date", "Empirum-Inv.", substr($rowsw['invDate'],0,10), "Datum", NULL),
          array("disp", "Grafik", $rowsw['display'], NULL, NULL),
          array("mem",  "RAM Speicher", $rowsw['mem'], "Byte", NULL),
          array("cpu",  "CPU Typ", $rowsw['cpu'], NULL, NULL),
          array("last", "Last Login", $rowsw['login'], "User", NULL),
          array("made", "Hersteller", $rowsw['made'], NULL, NULL),
          array("type", "Modell", $rowsw['model'], NULL, NULL),
          array("key",  "ServiceTag", $rowsw['serial'], NULL, NULL)
        );

        // get all disk's per computer
        $sthd = $dbms->query("SELECT Model, InterfaceType, DiskSize, SerialNumber FROM dbo.WMIDiskDrive WHERE client_id = '$cliid'");
        $rowhd = $sthd->fetchAll(PDO::FETCH_ASSOC);
        $cnthd = count($rowhd);
        if ($debug) echo "Anzahl Festplatten: $cnthd\n";
        if ( $cnthd > 0 ) {
          //print_r($rowhd);
          foreach ($rowhd as $keyhd => $col) {
            array_push($system, array("hd".$keyhd, "Speichermodell", "hd".$keyhd.": ".$col['Model'], $col['InterfaceType'], $col['SerialNumber']));
            array_push($system, array("hd".$keyhd, "Speichermedium", "hd".$keyhd.": ".round($col['DiskSize']/1000), "GB", "HD".$keyhd));
          }
        }
        unset($sthd);

        // get all network card's per computer
        $stnw = $dbms->query("SELECT Description as model, (CASE EnableDHCP WHEN '1' THEN DHCPIPAddress WHEN '0' THEN IPAddress END) AS ip, MACAddress AS mac FROM dbo.InvNetworkCards WHERE client_id = '$cliid'");
        $rownw = $stnw->fetchAll(PDO::FETCH_ASSOC);
        $cntnw = count($rownw);
        if ($debug) echo "Anzahl Netzwerkkarten: $cntnw\n";
        if ( $cntnw > 0 ) {
          //print_r($rownw);
          foreach ($rownw as $keynw => $col) {
            array_push($system, array("nw".$keynw, "NW-Model", "eth".$keynw.": ".$col['model'], "ETH", "NW".$keynw));
            array_push($system, array("nw".$keynw, "IP-Adresse", "eth".$keynw.": ".$col['ip'], "IP-V4", "NW".$keynw));
            array_push($system, array("nw".$keynw, "Mac-Adresse", "eth".$keynw.": ".substr($col['mac'],0,2).':'.substr($col['mac'],2,2).':'.substr($col['mac'],4,2).':'.substr($col['mac'],6,2).':'.substr($col['mac'],8,2).':'.substr($col['mac'],10,2), "MAC", "NW".$keynw));
          }
        }
        unset($stnw);

        //print_r ($system);

        // insert parameter in invDB
        foreach ($system as $sys => $param) {
          $typ     = $param[1];
          $paramet = utf8_encode(substr($param[2],0,50));
          $einheit = $param[3];
          $note    = $param[4];

          // check if typ exists, otherwise create
          $sqlsel = "SELECT typID FROM `list_parTyp` WHERE typ = '$typ'";
          $result = mysqli_query($dbmy, $sqlsel) OR trigger_error ('Query typID from list type failed: '.mysqli_error($dbmy)."\n");
          //print_r ($result);
          $cnt2 = mysqli_num_rows($result);
          if ( $cnt2 == 0 ) {
            if ($verbose) echo "Datensatz typID '$param[1]' not exists, create as first\n";
            mysqli_query($dbmy, "INSERT INTO `list_parTyp` (typ) VALUES ('$typ')") OR trigger_error ('Query insert typ failed: '.mysqli_error($dbmy)."\n");
            $result = mysqli_query($dbmy, $sqlsel) OR trigger_error ('Query typID from list type failed: '.mysqli_error($dbmy)."\n");
          }
          $rowid = mysqli_fetch_assoc($result);
          $typid = $rowid['typID'];

          // check if einheit exists and not null, otherwise create
          if ( $einheit != NULL ) {
            $sqlsel = "SELECT einheit FROM `list_einheit` WHERE einheit = '$einheit'";
            $result = mysqli_query($dbmy, $sqlsel) OR trigger_error ('Query einheit from list einheit failed: '.mysqli_error($dbmy)."\n");
            //print_r ($result);
            $cnt2 = mysqli_num_rows($result);
            If ( $cnt2 == 0 ) {
              if ($verbose) echo "Datensatz einheit '$param[3]' not exists, create as first\n";
              mysqli_query($dbmy, "INSERT INTO `list_einheit` (einheit) VALUES ('$einheit')") OR trigger_error ('Query insert einheit failed: '.mysqli_error($dbmy)."\n");
            }
            $sqlEinheit = "einheit = '$einheit'";
            $einheit = "'$einheit'";
          } else {
            $sqlEinheit = 'einheit IS NULL';
            $einheit = 'NULL';
          }
          if ( $note != NULL ) { 
            $sqlNote = "notiz = '$note'";
            $note = "'$note'";
          } else { 
            $sqlNote = "notiz IS NULL";
	    $note = 'NULL';
          }
          if ($debug) echo "cnt = $sys :: cliID = $cliid :: tabID = $tabid :: typ = '$typ' :: typID = $typid :: param = '$paramet' :: einheit = $einheit :: note = $note\n";
          // insert/update parameter
          $sqlsel = "SELECT parID, parameter FROM `tab_parameter` WHERE auswTab = 'gerID' AND tabID = '$tabid' AND typID = '$typid' AND $sqlEinheit AND $sqlNote AND bearbeiter = 'empirum'";
          //echo "$sqlsel\n";
          $result = mysqli_query($dbmy, $sqlsel) OR trigger_error ('Query parameter from table parameter failed: '.mysqli_error($dbmy)."\n");
          //print_r ($result);
          $cnt2 = mysqli_num_rows($result);
          if ( $cnt2 == 0 ) {
            $sqlins = "INSERT INTO `tab_parameter`(auswTab,tabID,typID,parameter,einheit,notiz,bearbeiter) VALUES ('gerID',$tabid,$typid,'$paramet',$einheit,$note,'empirum')";
            if ($debug) echo $sqlins."\n"; 
            mysqli_query($dbmy, $sqlins) OR trigger_error ('Insert parameter data failed: '.mysqli_error($dbmy)."\n");
            if ($debug) echo "Insert parameter '$paramet' to tabID '$tabid' in invDB\n";
          }
          elseif ( $cnt2 == 1 ) {
            $rowid = mysqli_fetch_assoc($result);
            $parLast = $rowid['parameter'];
            if ( $parLast != $paramet ) {
              $parID = $rowid['parID'];
              $sqlupd = "UPDATE `tab_parameter` SET parameter = '$paramet' WHERE parID = $parID AND parameter != '$paramet'";
              mysqli_query($dbmy, $sqlupd) OR trigger_error ('Update parameter failed: '.mysqli_error($dbmy)."\n");
              if ($debug) echo "Update parameter '$paramet' to parID '$parID' in invDB\n";
            } else {
              if ($debug) echo "Original and new parameter '$paramet' are identical, nothing to do.\n";
            }
          } else {
            trigger_error ("WARNING: Return count is $cnt2, expected value 0 or 1. Should not happend here!\n");
          }
        }
      } else {
        if ($verbose) echo "Computer '$comp' not exists on invDB, create as first by itself\n";
      }
    }
    unset($stsw);

    // delete parameter on invDB where no more exists on empDB
    $sqlsel = "SELECT tabID FROM `tab_parameter` WHERE auswTab = 'gerID' AND bearbeiter = 'empirum' GROUP BY tabID";
    $result = mysqli_query($dbmy, $sqlsel) OR trigger_error ('Query tabID from table parameter failed: '.mysqli_error($dbmy)."\n");
    $cntex = mysqli_num_rows($result);
    if ($verbose) echo "Anzahl Computer in EmpDB: $cntpc \n";
    if ($verbose) echo "Anzahl Computer in InvDB: $cntex \n";
    $diff = $cntex - $cntpc; 
    if ( $cntex >= 1 AND ($diff < 100 OR $debug = 1)) {
      while($row = mysqli_fetch_assoc($result)) {
        $tabid = $row['tabID'];
        $sqlsel = "SELECT name, alias FROM `mpi_geraete` WHERE tabID = $tabid";
        $res2 = mysqli_query($dbmy, $sqlsel) OR trigger_error ('Query nameD from table geraete failed: '.mysqli_error($dbmy)."\n");
        $row2 = mysqli_fetch_assoc($res2);
        $name = $row2['name'];
        $alias = $row2['alias'];
        if ($alias == NULL OR $alias == '') $alias = 'noAlias!';

        // check existing computer in invDB against empDB
        $stex = $dbms->query("SELECT client_id FROM dbo.InvComputer WHERE Computername LIKE '$name' OR Computername LIKE '$alias'");
        $rowex = $stex->fetchAll(PDO::FETCH_ASSOC);
        $cntex = count($rowex);
        if ( $cntex == 0 ) {
          $cliid = NULL;
          if ($verbose) echo "Computer $name no more exist in empirum, delete old parameter if exists!\n";
          $sqldel = "DELETE FROM `tab_parameter` WHERE tabID = '$tabid' AND auswTab = 'gerID' AND bearbeiter = 'empirum'";
          mysqli_query($dbmy, $sqldel) OR trigger_error ('Delete parameter for computer '.$name.' failed: '.mysqli_error($dbmy)."\n");
          if ($debug) echo "cnt = $cntex :: tabID = $tabid :: name = $name :: tabID = $tabid\n";
        //} else {
        //  $cliid = $rowex[0]['client_id'];
        }
        unset($stex);
      }
    } else {
      trigger_error ("WARNING: Anzahl der zu loeschenden Datensaetze ($diff) uebersteigt ein gesundes Mass!\n");
    }
  }

  unset($stpc);
  unset($dbms);
  mysqli_close($dbmy);

?>
