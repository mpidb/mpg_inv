<?php

class tables_mpi_gerWartung { 

  // setze Farbe in listenansicht entsprechend Wartung oder Pruefung
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    if (($record->val('status') == '0') OR ($record->strval('intervall') == '--')) return $table.'sand';
    // 1 Monat vorher Status auf Gelb 
    $hysDate = date('Y-m-d',mktime(0,0,0,date("m")+1,date("d"),date("Y")));
    $curDate = date('Y-m-d');
    $wartDate = $record->strval('naechste');
    if (($record->val('status') == '1') AND ($wartDate != NULL)) {
      if     ($wartDate <  $curDate) return $table.'rot';
      elseif ($wartDate >  $hysDate) return $table.'gruen';
      else return $table.'gelb';
    }
    return $table.'sand';
  }

  function block__after_result_list_content() {
    echo 'Wartungstermin';
    echo '<span style="background-color:#cfc;"> ist noch nicht erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> läuft in ein paar Tagen ab. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#fcc;"> ist überschritten. </span>&nbsp;|&nbsp;';
    echo '<span> ist deaktiviert. </span>&nbsp;';
  }

  // Anzeige des Feldes im "GlanceView" bei Relationship
  function getTitle(&$record) {
    return $record->strval('type').' : '.$record->display('intervall').' : '.$record->display('naechste');
  }

  function gerID__renderCell(Dataface_Record $record ) {
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('gerID');
    $name   = $record->display('gerID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function letzte__display(&$record) {
    if ($record->val('letzte') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('letzte'))); 
  }

  function naechste__display(&$record) {
    if ($record->val('naechste') == NULL) return; 
    return date('d.m.Y', strtotime($record->strval('naechste'))); 
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    if (($record->val('status') == '1') AND ($record->val('letzte') == NULL))
    return Dataface_Error::permissionDenied('Eine Aktivierung ohne Datum macht kein Sinn!');

    // setze NextDate entsprechend Intervall
    $addD = 0;
    $addM = 0;
    $addY = 0;
    if (($record->val('status') == '1') AND ($record->strval('intervall') != '00') AND ($record->val('letzte') != NULL)) {
      // liste in valuelists.ini, nicht in DB!!!
      $interv = $record->val('intervall');
      switch ($interv) {
        case '01'; #einmalig (letzte = naechste)
          break;
        case '02'; #woechentlich
          $addD = 7;
          break;
        case '03'; #alle 2 Wochen
          $addD = 14;
          break;
        case '04'; #alle 3 Wochen
          $addD = 21;
          break;
        case '05'; #monatlich
          $addM = 1;
          break;
        case '06'; #alle 2 Monate
          $addM = 2;
          break;
        case '07'; #vierteljaehrlich
          $addM = 3;
          break;
        case '08'; #alle 4 Monate
          $addM = 4;
          break;
        case '09'; #alle 5 Monate
          $addM = 5;
          break;
        case '10'; #halbjaehrlich
          $addM = 6;
          break;
        case '11'; #jaehrlich
          $addY = 1;
          break;
        case '12'; #alle 2 Jahre
          $addY = 2;
          break;
        case '13'; #alle 3 Jahre
          $addY = 3;
          break;
       case '14'; #alle 5 Jahre
          $addY = 5;
          break;
        case '15'; #alle 6 Jahre
          $addY = 6;
          break;
        case '16'; #alle 10 Jahre
          $addY = 10;
          break;
        default;
          return Dataface_Error::permissionDenied('Intervall "'.$interv.'" nicht implementiert, bitte DB-Admin kontaktieren!!');
          break;
      }
      $prevDate = $record->strval('letzte');
      $nextDate = date('Y-m-d',mktime(0,0,0,date("m",strtotime($prevDate))+$addM,date("d",strtotime($prevDate))+$addD,date("Y",strtotime($prevDate))+$addY));
      $record->setValue('naechste', $nextDate);
      //return Dataface_Error::permissionDenied('-'.$prevDate.'-'.$addD.$addM.$addY.'-'.$interv.'-'.$nextDate.'-');
    } else {
      $record->setValue('naechste', NULL);
    }
  }


}
?>
