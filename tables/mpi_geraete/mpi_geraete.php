<?php

class tables_mpi_geraete { 

  function block__before_fineprint() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $mailto = $app->_conf['_own']['mailto'];
    $mname  = $app->_conf['_own']['mailname'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep == $db_ver )
      echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> = <span style="color: green">DB: '.$db_ver.'</span> &copy;<a href="mailto:'.$mailto.'">'.$mname.'</a>';
  }

  function block__before_body() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep != $db_ver ) {
      if ( $fs_rep > $db_ver ) {
        echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> &ne; <span style="color: red">DB: '.$db_ver.'</span>';
      } else {
        echo 'Version <span style="color: red">FS: '.$fs_ver.'</span> &ne; <span style="color: green">DB: '.$db_ver.'</span>';
      }
    }
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  function rel_lizenz__permissions($record) {
    return array(
      'update related records' => 0,
      //'add new related record' => 0,
      //'add existing related record' => 0,
      'remove related record' => 0,
      'delete related record' => 0,
      'edit related record' => 0
    );
  }

  function block__before_related_lizenz_records_list() {
    $record =& Dataface_Application::getInstance()->getRecord();
    $host  = gethostname();
    $app    = Dataface_Application::getInstance();
    $path   = $app->_conf['_own']['licman_path'];
    $tabID = $record->val('tabID');
    $urlAddI = $site_href."/$path/index.php?-action=new&-table=mpi_install&instID=$tabID";
    $urlAddS = $site_href."/$path/index.php?-action=new&-table=con_softInv";
    $urlModI = $site_href."/$path/index.php?-action=list&-table=mpi_install&instID=$tabID";
    //echo '<div id="relatedActionsWrapper" class="contentActions">';
    //echo  '<ul id="relatedActions">';
	//echo   '<li id="addNew"><a id="add_new_related_record" href='.$url.'> Neuen Lizenz Datensatz hinzufügen </a></li>';
	echo   '<a class="contentActions" href='.$urlAddI.'><img src="/xataface/images/add_icon.gif" alt="" width="17" height="17"> Manuelle Installation hinzufügen </a>';
	echo   '<a class="contentActions" href='.$urlAddS.'><img src="/xataface/images/view.gif" alt="" width="16" height="16"> Automatische Installation hinzufügen </a>';
    echo   '<a class="contentActions" href='.$urlModI.'><img src="/xataface/images/recycle.gif" alt="" width="14" height="14"> Manuelle Installation entfernen </a>';
	//echo  '</ul>';
    //echo '</div>';
  }   

  function getTitle(Dataface_Record $record) {
    $name = $record->strval('name');
    if (strlen($name) > 15) $name = substr($name,0, 15)."...";
    if ($record->strval('seriennummer') == NULL) return $name;
    return $name.' : '.$record->strval('seriennummer');
  }

  // Anzeige des Feldes "Add related ... record" bei Relationsship
  function titleColumn() {
    return "IF(seriennummer IS NULL,name,CONCAT(name,' : ',seriennummer))";
  }

  // Preis bei ReadOnly verstecken
  function preis__permissions(&$record) {
    $user = Dataface_AuthenticationTool::getInstance()->getLoggedInUser();
    if ( !$user ) return Dataface_PermissionsTool::NO_ACCESS();
    $role = $user->val('role');
    if ( $role == 'READ ONLY' ) { return Dataface_PermissionsTool::NO_ACCESS(); }
  }

  function inventar__rendercell(&$record) {
    $table  = 'list_inventar';
    $action = 'browse';
    $tabID  = $record->strval('inventar');
    if ($tabID == NULL) return;
    $tabID2 = $record->strval('unterNr');
    $name   = $record->strval('inventar').':'.$record->display('unterNr');
    $sql    = "SELECT anlage FROM list_inventar WHERE anlage = '$tabID' AND unterNr = '$tabID2'";
    list($anlage) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (isset($anlage)) {
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&anlage=$tabID&unterNr=$tabID2";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    if ($record->strval('status') != 'verschrottet') return '<font color="red">'.$name.'</font>';
    return '<font color="gray">'.$name.'</font>';
  }

  function bestellnummer__rendercell(Dataface_Record $record) {
    $name   = $record->strval('bestellnummer');
    if ($record->strval('bestellung.bestellnummer') != NULL) {
      $table  = 'view_licman_lizenz';
      $action = 'list';
      $field  = 'bestellnummer';
      $tabID  = $record->strval($field);
      $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    return $name;
  }

  function block__after_preis_widget() { echo '&euro;'; }

  function lieferdatum__display(&$record) {
    if ($record->strval('lieferdatum') == NULL) return; 
    return date('d.m.Y', strtotime($record->strval('lieferdatum'))); 
  }

  function entsorgt__display(&$record) {
    if ($record->strval('entsorgt') == NULL) return; 
    return date('d.m.Y', strtotime($record->strval('entsorgt'))); 
  }

  function garantie__display(&$record) {
    if ($record->strval('garantie') == NULL) return; 
    return date('d.m.Y', strtotime($record->strval('garantie'))); 
  }

  function verleih__display(&$record) {
    if ($record->val('verleih') == '1') return 'ja'; else return 'nein';
  }

  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // 0 per default geht nicht in xataface, workaround
    if (($record->strval('inventar') != NULL) AND ($record->strval('unterNr') == NULL)) {
      $record->setValue('unterNr', '0');
    }

    // kopiere Protokollvorlage und loesche altes Protokoll wenn neuer Wert ungleich alter Wert
    $tabID   = $record->val('tabID');
    $protNew = $record->val('protID');
    // verhindere Selbstausloeschung von Vorlagen
    $sql = "SELECT gerID FROM list_protokoll WHERE gerID = '$tabID'";
    list($templ) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if ((@$templ) AND ($protNew != NULL)) {
     return Dataface_Error::permissionDenied('Host ist selbst Vorlage und darf daher nicht verändert werden!');
    }
    $sql = "SELECT protID FROM mpi_geraete WHERE tabID = '$tabID'";
    list($protLast) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if ($protNew != @$protLast) {
      $sql = "SELECT gerID FROM list_protokoll WHERE protID = '$protNew'";
      list($tmplHostID) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      $sql = "SELECT name FROM mpi_geraete WHERE tabID = '$tmplHostID'";
      list($tmplHost) = xf_db_fetch_row(xf_db_query($sql, df_db()));
      $sql = "DELETE FROM tab_instProt WHERE gerID = '$tabID' AND protID = '$protNew'";
      $res = xf_db_query($sql, df_db());
      error_log ('Res:'.$res.' New:'.$protNew.'-Old:'.$protLast.'-'.$sql, 0);
      $sql = "INSERT INTO tab_instProt (gerID, protID, schritt, bereich, sequenz, bearbeiter) SELECT $tabID AS gerID, $protNew AS protID, schritt, bereich, sequenz, '$tmplHost' AS bearbeiter FROM tab_instProt WHERE protID = '$protNew' AND gerID = '$tmplHostID' ORDER BY protID, schritt, bereich, sequenz";
      $res = xf_db_query($sql, df_db());
      //error_log ('Res:'.$res.' New:'.$protNew.'-Old:'.$protLast.'-'.$sql, 0);
    }

  }

/* nicht mehr unterstuezt
  function __import__csv(&$data, $defaultValues=array()) {
    // build an array of Dataface_Record objects that are to be inserted based
    // on the CSV file data.
    $records = array();
    // first split the CSV file into an array of rows.
    $rows = explode("\n", $data);
    // wenn Kopfzeile vorhanden
    // array_shift($rows); 
    foreach ( $rows as $row ) {
      // bug leere Zeile am Ende
      if ( !trim($row) ) continue; 
      // We iterate through the rows and parse the fields to that they can be stored in a Dataface_Record object.
      list($tabID, $name, $art, $projekt, $typ, $hersteller, $seriennummer, $inventar, $benutzer, $lagerort, $lieferant, $bestellnummer, $lieferdatum, $preis, $eigentum, $forschungsbereich, $status, $bemerkung, $speichergroesse, $festplatte, $taktfrequenz, $prozessor, $netzwerk) = explode(',', $row);
      $record = new Dataface_Record('mpi_geraete', array());
        
      // We insert the default values for the record.
      $record->setValues($defaultValues);
        
      // Now we add the values from the CSV file.
      $record->setValues(array(
        'tabID'=>$tabID,
        'name'=>$name,
        'art'=>$art,
        'projekt'=>$projekt,
        'typ'=>$typ,
        'hersteller'=>$hersteller,
        'seriennummer'=>$seriennummer,
        'inventar'=>$inventar,
        'benutzer'=>$benutzer,
        'lagerort'=>$lagerort,
        'lieferant'=>$lieferant,
        'bestellnummer'=>$bestellnummer,
        'lieferdatum'=>$lieferdatum,
        'preis'=>$preis,
        'eigentum'=>$eigentum,
        'forschungsbereich'=>$forschungsbereich,
        'status'=>$status,
        'bemerkung'=>$bemerkung,
        'speichergroesse'=>$speichergroesse,
        'festplatte'=>$festplatte,
        'taktfrequenz'=>$taktfrequenz,
        'prozessor'=>$prozessor,
        'netzwerk'=>$netzwerk,
      ));
      // Now add the record to the output array.
      $records[] = $record;
    }
    // Now we return the array of records to be imported.
    return $records;
  }
*/

}
?>
