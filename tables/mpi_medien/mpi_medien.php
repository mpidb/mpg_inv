<?php

class tables_mpi_medien { 

  function block__after_new_record_form() {
    echo <<<END
    <script language="javascript"><!--
    var typ_field = document.getElementById('typ');
    var name_field = document.getElementById('name');
END;
    // Let's get all types available.
    $app =& Dataface_Application::getInstance();
    $query =& $app->getQuery();
    $table =& Dataface_Table::loadTable($query['-table']);

    $valName = $table->getValuelist('name');
    // A JSON encoder to allow us to easily convert PHP arrays to javascript arrays.
    import('Services/JSON.php');
    $json = new Services_JSON();
    echo '
      var name_options = '.$json->encode($valName).';
      var typ_options = '.$json->encode($valName).';
    ';
    echo <<<END
    typ_field.onchange = function() {
	  var selName = typ_field.options[typ_field.selectedIndex].text;
      var index = '1';
      var pad = '0000';
      var res = "";
      for ( name_id in name_options) {
        res = name_options[name_id].split("-",2);
        if (res[0] == selName) {
          if (index != Number(res[1])) { break; } 
          index++;
        }
        //alert (selName + ' - ' + res[1] + ' - ' + index);
      } 
      var cnt = (pad + index).slice(-pad.length);
      name_field.value = selName + '-' + cnt;
    };
    name_field.onchange = function() {
      var res = name_field.value.split("-",2);
      //typ_field.options[typ_field.selectedIndex].text = (res[0]);
      index = '0';
      for ( typ_id in typ_options) { index++ };
      typ_field.options[index++] = new Option(res[0], res[0]);
      typ_field.value = res[0];
      //alert(index + ' - ' + name_field.value + ' - ' + res[0]);
    };

    //--></script>
END;
  }

  function getTitle(&$record) {
    if ($record->strval('seriennummer') == NULL) return $record->strval('name');
    return $record->strval('name').' : '.$record->strval('seriennummer');
  }

  // Also place this javascript after an edit record form...
  function block__after_edit_record_form(){
    return $this->block__after_new_record_form();
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // Anzeige des Feldes "Add related ... record" bei Relationsship
  function titleColumn() {
    return "IF(seriennummer IS NULL,name,CONCAT(name,' : ',seriennummer))";
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
