-- loesche auch alle abhaengigen Verbindungen wie parameter, ablage, verbaut
-- ist auch als cronjob loesbar, aber dann nur alle 24h und wer cronjob nicht ausfuehrt, da wird es garnicht gloescht
--

--
-- Trigmed AFTER delete `mpi_medien`
--

  DROP TRIGGER IF EXISTS `med_after_del`;
  DELIMITER $$
  CREATE TRIGGER `med_after_del` AFTER DELETE ON mpi_medien FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab = 'medID' AND tabID = OLD.medID;
    DELETE FROM con_ablage    WHERE auswTab = 'medID' AND tabID = OLD.medID;
   END
   $$
   DELIMITER ;

