<?php

class tables_mpi_matMengenfluss { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // Anzeige des Feldes im "GlanceView" bei Relationship
  function getTitle(&$record) {
    if ($record->val('fluss') > 0) return $record->strval('vorgang').' : +'.$record->val('fluss');
    return $record->strval('vorgang').' : '.$record->val('fluss');
  }

  // Ueberpruefe syntaktische Eingabe im Feld Fluss
  function fluss__validate(&$record, $value, &$params) {
    $tabID = $record->val('matID');
    $sql = "SELECT SUM(fluss) FROM mpi_matMengenfluss WHERE matID = '$tabID'";
    list($menge) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (!isset($menge)) $menge = 0;
    if (!is_numeric($value)) {
      $params['message'] = 'Fehler: Nur Zahlen hier erlaubt!';  
      return false;   
    } elseif (($menge + $value) < '0') {  
      $params['message'] = 'Fehler: Dieser Wert hat ein negatives Resultat zur Folge! ('.$menge.' + '.$value.') < 0';
      return false;
    } elseif ($value == '0') {
      $params['message'] = 'Fehler: Was soll denn NULL bewirken!';
      return false;
    } else {
    return true;
    }
  }

  function matID__renderCell( &$record ) {
    $table  = 'mpi_material';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('matID');
    $name   = $record->display('matID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return "";
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  function beforeSave(&$record) {
    // User automatisch bei neu und aendern setzen
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // verhindere Einbau in zwei Anlagen
    $gerID = $record->val('gerID');
    $sysID = $record->val('sysID');
    if (($gerID != NULL) AND ($sysID != NULL)) {
      return Dataface_Error::permissionDenied('Material kann nur einmal pro Datensatz verbaut werden!');
    }
  }

/*
  // todo - menge uber grafted field und join fields.ini ermitteln
  function afterSave(&$record) {
    // Summe Menge in Tabelle Menge schreiben 
    $table = 'mpi_material';
    $tabID = $record->val('matID');
    $sql = "SELECT SUM(fluss) FROM mpi_matMengenfluss WHERE matID = '$tabID'";
    list($sum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $sql = "UPDATE $table SET menge = '$sum' WHERE tabID = '$tabID'";
    $res = xf_db_query($sql, df_db());
  }
*/

}
?>
