-- loesche auch alle abhaengigen Verbindungen wie parameter, ablage, verbaut
-- ist auch als cronjob loesbar, aber dann nur alle 24h und wer cronjob nicht ausfuehrt, da wird es garnicht gloescht
--

--
-- Trigmat AFTER delete `mpi_material`
--

  DROP TRIGGER IF EXISTS `mat_after_del`;
  DELIMITER $$
  CREATE TRIGGER `mat_after_del` AFTER DELETE ON mpi_material FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'matID' AND tabID =  OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'matID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'matID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'matID' AND tabID =  OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END
   $$
   DELIMITER ;

