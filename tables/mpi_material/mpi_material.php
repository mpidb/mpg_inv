<?php

class tables_mpi_material { 

  function block__after_result_list_content() {
    echo 'Lagerbestand hat den Mindestwert ';
    echo '<span style="background-color:#fcc;"> unterschritten. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#ffc;"> erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cff;"> fast erreicht. </span>&nbsp;|&nbsp;';
    echo '<span style="background-color:#cfc;"> noch nicht erreicht. </span>&nbsp;|&nbsp;';
    echo '<span> nicht ueberwacht. </span>&nbsp;';
  }

  // setze Farbe in listenansicht entsprechend Minwert zu Menge
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    $tabID = $record->val('tabID');
    $sql = "SELECT SUM(fluss) FROM mpi_matMengenfluss WHERE matID = $tabID";
    list($menge) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $min   = $record->val('min');
    $near  = $min + 1;
    if (($min == NULL) OR ($menge == NULL)) return $table.'sand'; 
    elseif ($menge <  $min)  return $table.'rot';
    elseif ($menge == $min)  return $table.'gelb';
    elseif ($menge == $near) return $table.'cyan';
    elseif ($menge >  $min)  return $table.'gruen';
    return $table.'sand';
  }

  function getTitle(Dataface_Record $record) {
    $name = $record->strval('name');
    if (strlen($name) > 15) $name = substr($name,0, 15)."...";
    if ($record->strval('eigenschaft') == NULL) return $name;
    return $name.' : '.$record->strval('eigenschaft');
  }

  // Anzeige des Feldes "Add related ... record" bei Relationsship
  function titleColumn() {
    return "IF(eigenschaft IS NULL,name,CONCAT(name,' : ',eigenschaft))";
  }

  // Menge dynamisch berechnen
  function menge__display(Dataface_Record $record) {
    $table = 'mpi_matMengenfluss';
    $tabID = $record->val('tabID');
    $sql = "SELECT SUM(fluss) FROM $table WHERE matID = '$tabID'";
    list($menge) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $menge;
  }

  function menge__rendercell(Dataface_Record $record) {
    return '<div style="text-align:right;">'.$record->display('menge').'</div>';
  }

  // Anzeige der 0-Werte (nicht verwechseln mit NULL) in details, wird sonst ausgeblendet
  function menge__htmlValue( &$record ) {
    if ($record->display('menge') == 0) return '&#x30';
    return $record->display('menge');
  }

  function block__menge_widget() {
    $app = Dataface_Application::getInstance();
    $query = $app->getQuery();
    if ( $query['-action'] == 'new') { 
      echo '--';
    } else {
      $record = $app->getRecord();
      echo $record->display('menge');
    }
  }

  // Anzeige der 0-Werte (nicht verwechseln mit NULL) in details, wird sonst ausgeblendet
  function min__htmlValue( &$record ) {
    if ($record->val('min') == 0) return '&#x30';
    return $record->val('min');
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
