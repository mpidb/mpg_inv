<?php

class tables_mpi_notiz { 

  function sysID__renderCell( &$record ) {
    if ( $record->val('sysID') == NULL ) return;
    $table  = 'mpi_arbeitsplatz';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('sysID');
    $name   = $record->display('sysID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function gerID__renderCell(&$record) {
    if ( $record->val('gerID') == NULL ) return;
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('gerID');
    $name   = $record->display('gerID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function matID__renderCell( &$record ) {
    if ( $record->val('matID') == NULL ) return;
    $table  = 'mpi_material';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('matID');
    $name   = $record->display('matID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
