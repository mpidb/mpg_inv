<?php

class tables_link
{ 

  function zeitstempel__display(&$record)
  {
    if ($record->val('zeitstempel') == NULL)
    {
      return "";
    } else {
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
    }
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record)
  {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
