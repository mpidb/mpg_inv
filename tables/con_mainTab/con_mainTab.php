<?php

class tables_con_mainTab { 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    $nameD  = $record->display('tabIDD');
    if (strlen($nameD) > 15) $nameD = substr($nameD,0, 15)."..."; 
    $nameS  = $record->display('tabIDS');
    if (strlen($nameS) > 15) $nameS = substr($nameS,0, 15).'...';
    return $nameD.' < '.$nameS;
  }

  function tabIDS__display(&$record) {
    $auswTbl = $record->strval('auswTabS');
    $table   = 'view_mainTab';
    $field   = 'tabID';
    $tabID   = $record->val('tabIDS');
    $sql     = "SELECT CONCAT(name,IF(ident IS NULL,'',CONCAT(' : ',ident))) FROM $table WHERE $field = '$tabID' AND auswTab = '$auswTbl'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db())); 
    return $name;
  }

  function tabIDS__renderCell( &$record ) {
    $auswTbl = $record->strval('auswTabS');
    switch ($auswTbl) {
      case 'sysID':
        $table = 'mpi_arbeitsplatz';
        $field = 'tabID';
        break;
      case 'gerID':
        $table = 'mpi_geraete';
        $field  = 'tabID';
        break;
      case 'matID':
        $table = 'mpi_material';
        $field  = 'tabID';
        break;
      case 'medID':
        $table = 'mpi_medien';
        $field  = 'medID';
        break;
    }
    $action = 'browse';
    $tabID  = $record->val('tabIDS');
    $name   = $record->display('tabIDS');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function tabIDD__display(&$record) {
    $auswTbl = $record->strval('auswTabD');
    $table   = 'view_mainTab';
    $field   = 'tabID';
    $tabID   = $record->val('tabIDD');
    $sql     = "SELECT CONCAT(name,IF(ident IS NULL,'',CONCAT(' : ',ident))) FROM $table WHERE $field = '$tabID' AND auswTab = '$auswTbl'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db())); 
    return $name;
  }

  function tabIDD__renderCell( &$record ) {
    $auswTbl = $record->strval('auswTabD');
    switch ($auswTbl) {
      case 'sysID':
        $table = 'mpi_arbeitsplatz';
        $field = 'tabID';
        break;
      case 'gerID':
        $table = 'mpi_geraete';
        $field  = 'tabID';
        break;
      case 'matID':
        $table = 'mpi_material';
        $field  = 'tabID';
        break;
      case 'medID':
        $table = 'mpi_medien';
        $field  = 'medID';
        break;
    }
    $action = 'browse';
    $tabID  = $record->val('tabIDD');
    $name   = $record->display('tabIDD');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function valuelist__used() {
    return array(0=>'Nein', 1=>'Ja');
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }


}
?>
