-- loesche auch alle abhaengigen Verbindungen wie parameter, ablage, verbaut
-- ist auch als cronjob loesbar, aber dann nur alle 24h und wer cronjob nicht ausfuehrt, da wird es garnicht gloescht
--

--
-- Trigger AFTER delete `mpi_arbeitsplatz`
--

  DROP TRIGGER IF EXISTS `sys_after_del`;
  DELIMITER $$
  CREATE TRIGGER `sys_after_del` AFTER DELETE ON mpi_arbeitsplatz FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'sysID' AND tabID  = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'sysID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'sysID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'sysID' AND tabID  = OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END
   $$
   DELIMITER ;

