<?php

class tables_mpi_arbeitsplatz
{ 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // Preis bei ReadOnly verstecken
  function preis__permissions(&$record)
  {
    $user = Dataface_AuthenticationTool::getInstance()->getLoggedInUser();
    if ( !$user ) return Dataface_PermissionsTool::NO_ACCESS();
    $role = $user->val('role');
    if ( $role == 'READ ONLY' ) { return Dataface_PermissionsTool::NO_ACCESS(); }
  }

  function getTitle(Dataface_Record $record) {
    $name = $record->strval('name');
    if (strlen($name) > 15) $name = substr($name,0, 15)."...";
    if ($record->strval('seriennummer') == NULL) return $name;
    return $name.' : '.$record->strval('seriennummer');
  }

  // Anzeige des Feldes "Add related ... record" bei Relationsship
  function titleColumn() {
    return "IF(seriennummer IS NULL,name,CONCAT(name,' : ',seriennummer))";
  }

  function inventar__rendercell(&$record) {
    $table  = 'list_inventar';
    $action = 'browse';
    $tabID  = $record->strval('inventar');
    if ($tabID == NULL) return;
    $tabID2 = $record->strval('unterNr');
    $name   = $record->strval('inventar').':'.$record->display('unterNr');
    $sql    = "SELECT anlage FROM list_inventar WHERE anlage = '$tabID' AND unterNr = '$tabID2'";
    list($anlage) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (isset($anlage)) {
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&anlage=$tabID&unterNr=$tabID2";
      return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
    }
    if ($record->strval('status') != 'verschrottet') return '<font color="red">'.$name.'</font>';
    return '<font color="gray">'.$name.'</font>';
  }

  function block__after_preis_widget() {
    echo Euro;
  }

  function lieferdatum__display(&$record) {
    if ($record->val('lieferdatum') == NULL) { return ""; } 
    else { return date('d.m.Y', strtotime($record->strval('lieferdatum'))); }
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
      return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // 0 per default geht nicht in xataface, workaround
    if (($record->strval('inventar') != NULL) AND ($record->strval('unterNr') == NULL)) {
      $record->setValue('unterNr', '0');
    }
  }
 
}
?>
