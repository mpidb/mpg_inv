CREATE OR REPLACE VIEW view_it_lab_labSystem AS
SELECT
 lsys.systemID, lsys.system, lsys.lieferant, lpc.labpc, lusr.luser, lusr.lgroup
FROM
 mpidb_it_lab.mpi_labSystem AS lsys
 LEFT JOIN mpidb_it_lab.con_syspc   AS scon ON lsys.systemID = scon.systemID
 LEFT JOIN mpidb_it_lab.mpi_labPC   AS lpc  ON scon.labpc    = lpc.labpc
 LEFT JOIN mpidb_it_lab.con_pcuser  AS pcon ON lpc.labpc     = pcon.labpc
 LEFT JOIN mpidb_it_lab.mpi_labUser AS lusr ON pcon.luser    = lusr.luser
ORDER BY lsys.system
