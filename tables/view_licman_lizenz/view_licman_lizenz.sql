-- hole von db licman alles passend zur bestellnummer
-- version standalone
CREATE OR REPLACE VIEW view_licman_lizenz AS
 SELECT
  '000001' AS lizenzID,
  'software : modell : anzahl' AS bezeichnung,
  '0123456789' AS bestellnummer,
  'alone' AS bearbeiter,
  '2016-07-25 12:46:00' AS zeitstempel
;

-- version mpi-dcts mit it_inv
CREATE OR REPLACE VIEW view_licman_lizenz AS
 SELECT
  lizenzID,
  bezeichnung,
  bestellnummer,
  bearbeiter,
  zeitstempel
 FROM
  mpidb_it_licman.mpi_lizenz WHERE bestellnummer IS NOT NULL AND bestellnummer != '' AND status = 1
 ORDER BY
  bezeichnung
;

-- version mpg mit mpg_inv
CREATE OR REPLACE VIEW view_licman_lizenz AS
 SELECT
  lizenzID,
  bezeichnung,
  bestellnummer,
  bearbeiter,
  zeitstempel
 FROM
  mpidb_mpg_licman.mpi_lizenz WHERE bestellnummer IS NOT NULL AND bestellnummer != '' AND status = 1
 ORDER BY
  bezeichnung
;

