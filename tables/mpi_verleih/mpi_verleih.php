<?php

class tables_mpi_verleih
{ 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    // return $table;  // ohne color
    if ( $record->strval('status') == '0' ) return $table.'gruen';
    return $table.'normal';
  }

  // loeschen nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS(); //autom. login dialog
    return Dataface_PermissionsTool::getRolePermissions('EDIT');
  }

  // Aktuelles Datum bei Ausgabe setzen
  function ausgabe__default() { 
    return date('Y-m-d');
  }	

  function gerID__rendercell(&$record) {
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'gerID';
    $tabID  = $record->val($field);
    $sql = "SELECT CONCAT(art,' : ',name,' : ',status) from $table WHERE tabID='$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&tabID=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function valuelist__status(){
       return array(0=>'verfuegbar', 1=>'ausgeliehen');
  }


  function ausgabe__display(&$record) {
    if ($record->val('ausgabe') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('ausgabe')));
  }

  function rueckgabe__display(&$record) {
    if ($record->val('rueckgabe') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('rueckgabe')));
  }

  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);

    // leihbar?
    $tabID = $record->strval('gerID');
    $table = 'mpi_geraete';
    $sql = "SELECT verleih FROM $table WHERE tabID='$tabID'";
    list($res) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    If ($res == 0) return Dataface_Error::permissionDenied('Sorry, Geraet nicht ausleihbar. Bitte erst auf Ausleihe setzen!');

    // Datum checken, ob Ausgabe <= Rueckgabe !!!
    $ausg  = $record->strval('ausgabe');
    $rueck = $record->strval('rueckgabe');
    if (($ausg > $rueck) AND ($rueck != NULL)) {
      return Dataface_Error::permissionDenied('Datum Rueckgabe '.$rueck.' kann nicht vor Ausgabe '.$ausg.' liegen!');
    }
  }

}

?>
