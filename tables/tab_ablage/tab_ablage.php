<?php

class tables_tab_ablage { 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    $fname = $record->display('file_filename');
    if (strlen($fname) > 15) $fname = substr($fname,0, 15)."...";
    return $record->display('kategorie').' : '.$fname;
  }

  function file_filename__renderCell( &$record ) {
    $table  = 'tab_ablage';
    $action = 'getBlob';
    $field  = 'ablID';
    $tabID  = $record->val($field);
    $name   = $record->strval('file_filename');
    //$url = $record->getURL('-action=getBlob');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&-field=file&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function links__renderCell( &$record ) {
    $table  = 'con_ablage';
    $action = 'list';
    $field  = 'ablID';
    $tabID  = $record->val($field);
    $sql = "SELECT COUNT(ablID) FROM $table WHERE $field = '$tabID'";
    list($sum) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if ($sum == 0) return '<div style="text-align:right;">-</div>';
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:right;"><a href="'.$url.'">'.$sum.'</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record) {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}
?>
