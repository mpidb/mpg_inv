-- funktioniert zwar auf db-ebene, aber nicht in xataface, weil im Dialog die Verbindung ploetzlich zur Ablage fehlt und das fuehrt zu einer Fehleranzeige, deshalb wieder ueber cronjob
-- loesche auch alle abhaengigen Verbindungen wie parameter, ablage, verbaut

--
-- Trigmed AFTER delete `tab_ablage`
--

  DROP TRIGGER IF EXISTS `abl_after_del`;
  DELIMITER $$
  CREATE TRIGGER `abl_after_del` AFTER DELETE ON con_ablage FOR EACH ROW
   BEGIN
    DELETE FROM tab_ablage WHERE ablID NOT IN (SELECT ablID FROM con_ablage) AND ablID = OLD.ablID;
   END
   $$
   DELIMITER ;

