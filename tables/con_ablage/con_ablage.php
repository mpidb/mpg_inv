<?php

class tables_con_ablage { 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    //return $record->strval('kategorie').' : '.$record->strval('ablage.file_filename');
    //return $record->display('ablID');
    return $record->display('tabID');
  }

  function tabID__display(&$record) {
    $auswTbl = $record->strval('auswTab');
    $table   = 'view_mainTab';
    $field   = 'tabID';
    $tabID   = $record->val('tabID');
    $sql     = "SELECT CONCAT(name,IF(ident IS NULL,'',CONCAT(' : ',ident))) FROM $table WHERE $field = '$tabID' AND auswTab = '$auswTbl'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    return $name;
  }

  function tabID__renderCell( &$record ) {
    $auswTbl = $record->strval('auswTab');
    switch ($auswTbl) {
      case 'sysID':
        $table = 'mpi_arbeitsplatz';
        $field = 'tabID';
        break;
      case 'gerID':
        $table = 'mpi_geraete';
        $field  = 'tabID';
        break;
      case 'matID':
        $table = 'mpi_material';
        $field  = 'tabID';
        break;
      case 'medID':
        $table = 'mpi_medien';
        $field  = 'medID';
        break;
    }
    $action = 'browse';
    $tabID  = $record->val('tabID');
    $name   = $record->display('tabID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function ablID__renderCell( &$record ) {
    $table  = 'tab_ablage';
    $action = 'browse';
    $field  = 'ablID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<a href="'.$url.'">'.$name.'</a>';
  }

   // Zeitanzeige aendern
  function zeitstempel__display(&$record) {
    if ($record->val('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

}
?>
