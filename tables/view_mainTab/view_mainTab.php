<?php

class tables_view_mainTab { 

  // only authenticated user; edit in view not allowed; check role lower READ ONLY
  function getRoles(&$record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return 'NO ACCESS';
    if ( strpos($user->val('role'),'VIEW GROUP ') !== false) return 'NO ACCESS';
    return 'READ ONLY';
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // glanceview
  function getTitle(&$record) {
    if ($record->strval('ident') == NULL) return $record->strval('name');
    return $record->strval('name').' : '.$record->strval('ident');
  }

  function tabID__renderCell( &$record ) {
    $tabID = $record->strval('tabID');
    $tabID = str_pad($tabID, 6, 0, STR_PAD_LEFT);
    return $tabID; 
  }

/*
  function tabID_display( &$record ) {
    if ($record->strval('ident') == NULL) return $record->strval('name');
    return $record->strval('name').' : '.$record->strval('ident');
  }
*/

  function name__renderCell( &$record ) {
    $auswTbl = $record->strval('auswTab');
    switch ($auswTbl) {
      case 'sysID':
        $table = 'mpi_arbeitsplatz';
        $field  = 'tabID';
        break;
      case 'gerID':
        $table = 'mpi_geraete';
        $field  = 'tabID';
        break;
      case 'matID':
        $table = 'mpi_material';
        $field  = 'tabID';
        break;
      case 'medID':
        $table = 'mpi_medien';
        $field  = 'medID';
        break;
    }
    $action = 'browse';
    $tabID  = $record->val('tabID');
    $name   = $record->strval('name');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

}
?>
