CREATE OR REPLACE VIEW view_mainTab AS    
 SELECT
  CONVERT('sysID' USING utf8) COLLATE utf8_unicode_ci AS auswTab,
  tabID AS tabID,
  name,
  seriennummer AS ident
 FROM
  mpi_arbeitsplatz
 UNION ALL
 SELECT
  CONVERT('gerID' USING utf8) COLLATE utf8_unicode_ci AS auswTab,
  tabID AS tabID,
  name,
  seriennummer AS ident
 FROM
  mpi_geraete
 UNION ALL
 SELECT
  CONVERT('matID' USING utf8) COLLATE utf8_unicode_ci AS auswTab,
  tabID AS tabID,
  name,
  eigenschaft AS ident
 FROM
  mpi_material
 UNION ALL
 SELECT
  CONVERT('medID' USING utf8) COLLATE utf8_unicode_ci AS auswTab,
  medID AS tabID,
  name,
  seriennummer AS ident
 FROM
  mpi_medien
;

-- last
CREATE OR REPLACE VIEW view_mainTab AS    
 SELECT 'sysID' AS auswTab, tabID AS tabID, name, seriennummer AS ident FROM mpi_arbeitsplatz
 UNION ALL
 SELECT 'gerID' AS auswTab, tabID AS tabID, name, seriennummer AS ident FROM mpi_geraete
 UNION ALL
 SELECT 'matID' AS auswTab, tabID AS tabID, name, eigenschaft AS ident FROM mpi_material
 UNION ALL
 SELECT 'medID' AS auswTab, medID AS tabID, name, seriennummer AS ident FROM mpi_medien
;

-- last
CREATE OR REPLACE VIEW view_mainTab AS    
 SELECT 'sysID' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(seriennummer,'--')) AS name FROM mpi_arbeitsplatz
 UNION ALL
 SELECT 'gerID' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(seriennummer,'--')) AS name FROM mpi_geraete
 UNION ALL
 SELECT 'matID' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(eigenschaft,'--')) AS name FROM mpi_material
 UNION ALL
 SELECT 'medID' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(seriennummer,'--')) AS name FROM mpi_medien
;

