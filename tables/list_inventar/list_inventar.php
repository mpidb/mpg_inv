<?php

class tables_list_inventar { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // glanceview and selectbox
  function getTitle(&$record) {
    return $record->strval('invNummer').' : '.$record->strval('bezeichnung');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  // genutzt in anlage und geraete und per inventar.html angesprungen
  function block__after_inventar_widget() {
    $app    =& Dataface_Application::getInstance();
    $query  =& $app->getQuery();
    $action = $query['-action'];
    if ($action == 'new') return;
    $record =& $app->getRecord();
    if (!isset($record)) return;
    if ($record->strval('inventar') != NULL) {
      if ($record->strval('inventar.invNummer') != NULL) {
        echo $record->strval('inventar.invNummer').' : '.$record->strval('inventar.bezeichnung').' : '.$record->strval('inventar.invDatum');
      } elseif ($record->strval('status') != 'verschrottet') {
        echo '<font color="red">Inventarnummer nicht im System</font>';
      } else {
        echo '<font color="gray">Status: verschrottet</font>';
      }
    }
  }

  function tabID__renderCell( &$record ) {
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function invDatum__display(&$record)  {
    if ($record->strval('invDatum') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('invDatum')));
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
