<?php

class tables_view_it_licman_instInv {

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    return 'lizenz';
  }

  // glanceview and selectbox
  function getTitle(&$record) {
    return $record->display('software').' : '.$record->display('version');
  }

  // check login & editieren nicht erlaubt
  function getPermissions($record)
  {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  function valuelist__install() {
       return array(0=>'man', 1=>'auto');
  }

  function instID__renderCell(&$record) {
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $tabID  = $record->val('instID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    $sql    = "SELECT name FROM $table WHERE $field = '$tabID'";
    list($name) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if (!isset($name)) return '--';
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function instName__renderCell(&$record) {
    $action = 'browse';
    $tabID  = $record->val('tabID');
    $name   = str_pad($record->val('tabID'), 6, 0, STR_PAD_LEFT).' : '.$record->val('instName');
    if ($record->strval('inventar') == '1') {
      $field = 'conID';
      $insNa = 'instName';
      $table = 'mpi_inventar';
    } else {
      $field = 'installID';
      $insNa = 'instID';
      $table = 'mpi_install';
    }
    $instID = $record->val($insNa);
    $href   = str_replace('_inv/','_licman/',DATAFACE_SITE_HREF);
    $url    = "${href}?-table=${table}&-action=${action}&${field}=${tabID}&${insNa}=${instID}";
    return '<a href="'.$url.'">'.$name.'</a>';
  }

  function softID__renderCell(&$record) {
    $table  =  'mpi_software';
    $action = 'browse';
    $field  = 'softID';
    $tabID  = $record->val($field);
    $name   = $record->val('software');
    $href   = str_replace('_inv/','_licman/',DATAFACE_SITE_HREF);
    $url    = "${href}?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

 function verID__renderCell(&$record) {
    $table  = 'mpi_version';
    $action = 'browse';
    $field  = 'verID';
    $tabID  = $record->val($field);
    $name   = $record->strval('version');
    $href   = str_replace('_inv/','_licman/',DATAFACE_SITE_HREF);
    $url    = "${href}?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function lizenzID__renderCell(&$record) {
    $table  = 'mpi_lizenz';
    $action = 'browse';
    $field  = 'lizenzID';
    $tabID  = $record->val($field);
    $name   = $record->val('lizenz');
    $href   = str_replace('_inv/','_licman/',DATAFACE_SITE_HREF);
    $url    = "${href}?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function keyID__renderCell(&$record) {
    $table  = 'mpi_key';
    $action = 'browse';
    $field  = 'keyID';
    $tabID  = $record->val($field);
    $name   = $record->val('key');
    $href   = str_replace('_inv/','_licman/',DATAFACE_SITE_HREF);
    $url    = "${href}?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function inventar__renderCell(&$record) {
    $action = 'list';
    if ($record->strval('inventar') == '1') {
      $table = 'mpi_inventar';
      $field = 'instName';
      $name  = 'automatisch';
    } else {
      $table = 'mpi_install';
      $field = 'instID';
      $name  = 'manuell';
    }
    $tabID = $record->val($field);
    $href   = str_replace('_inv/','_licman/',DATAFACE_SITE_HREF);
    $url    = "${href}?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<a href="'.$url.'">'.$name.'</a>';
  }

}
?>

