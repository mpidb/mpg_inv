-- view nur wegen der einfachen permissions und der JOINS
-- lizenz status = 1
CREATE OR REPLACE VIEW view_it_licman_instInv AS    
SELECT
 vins.tabID,
 vins.instID,
 vins.instName,
 vins.softID,
 vins.verID,
 vins.lizenzID,
 vins.inventar,
 vins.useLizenz,
 vins.anzTreffer,
 vins.bearbeiter,
 soft.software,
 ver.version,
 lic.bezeichnung AS lizenz,
 sch.`key`
FROM
 mpidb_it_licman.view_invInst AS vins
 LEFT JOIN mpidb_it_licman.mpi_software AS soft ON vins.softID = soft.softID
 LEFT JOIN mpidb_it_licman.mpi_version AS ver ON vins.verID = ver.verID
 LEFT JOIN mpidb_it_licman.mpi_lizenz AS lic ON vins.lizenzID = lic.lizenzID AND lic.status = 1
 LEFT JOIN mpidb_it_licman.mpi_key AS sch ON vins.keyID = sch.keyID
ORDER BY software, instName
;

-- mpg-version ohne lizenzverwaltung (fake standalone)
CREATE OR REPLACE VIEW mpidb_mpg_inv.view_it_licman_instInv AS
SELECT
 1 AS tabID,
 1 AS instID
FROM
 mpi_geraete
WHERE
 FALSE
;

-- mpg-version mit lizenzverwaltung
CREATE OR REPLACE VIEW mpidb_mpg_inv.view_it_licman_instInv AS
SELECT
 vins.tabID,
 vins.instID,
 vins.instName,
 vins.softID,
 vins.verID,
 vins.lizenzID,
 vins.inventar,
 vins.useLizenz,
 vins.anzTreffer,
 vins.bearbeiter,
 soft.software,
 ver.version,
 lic.bezeichnung AS lizenz,
 sch.`key`
FROM
 mpidb_mpg_licman.view_invInst AS vins
 LEFT JOIN mpidb_mpg_licman.mpi_software AS soft ON vins.softID = soft.softID
 LEFT JOIN mpidb_mpg_licman.mpi_version AS ver ON vins.verID = ver.verID
 LEFT JOIN mpidb_mpg_licman.mpi_lizenz AS lic ON vins.lizenzID = lic.lizenzID AND lic.status = 1
 LEFT JOIN mpidb_mpg_licman.mpi_key AS sch ON vins.keyID = sch.keyID
ORDER BY software, instName
;


