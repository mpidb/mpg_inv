<?php

class tables_tab_instProt { 

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  function getTitle(&$record) {
    return $record->display('protID').' - '.$record->strval('schritt');
  }

  function gerID__renderCell(&$record) {
    $table  = 'mpi_geraete';
    $action = 'related_records_list';
    $field  = 'tabID';
    $tabID  = $record->val('gerID');
    $name   = $record->display('gerID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}&-relationship=protokoll";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // link auf interne db
  function sequenz__renderCell(&$record) {
    $table  = 'list_sequenz';
    $action = 'browse';
    $field  = 'sequenz';
    $tabID  = $record->strval($field);
    $name   = $record->strval('sequenz');
    $bereich = $record->strval('bereich');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}&bereich=${bereich}";
    $sql = "SELECT beschreibung FROM $table WHERE $field = '$name' AND bereich = '$bereich'";
    list($descr) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    if ($descr != NULL) {
      return '<div style="text-align:left;" title="'.$descr.'"><a href="'.$url.'">'.$name.'</a>&nbsp;&nbsp;<img src="images/info_icon.gif"></img></div>';
    }
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function protID__renderCell(&$record) {
    $name = $record->display('protID');
    return '<div style="text-align:left;">'.$name.'</div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $user = Dataface_AuthenticationTool::getInstance()->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
