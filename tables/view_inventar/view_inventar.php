<?php

class tables_view_inventar { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;  // ohne color
  }

  function anlage__renderCell( &$record ) {
    $table  = 'view_inventar';
    $action = 'browse';
    $field  = 'anlage';
    $tabID  = $record->val($field);
    $name   = $record->display($field);
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${tabID}";
    return '<div style="text-align:right;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function unterNr__renderCell( &$record ) {
    $name   = $record->strval('unterNr');
    return '<div style="text-align:right;">'.$name.'</div>';
  }

  function sysID__renderCell( &$record ) {
    $table  = 'mpi_arbeitsplatz';
    $action = 'browse';
    $field  = 'tabID';
    $gerID  = $record->val('gerID');
    $sysID  = $record->val('sysID');
    if (($gerID == NULL) AND ($sysID == NULL)) {
      $name    = $record->strval('bezeichnung');
      $anlage  = $record->strval('anlage');
      $unterNr = $record->strval('unterNr');
      $raum    = $record->strval('standort');
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=new&name=${name}&inventar=${anlage}&unterNr=${unterNr}&typ=${name}&lagerort=${raum}";
      return '<a href="'.$url.'" style="text-align:right;" title="Neues System ['.$name.'] hinzufügen"><img src="images/addBlau.png"></img></a>';
    }
    $name   = $record->display('sysID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${sysID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  function gerID__renderCell( &$record ) {
    $table  = 'mpi_geraete';
    $action = 'browse';
    $field  = 'tabID';
    $gerID  = $record->val('gerID');
    $sysID  = $record->val('sysID');
    if (($gerID == NULL) AND ($sysID == NULL)) {
      $name    = $record->strval('bezeichnung');
      $anlage  = $record->strval('anlage');
      $unterNr = $record->strval('unterNr');
      $raum    = $record->strval('standort');
      $url = DATAFACE_SITE_HREF."?-table=${table}&-action=new&name=${name}&inventar=${anlage}&unterNr=${unterNr}&typ=${name}&lagerort=${raum}";
      return '<a href="'.$url.'" style="text-align:right;" title="Neues Gerät ['.$name.'] hinzufügen"><img src="images/addBlau.png"></img></a>';
    }
    $name   = $record->display('gerID');
    $url    = DATAFACE_SITE_HREF."?-table=${table}&-action=${action}&${field}=${gerID}";
    return '<div style="text-align:left;"><a href="'.$url.'">'.$name.'</a></div>';
  }

  // Formatiere Zeitstempel auf Deutsch
  function invDatum__display(&$record)  {
    if ($record->strval('invDatum') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('invDatum')));
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

  // User automatisch bei neu und aendern setzen
  function beforeSave(&$record) {
    // setze user
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user = $auth->getLoggedInUsername();
    $record->setValue('bearbeiter', $user);
  }

}

?>
