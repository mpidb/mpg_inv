-- view auf list_inventar, beschleinigt tabellenaufbau
 CREATE OR REPLACE VIEW view_inventar AS
 SELECT
  inv .*,
  CONCAT(inv.anlage,':',inv.unterNr) AS inventar,
  sys.tabID AS sysID,
  sys.lagerort AS sysOrt,
  ger.tabID AS gerID,
  ger.lagerort AS gerOrt,
  IF((ger.tabID IS NULL) AND (sys.tabID IS NULL), 0, 1) AS exist
 FROM
  list_inventar AS inv
  LEFT JOIN mpi_arbeitsplatz AS sys ON inv.anlage = sys.inventar AND inv.unterNr = sys.unterNr
  LEFT JOIN mpi_geraete AS ger ON inv.anlage = ger.inventar AND inv.unterNr = ger.unterNr
 ;

-- ohne anlage
CREATE OR REPLACE VIEW view_inventar AS 
SELECT
 inv .*,
 CONCAT(inv.anlage,':',inv.unterNr) AS inventar,
 ger.tabID,
 ger.lagerort,
 IF(ger.tabID IS NULL, 0, 1) AS exist
FROM
 list_inventar AS inv
 LEFT JOIN mpi_geraete AS ger ON inv.anlage = ger.inventar AND inv.unterNr = ger.unterNr
;

