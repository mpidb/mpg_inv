
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mpidb_mpg_inv` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `mpidb_mpg_inv`;
DROP TABLE IF EXISTS `con_ablage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `con_ablage` (
  `conID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `auswTab` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tabID` smallint(6) unsigned NOT NULL,
  `ablID` smallint(6) unsigned zerofill NOT NULL,
  PRIMARY KEY (`conID`),
  UNIQUE KEY `ablage_tab` (`auswTab`,`tabID`,`ablID`),
  KEY `ablID` (`ablID`),
  CONSTRAINT `ablage_ablage` FOREIGN KEY (`ablID`) REFERENCES `tab_ablage` (`ablID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `con_ablage` WRITE;
/*!40000 ALTER TABLE `con_ablage` DISABLE KEYS */;
INSERT INTO `con_ablage` VALUES (000001,'gerID',1,000001);
/*!40000 ALTER TABLE `con_ablage` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `con_mainTab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `con_mainTab` (
  `conID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `auswTabS` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tabIDS` smallint(6) unsigned NOT NULL,
  `auswTabD` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tabIDD` smallint(6) unsigned NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`conID`),
  UNIQUE KEY `ausw_tabID` (`auswTabS`,`tabIDS`,`auswTabD`,`tabIDD`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `con_mainTab` WRITE;
/*!40000 ALTER TABLE `con_mainTab` DISABLE KEYS */;
INSERT INTO `con_mainTab` VALUES (000002,'medID',1,'gerID',1,0,NULL,'admin','2017-02-07 07:25:43');
/*!40000 ALTER TABLE `con_mainTab` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__failed_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__failed_logins` (
  `attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `time_of_attempt` int(11) NOT NULL,
  PRIMARY KEY (`attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__failed_logins` WRITE;
/*!40000 ALTER TABLE `dataface__failed_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__failed_logins` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__index` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `table` varchar(64) NOT NULL,
  `record_id` varchar(255) NOT NULL,
  `record_url` varchar(255) NOT NULL,
  `record_title` varchar(255) NOT NULL,
  `record_description` text,
  `lang` varchar(2) NOT NULL,
  `searchable_text` text,
  PRIMARY KEY (`index_id`),
  UNIQUE KEY `record_key` (`record_id`,`lang`),
  FULLTEXT KEY `searchable_text_index` (`searchable_text`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__index` WRITE;
/*!40000 ALTER TABLE `dataface__index` DISABLE KEYS */;
INSERT INTO `dataface__index` VALUES (19,'mpi_geraete','mpi_geraete?tabID=1','https://192.168.30.128/mpg_inv/index.php?-table=mpi_geraete&-action=browse&tabID=%3D1','pc101 : 1234567','pc101','en','pc101, , , 1234567, , PC, , , , , , , , 400100, 1, 400100-1, MPI, , , , admin[/////]: P200 0000 P200 0000 0000 0000 M100 A355');
INSERT INTO `dataface__index` VALUES (20,'mpi_geraete','mpi_geraete?tabID=1','https://192.168.30.128/mpg_inv/index.php?-table=mpi_geraete&-action=browse&tabID=%3D1','pc101 : 1234567','pc101','de','pc101, , , 1234567, , PC, , , , , , , , 400100, 1, 400100-1, MPI, , , , admin[/////]: P200 0000 P200 0000 0000 0000 M100 A355');
INSERT INTO `dataface__index` VALUES (21,'mpi_medien','mpi_medien?medID=000001','https://192.168.30.128/mpg_inv/index.php?-table=mpi_medien&-action=browse&medID=%3D000001','HDD-0001 : 1234567','HDD','en','HDD, HDD-0001, 1234567, , , , , , , , , , admin[/////]: H300 H300 0000 A355');
INSERT INTO `dataface__index` VALUES (22,'mpi_medien','mpi_medien?medID=000001','https://192.168.30.128/mpg_inv/index.php?-table=mpi_medien&-action=browse&medID=%3D000001','HDD-0001 : 1234567','HDD','de','HDD, HDD-0001, 1234567, , , , , , , , , , admin[/////]: H300 H300 0000 A355');
/*!40000 ALTER TABLE `dataface__index` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__modules` (
  `module_name` varchar(255) NOT NULL,
  `module_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__modules` WRITE;
/*!40000 ALTER TABLE `dataface__modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__modules` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__mtimes` (
  `name` varchar(255) NOT NULL,
  `mtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__preferences` (
  `pref_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `table` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pref_id`),
  KEY `username` (`username`),
  KEY `table` (`table`),
  KEY `record_id` (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__preferences` WRITE;
/*!40000 ALTER TABLE `dataface__preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__preferences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__record_mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__record_mtimes` (
  `recordhash` varchar(32) NOT NULL,
  `recordid` varchar(255) NOT NULL,
  `mtime` int(11) NOT NULL,
  PRIMARY KEY (`recordhash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__record_mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__record_mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__record_mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__version` (
  `version` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__version` WRITE;
/*!40000 ALTER TABLE `dataface__version` DISABLE KEYS */;
INSERT INTO `dataface__version` VALUES (1100);
/*!40000 ALTER TABLE `dataface__version` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `tabID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gerID` smallint(6) NOT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tabID`),
  UNIQUE KEY `name` (`name`,`gerID`),
  KEY `linkGroup` (`linkGroup`),
  KEY `gerID` (`gerID`),
  CONSTRAINT `interface_geraete` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `linkid` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `masterlink` int(10) DEFAULT NULL,
  `endpoint1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endpoint2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conGroup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conMember` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`linkid`),
  KEY `type` (`type`),
  KEY `conMember` (`conMember`),
  KEY `conType` (`conType`),
  KEY `endpoint2` (`endpoint2`),
  KEY `endpoint1` (`endpoint1`),
  KEY `masterlink` (`masterlink`),
  CONSTRAINT `link_ibfk_1` FOREIGN KEY (`conType`) REFERENCES `list_conType` (`conType`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_10` FOREIGN KEY (`conMember`) REFERENCES `list_conMember` (`conMember`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_11` FOREIGN KEY (`endpoint1`) REFERENCES `interface` (`linkGroup`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_12` FOREIGN KEY (`endpoint2`) REFERENCES `interface` (`linkGroup`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_13` FOREIGN KEY (`masterlink`) REFERENCES `link` (`linkid`) ON DELETE SET NULL,
  CONSTRAINT `link_ibfk_2` FOREIGN KEY (`type`) REFERENCES `list_lnkType` (`lnkType`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_3` FOREIGN KEY (`conMember`) REFERENCES `list_conMember` (`conMember`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_5` FOREIGN KEY (`endpoint1`) REFERENCES `interface` (`linkGroup`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_6` FOREIGN KEY (`endpoint2`) REFERENCES `interface` (`linkGroup`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_7` FOREIGN KEY (`masterlink`) REFERENCES `link` (`linkid`) ON DELETE SET NULL,
  CONSTRAINT `link_ibfk_8` FOREIGN KEY (`conType`) REFERENCES `list_conType` (`conType`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `link_ibfk_9` FOREIGN KEY (`type`) REFERENCES `list_lnkType` (`lnkType`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_art`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_art` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `art` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `art` (`art`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_art` WRITE;
/*!40000 ALTER TABLE `list_art` DISABLE KEYS */;
INSERT INTO `list_art` VALUES (2,'Bildschirm');
INSERT INTO `list_art` VALUES (15,'Labor Device');
INSERT INTO `list_art` VALUES (8,'Laptop');
INSERT INTO `list_art` VALUES (1,'PC');
INSERT INTO `list_art` VALUES (4,'Server');
INSERT INTO `list_art` VALUES (3,'Switch');
INSERT INTO `list_art` VALUES (5,'USB-Stick');
INSERT INTO `list_art` VALUES (13,'Virtual');
/*!40000 ALTER TABLE `list_art` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_bereich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_bereich` (
  `berID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `bereich` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`berID`),
  UNIQUE KEY `bereich` (`bereich`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Liste Sequenzen Installation';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_bereich` WRITE;
/*!40000 ALTER TABLE `list_bereich` DISABLE KEYS */;
INSERT INTO `list_bereich` VALUES (000007,'Auslieferung');
INSERT INTO `list_bereich` VALUES (000002,'Betriebssystem');
INSERT INTO `list_bereich` VALUES (000005,'Deaktivierung');
INSERT INTO `list_bereich` VALUES (000001,'Prepare');
INSERT INTO `list_bereich` VALUES (000003,'SoftwarePakete');
INSERT INTO `list_bereich` VALUES (000004,'Update');
INSERT INTO `list_bereich` VALUES (000006,'Verschrottung');
/*!40000 ALTER TABLE `list_bereich` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_conMember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_conMember` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `conMember` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `conMember` (`conMember`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_conMember` WRITE;
/*!40000 ALTER TABLE `list_conMember` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_conMember` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_conType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_conType` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `conType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `conType` (`conType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_conType` WRITE;
/*!40000 ALTER TABLE `list_conType` DISABLE KEYS */;
INSERT INTO `list_conType` VALUES (1,'direct');
INSERT INTO `list_conType` VALUES (2,'link');
INSERT INTO `list_conType` VALUES (3,'uplink');
/*!40000 ALTER TABLE `list_conType` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_einheit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_einheit` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `einheit` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `einheit` (`einheit`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_einheit` WRITE;
/*!40000 ALTER TABLE `list_einheit` DISABLE KEYS */;
INSERT INTO `list_einheit` VALUES (4,'GB');
INSERT INTO `list_einheit` VALUES (6,'GHz');
INSERT INTO `list_einheit` VALUES (12,'HDD');
INSERT INTO `list_einheit` VALUES (9,'IP-V4');
INSERT INTO `list_einheit` VALUES (10,'IP-V6');
INSERT INTO `list_einheit` VALUES (2,'KB');
INSERT INTO `list_einheit` VALUES (3,'MB');
INSERT INTO `list_einheit` VALUES (5,'MHz');
INSERT INTO `list_einheit` VALUES (8,'Pixel');
INSERT INTO `list_einheit` VALUES (11,'SSD');
INSERT INTO `list_einheit` VALUES (1,'TB');
INSERT INTO `list_einheit` VALUES (7,'Zoll');
/*!40000 ALTER TABLE `list_einheit` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_inventar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_inventar` (
  `anlage` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `unterNr` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `invNummer` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bezeichnung` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `standort` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wert` decimal(7,2) DEFAULT NULL,
  `invDatum` date DEFAULT NULL,
  `kostenstelle` decimal(5,0) DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`anlage`,`unterNr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_inventar` WRITE;
/*!40000 ALTER TABLE `list_inventar` DISABLE KEYS */;
INSERT INTO `list_inventar` VALUES ('400100','1','400100:1 Computer','Dell Optiplex 755','N1.01',250.00,'2016-04-01',1000,'Beispieleintrag','import','2016-04-10 06:12:22');
/*!40000 ALTER TABLE `list_inventar` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_katReiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_katReiter` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_katReiter` WRITE;
/*!40000 ALTER TABLE `list_katReiter` DISABLE KEYS */;
INSERT INTO `list_katReiter` VALUES (5,'Ablage');
INSERT INTO `list_katReiter` VALUES (7,'Auswertung');
INSERT INTO `list_katReiter` VALUES (11,'Autorisierung');
INSERT INTO `list_katReiter` VALUES (1,'Haupttabelle');
INSERT INTO `list_katReiter` VALUES (8,'History');
INSERT INTO `list_katReiter` VALUES (2,'Liste');
INSERT INTO `list_katReiter` VALUES (10,'Netzwerk');
INSERT INTO `list_katReiter` VALUES (9,'Programmierung');
INSERT INTO `list_katReiter` VALUES (3,'View');
INSERT INTO `list_katReiter` VALUES (6,'Zuordnung');
/*!40000 ALTER TABLE `list_katReiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_kategorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_kategorie` (
  `katID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`katID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_kategorie` WRITE;
/*!40000 ALTER TABLE `list_kategorie` DISABLE KEYS */;
INSERT INTO `list_kategorie` VALUES (000001,'Angebot');
INSERT INTO `list_kategorie` VALUES (000002,'Auftragsbestätigung');
INSERT INTO `list_kategorie` VALUES (000003,'Betriebsanweisung');
INSERT INTO `list_kategorie` VALUES (000004,'Lieferant');
INSERT INTO `list_kategorie` VALUES (000008,'Lizenz');
INSERT INTO `list_kategorie` VALUES (000005,'Preisliste');
INSERT INTO `list_kategorie` VALUES (000006,'Technische Information');
INSERT INTO `list_kategorie` VALUES (000007,'Zertifikat');
/*!40000 ALTER TABLE `list_kategorie` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_lagerort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_lagerort` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `lagerort` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `lagerort` (`lagerort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_lagerort` WRITE;
/*!40000 ALTER TABLE `list_lagerort` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_lagerort` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_lnkType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_lnkType` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `lnkType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `lnkType` (`lnkType`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_lnkType` WRITE;
/*!40000 ALTER TABLE `list_lnkType` DISABLE KEYS */;
INSERT INTO `list_lnkType` VALUES (1,'CAT6');
INSERT INTO `list_lnkType` VALUES (2,'CAT7');
INSERT INTO `list_lnkType` VALUES (3,'MMF');
INSERT INTO `list_lnkType` VALUES (4,'SMF');
INSERT INTO `list_lnkType` VALUES (5,'virtual');
/*!40000 ALTER TABLE `list_lnkType` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_nachricht`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_nachricht` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `nachricht` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `nachricht` (`nachricht`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_nachricht` WRITE;
/*!40000 ALTER TABLE `list_nachricht` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_nachricht` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_os`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_os` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `os` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `os` (`os`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_os` WRITE;
/*!40000 ALTER TABLE `list_os` DISABLE KEYS */;
INSERT INTO `list_os` VALUES (7,' Windows 7 Workplace');
INSERT INTO `list_os` VALUES (3,'--');
INSERT INTO `list_os` VALUES (9,'iOS');
INSERT INTO `list_os` VALUES (14,'OS X');
INSERT INTO `list_os` VALUES (2,'Ubuntu 10.04');
INSERT INTO `list_os` VALUES (1,'Ubuntu 12.04');
INSERT INTO `list_os` VALUES (4,'Ubuntu 12.04 puppet');
INSERT INTO `list_os` VALUES (15,'Ubuntu 14.04');
INSERT INTO `list_os` VALUES (8,'Windows 7 LabPC');
INSERT INTO `list_os` VALUES (13,'Windows 7 Pro');
INSERT INTO `list_os` VALUES (10,'Windows 8');
INSERT INTO `list_os` VALUES (11,'Windows Server 2008R2');
INSERT INTO `list_os` VALUES (12,'Windows Server 2012 R2');
INSERT INTO `list_os` VALUES (6,'Windows XP LabPC');
INSERT INTO `list_os` VALUES (5,'Windows XP Workplace');
/*!40000 ALTER TABLE `list_os` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_parTyp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_parTyp` (
  `typID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `typ` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`typID`),
  UNIQUE KEY `typ` (`typ`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_parTyp` WRITE;
/*!40000 ALTER TABLE `list_parTyp` DISABLE KEYS */;
INSERT INTO `list_parTyp` VALUES (000012,'Aufloesung');
INSERT INTO `list_parTyp` VALUES (000018,'CPU Kerne');
INSERT INTO `list_parTyp` VALUES (000017,'CPU Taktfrequenz');
INSERT INTO `list_parTyp` VALUES (000016,'CPU Typ');
INSERT INTO `list_parTyp` VALUES (000032,'CPU-Typ');
INSERT INTO `list_parTyp` VALUES (000011,'Diagonale');
INSERT INTO `list_parTyp` VALUES (000028,'IP-Adresse');
INSERT INTO `list_parTyp` VALUES (000033,'lastFound');
INSERT INTO `list_parTyp` VALUES (000034,'lastFoundM');
INSERT INTO `list_parTyp` VALUES (000029,'Mac-Adresse');
INSERT INTO `list_parTyp` VALUES (000030,'NW-Interface');
INSERT INTO `list_parTyp` VALUES (000020,'RAM Speicher');
INSERT INTO `list_parTyp` VALUES (000019,'RAM Typ');
INSERT INTO `list_parTyp` VALUES (000031,'Speichermedium');
/*!40000 ALTER TABLE `list_parTyp` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_projekt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_projekt` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `projekt` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `projekt` (`projekt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_projekt` WRITE;
/*!40000 ALTER TABLE `list_projekt` DISABLE KEYS */;
/*!40000 ALTER TABLE `list_projekt` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_protokoll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_protokoll` (
  `protID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `protokoll` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gerID` smallint(6) NOT NULL,
  `bemerkung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`protID`),
  UNIQUE KEY `prot_geraet` (`protokoll`,`gerID`),
  KEY `gerID` (`gerID`),
  CONSTRAINT `protokoll_geraete` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Vorlagen Protokolle';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_protokoll` WRITE;
/*!40000 ALTER TABLE `list_protokoll` DISABLE KEYS */;
INSERT INTO `list_protokoll` VALUES (000001,'Inbetriebnahme_PC',1,'Der Vorlage-PC sollte dann spaeter nicht geloescht werden ;-)');
/*!40000 ALTER TABLE `list_protokoll` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_protokoll__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_protokoll__history` (
  `history__id` int(11) NOT NULL AUTO_INCREMENT,
  `history__language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__comments` text COLLATE utf8_unicode_ci,
  `history__user` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__state` int(5) DEFAULT '0',
  `history__modified` datetime DEFAULT NULL,
  `protID` smallint(6) unsigned zerofill DEFAULT NULL,
  `protokoll` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gerID` smallint(6) DEFAULT NULL,
  `bemerkung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`history__id`),
  KEY `prikeys` (`protID`) USING HASH,
  KEY `datekeys` (`history__modified`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_protokoll__history` WRITE;
/*!40000 ALTER TABLE `list_protokoll__history` DISABLE KEYS */;
INSERT INTO `list_protokoll__history` VALUES (1,'en','','admin',0,'2017-02-07 08:40:07',000001,'Inbetriebnahme_PC',1,'Der Vorlage-PC sollte dann spaeter nicht geloescht werden ;-)');
/*!40000 ALTER TABLE `list_protokoll__history` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_reiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_reiter` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `favorit` tinyint(1) NOT NULL DEFAULT '0',
  `history` tinyint(1) NOT NULL DEFAULT '0',
  `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `reiter` (`reiter`),
  KEY `kategorie` (`kategorie`),
  CONSTRAINT `list_reiter_ibfk_1` FOREIGN KEY (`kategorie`) REFERENCES `list_katReiter` (`kategorie`) ON UPDATE CASCADE,
  CONSTRAINT `list_reiter_ibfk_2` FOREIGN KEY (`kategorie`) REFERENCES `list_katReiter` (`kategorie`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=279 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_reiter` WRITE;
/*!40000 ALTER TABLE `list_reiter` DISABLE KEYS */;
INSERT INTO `list_reiter` VALUES (000100,'interface','Netzwerk',0,0,'???');
INSERT INTO `list_reiter` VALUES (000105,'link','Netzwerk',0,0,'???');
INSERT INTO `list_reiter` VALUES (000110,'list_art','Liste',0,0,'Art des Geraetes');
INSERT INTO `list_reiter` VALUES (000115,'list_conMember','Netzwerk',0,0,'???');
INSERT INTO `list_reiter` VALUES (000120,'list_conType','Netzwerk',0,0,'???');
INSERT INTO `list_reiter` VALUES (000125,'list_lagerort','Liste',0,0,'Aufstellungsort bzw. Abladeplatz');
INSERT INTO `list_reiter` VALUES (000130,'list_katReiter','Liste',0,0,'Zugehoerigkeit DB-Tabellen');
INSERT INTO `list_reiter` VALUES (000135,'list_lnkTyp','Netzwerk',0,0,'???');
INSERT INTO `list_reiter` VALUES (000140,'list_einheit','Netzwerk',0,0,'???');
INSERT INTO `list_reiter` VALUES (000145,'list_nachricht','Liste',0,0,'Mailadresse fuer Wartungsmail');
INSERT INTO `list_reiter` VALUES (000150,'list_os','Liste',0,0,'Auswahl Betriebssystem');
INSERT INTO `list_reiter` VALUES (000155,'list_projekt','Liste',0,0,'Projektliste');
INSERT INTO `list_reiter` VALUES (000160,'list_status','Liste',0,0,'Betriebszustand Geraet');
INSERT INTO `list_reiter` VALUES (000163,'list_reiter','Liste',0,0,'Sammelcontainer fuer Tabbutton \"mehr ..\"');
INSERT INTO `list_reiter` VALUES (000170,'list_vorgang','Liste',0,0,'Materialfluss');
INSERT INTO `list_reiter` VALUES (000172,'tab_ablage','Ablage',0,0,'Dateiablagen fuer Geraete, Medien usw.');
INSERT INTO `list_reiter` VALUES (000180,'mpi_arbeitsplatz','Haupttabelle',0,1,'Arbeitsplatz als System');
INSERT INTO `list_reiter` VALUES (000190,'mpi_geraete','Haupttabelle',0,1,'Geraete der IT');
INSERT INTO `list_reiter` VALUES (000200,'mpi_gerWartung','Haupttabelle',0,0,'Wartungslisten fuer Geraete');
INSERT INTO `list_reiter` VALUES (000205,'mpi_lieferant','Liste',0,0,'Liste Lieferanten');
INSERT INTO `list_reiter` VALUES (000210,'mpi_material','Haupttabelle',0,1,'Verbrauchsmaterial');
INSERT INTO `list_reiter` VALUES (000212,'mpi_medien','Haupttabelle',0,1,'Lifecycle Speichermedien');
INSERT INTO `list_reiter` VALUES (000225,'mpi_matMengenfluss','Liste',0,0,'Materialfluss fuer Material');
INSERT INTO `list_reiter` VALUES (000232,'mpi_verleih','Haupttabelle',0,1,'Ausleihe Geraete');
INSERT INTO `list_reiter` VALUES (000240,'view_user','View',0,0,'Auswahlliste fuer aktive und nicht abgelaufene Benutzer');
INSERT INTO `list_reiter` VALUES (000249,'view_it_licman_instInv','View',1,0,'Zeige alle manuellen und automatische Softwareinstallationen');
INSERT INTO `list_reiter` VALUES (000251,'tab_parameter','Liste',0,0,'Parameterliste mit Typangabe');
INSERT INTO `list_reiter` VALUES (000252,'list_parTyp','Liste',0,0,'Benennung Parametertyp');
INSERT INTO `list_reiter` VALUES (000253,'mpi_notiz','Ablage',0,0,'Liste Lizenznotizen');
INSERT INTO `list_reiter` VALUES (000254,'list_inventar','Liste',0,0,'Import SAP Inventar');
INSERT INTO `list_reiter` VALUES (000265,'view_favorit','Programmierung',0,0,'Für den schnelleren Zugriff unter dem Menüpunkt ...');
INSERT INTO `list_reiter` VALUES (000266,'view_reiter','Programmierung',0,0,'Hole alle Tabellen von Datenbank von mysql');
INSERT INTO `list_reiter` VALUES (000267,'view_inventar','Auswertung',1,0,'Zeige alle Inventarnummern und deren Verlinkung zur Geraetetabelle');
INSERT INTO `list_reiter` VALUES (000268,'view_licman_lizenz','View',0,0,'Beziehung Bestellnummer zur DB Lizenzverwaltung');
INSERT INTO `list_reiter` VALUES (000269,'tab_instProt','Liste',1,0,'Protokoll Abarbeitung Installationsanweisungen');
INSERT INTO `list_reiter` VALUES (000270,'list_sequenz','Liste',0,0,'Abschnittssequenz fuer Protokollinstallation');
INSERT INTO `list_reiter` VALUES (000271,'list_protokoll','Liste',0,0,'Vorlage Protokoll von einen Host aus Geraetetabelle');
INSERT INTO `list_reiter` VALUES (000272,'list_bereich','Liste',0,0,'Auswahl Protokollbereich fuer Sequenzschritte');
INSERT INTO `list_reiter` VALUES (000273,'con_mainTab','Zuordnung',1,0,'Zuordnung von Ersatzteilen, Komponenten, Verbrauchsmaterial zu Hauptkomponenten');
INSERT INTO `list_reiter` VALUES (000274,'view_mainTab','Programmierung',0,0,'Dynamischer Auswahlfilter von Haupttabellen fuer widget:grid');
INSERT INTO `list_reiter` VALUES (000275,'con_ablage','Zuordnung',1,0,'Verlinkung Haupttabellen mit Dateiablage');
INSERT INTO `list_reiter` VALUES (000276,'list_kategorie','Liste',0,0,'Auswahl Kategorie fuer Dateiablage');
INSERT INTO `list_reiter` VALUES (000277,'sys_user','Autorisierung',1,1,'Autorisierung und Berechtigung Benutzer');
INSERT INTO `list_reiter` VALUES (000278,'list_role','Autorisierung',1,0,'Liste aller Berechtigungen (Rollen)');
/*!40000 ALTER TABLE `list_reiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_role` (
  `rolID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`rolID`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_role` WRITE;
/*!40000 ALTER TABLE `list_role` DISABLE KEYS */;
INSERT INTO `list_role` VALUES (000001,'NO ACCESS','No_Access');
INSERT INTO `list_role` VALUES (000002,'READ ONLY','view, list, calendar, view xml, show all, find, navigate');
INSERT INTO `list_role` VALUES (000003,'EDIT','READ_ONLY and edit, new record, remove, import, translate, copy');
INSERT INTO `list_role` VALUES (000004,'DELETE','EDIT and delete and delete found');
INSERT INTO `list_role` VALUES (000005,'OWNER','DELETE except navigate, new, and delete found');
INSERT INTO `list_role` VALUES (000006,'REVIEWER','READ_ONLY and edit and translate');
INSERT INTO `list_role` VALUES (000007,'USER','READ_ONLY and add new related record');
INSERT INTO `list_role` VALUES (000008,'ADMIN','DELETE and xml_view');
INSERT INTO `list_role` VALUES (000009,'MANAGER','ADMIN and manage, manage_migrate, manage_build_index, and install');
/*!40000 ALTER TABLE `list_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_sequenz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_sequenz` (
  `seqID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `bereich` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sequenz` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`seqID`),
  UNIQUE KEY `ber_seq` (`bereich`,`sequenz`),
  KEY `sequenz` (`sequenz`),
  CONSTRAINT `bereich_bereich` FOREIGN KEY (`bereich`) REFERENCES `list_bereich` (`bereich`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Liste Sequenzen Installation';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_sequenz` WRITE;
/*!40000 ALTER TABLE `list_sequenz` DISABLE KEYS */;
INSERT INTO `list_sequenz` VALUES (000001,'Prepare','Inventarisieren','Aufkleber auf PC existiert: OEM-Key fotografieren, zuschneiden und in die Inventar-DB als Geräteimage ablegen.\r\nPC mit OS geliefert: Erst Windows-Key mit KeyExtract-Tools auslesen und in Inventar-DB mit OS als  Parameter mit Typ \'Lizenz\' eintragen.\r\nAchtung: PC ohne Netzwerk starten, wegen autom. Aktivierung.');
/*!40000 ALTER TABLE `list_sequenz` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_sequenz__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_sequenz__history` (
  `history__id` int(11) NOT NULL AUTO_INCREMENT,
  `history__language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__comments` text COLLATE utf8_unicode_ci,
  `history__user` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__state` int(5) DEFAULT '0',
  `history__modified` datetime DEFAULT NULL,
  `seqID` smallint(6) unsigned zerofill DEFAULT NULL,
  `bereich` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequenz` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `beschreibung` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`history__id`),
  KEY `prikeys` (`seqID`) USING HASH,
  KEY `datekeys` (`history__modified`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_sequenz__history` WRITE;
/*!40000 ALTER TABLE `list_sequenz__history` DISABLE KEYS */;
INSERT INTO `list_sequenz__history` VALUES (1,'en','','admin',0,'2017-02-07 08:37:00',000001,'Prepare','Inventarisieren','Aufkleber auf PC existiert: OEM-Key fotografieren, zuschneiden und in die Inventar-DB als Geräteimage ablegen.\r\nPC mit OS geliefert: Erst Windows-Key mit KeyExtract-Tools auslesen und in Inventar-DB mit OS als  Parameter mit Typ \'Lizenz\' eintragen.\r\nAchtung: PC ohne Netzwerk starten, wegen autom. Aktivierung.');
/*!40000 ALTER TABLE `list_sequenz__history` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_status` (
  `statID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tabID` smallint(6) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`statID`),
  UNIQUE KEY `status` (`status`),
  KEY `tabID` (`tabID`),
  CONSTRAINT `list_reiter_tabID` FOREIGN KEY (`tabID`) REFERENCES `list_reiter` (`autoID`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_status` WRITE;
/*!40000 ALTER TABLE `list_status` DISABLE KEYS */;
INSERT INTO `list_status` VALUES (000001,'verschrottet',000212);
INSERT INTO `list_status` VALUES (000002,'in Benutzung',000212);
INSERT INTO `list_status` VALUES (000003,'Schrottlager',000212);
INSERT INTO `list_status` VALUES (000004,'auf Lager',000212);
/*!40000 ALTER TABLE `list_status` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_vorgang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_vorgang` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `vorgang` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `vorgang` (`vorgang`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_vorgang` WRITE;
/*!40000 ALTER TABLE `list_vorgang` DISABLE KEYS */;
INSERT INTO `list_vorgang` VALUES (1,'UpdateDB');
/*!40000 ALTER TABLE `list_vorgang` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_arbeitsplatz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_arbeitsplatz` (
  `tabID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `art` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `projekt` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typ` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hersteller` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seriennummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventar` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unterNr` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benutzer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lagerort` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestellnummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferdatum` date DEFAULT NULL,
  `preis` decimal(8,2) DEFAULT NULL,
  `eigentum` enum('MPI','UNI','--') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MPI',
  `forschungsbereich` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tabID`),
  UNIQUE KEY `name_sn` (`name`,`seriennummer`),
  KEY `lagerort` (`lagerort`),
  KEY `status` (`status`),
  KEY `projekt` (`projekt`),
  KEY `manager` (`benutzer`),
  KEY `art` (`art`),
  CONSTRAINT `mpi_arbeitsplatz_ibfk_1` FOREIGN KEY (`art`) REFERENCES `list_art` (`art`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_arbeitsplatz_ibfk_2` FOREIGN KEY (`projekt`) REFERENCES `list_projekt` (`projekt`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_arbeitsplatz_ibfk_4` FOREIGN KEY (`lagerort`) REFERENCES `list_lagerort` (`lagerort`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_arbeitsplatz_ibfk_5` FOREIGN KEY (`status`) REFERENCES `list_status` (`status`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_arbeitsplatz` WRITE;
/*!40000 ALTER TABLE `mpi_arbeitsplatz` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_arbeitsplatz` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `sys_after_del` AFTER DELETE ON mpi_arbeitsplatz FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'sysID' AND tabID  = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'sysID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'sysID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'sysID' AND tabID  = OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `mpi_gerWartung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_gerWartung` (
  `wartID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `gerID` smallint(6) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type` enum('Wartung','Prüfung','Kalibrierung') COLLATE utf8_unicode_ci NOT NULL,
  `intervall` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `letzte` date DEFAULT NULL,
  `naechste` date DEFAULT NULL,
  `nachricht` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bemerkung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`wartID`),
  KEY `tabID` (`gerID`),
  KEY `nachricht` (`nachricht`),
  KEY `intervall` (`intervall`),
  CONSTRAINT `geraete_tabID` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_gerWartung_ibfk_3` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_gerWartung_ibfk_4` FOREIGN KEY (`nachricht`) REFERENCES `list_nachricht` (`nachricht`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `nachricht_nachricht` FOREIGN KEY (`nachricht`) REFERENCES `list_nachricht` (`nachricht`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_gerWartung` WRITE;
/*!40000 ALTER TABLE `mpi_gerWartung` DISABLE KEYS */;
INSERT INTO `mpi_gerWartung` VALUES (000001,1,1,'Prüfung','05','2015-04-23','2015-05-23',NULL,NULL,'admin','2015-04-23 09:49:33');
/*!40000 ALTER TABLE `mpi_gerWartung` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_geraete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_geraete` (
  `tabID` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `art` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `projekt` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typ` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verleih` tinyint(1) NOT NULL DEFAULT '0',
  `hersteller` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seriennummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inventar` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unterNr` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benutzer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lagerort` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestellnummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferdatum` date DEFAULT NULL,
  `entsorgt` date DEFAULT NULL,
  `garantie` date DEFAULT NULL,
  `preis` decimal(8,2) DEFAULT NULL,
  `eigentum` enum('MPI','UNI','--') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'MPI',
  `os` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protID` smallint(6) unsigned zerofill DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tabID`),
  UNIQUE KEY `name_sn` (`name`,`seriennummer`),
  KEY `lagerort` (`lagerort`),
  KEY `status` (`status`),
  KEY `projekt` (`projekt`),
  KEY `manager` (`benutzer`),
  KEY `art` (`art`),
  KEY `lieferant` (`lieferant`),
  KEY `os` (`os`),
  KEY `protID` (`protID`),
  CONSTRAINT `geraete_protID` FOREIGN KEY (`protID`) REFERENCES `list_protokoll` (`protID`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_geraete_ibfk_1` FOREIGN KEY (`projekt`) REFERENCES `list_projekt` (`projekt`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_geraete_ibfk_10` FOREIGN KEY (`os`) REFERENCES `list_os` (`os`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_geraete_ibfk_2` FOREIGN KEY (`lagerort`) REFERENCES `list_lagerort` (`lagerort`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_geraete_ibfk_3` FOREIGN KEY (`status`) REFERENCES `list_status` (`status`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_geraete_ibfk_5` FOREIGN KEY (`art`) REFERENCES `list_art` (`art`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_geraete_ibfk_6` FOREIGN KEY (`lieferant`) REFERENCES `mpi_lieferant` (`lieferant`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_geraete` WRITE;
/*!40000 ALTER TABLE `mpi_geraete` DISABLE KEYS */;
INSERT INTO `mpi_geraete` VALUES (1,'pc101',NULL,'PC',NULL,NULL,1,NULL,'1234567','400100','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MPI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2017-02-06 15:22:08');
/*!40000 ALTER TABLE `mpi_geraete` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `ger_after_del` AFTER DELETE ON mpi_geraete FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'gerID' AND tabID =  OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'gerID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'gerID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'gerID' AND tabID =  OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `mpi_geraete__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_geraete__history` (
  `history__id` int(11) NOT NULL AUTO_INCREMENT,
  `history__language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__comments` text COLLATE utf8_unicode_ci,
  `history__user` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__state` int(5) DEFAULT '0',
  `history__modified` datetime DEFAULT NULL,
  `tabID` smallint(6) DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seriennummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alias` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `art` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verleih` tinyint(1) DEFAULT NULL,
  `typ` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hersteller` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benutzer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lagerort` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestellnummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferdatum` date DEFAULT NULL,
  `entsorgt` date DEFAULT NULL,
  `garantie` date DEFAULT NULL,
  `preis` decimal(8,2) DEFAULT NULL,
  `inventar` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unterNr` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `eigentum` enum('MPI','UNI','--') COLLATE utf8_unicode_ci DEFAULT NULL,
  `projekt` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `os` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `protID` smallint(6) unsigned zerofill DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` longblob,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`history__id`),
  KEY `prikeys` (`tabID`) USING HASH,
  KEY `datekeys` (`history__modified`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_geraete__history` WRITE;
/*!40000 ALTER TABLE `mpi_geraete__history` DISABLE KEYS */;
INSERT INTO `mpi_geraete__history` VALUES (1,'en','','admin',0,'2017-02-06 16:22:09',1,'pc101',NULL,NULL,'1234567',NULL,'PC',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'400100','1','MPI',NULL,NULL,NULL,NULL,NULL,'admin','2017-02-06 15:22:08');
INSERT INTO `mpi_geraete__history` VALUES (2,'en','','admin',0,'2017-02-06 16:23:12',1,'pc101',NULL,NULL,'1234567',NULL,'PC',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'400100','1','MPI',NULL,NULL,NULL,NULL,NULL,'admin','2017-02-06 15:22:08');
/*!40000 ALTER TABLE `mpi_geraete__history` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_lieferant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_lieferant` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ansprechpartnerID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefonID` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `positionID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ansprechpartnerAD` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefonAD` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `positionAD` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `strasse` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plz` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ort` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stateBundesland` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `land` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `webseite` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kundenNr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bemerkung` char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `lieferant` (`lieferant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_lieferant` WRITE;
/*!40000 ALTER TABLE `mpi_lieferant` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_lieferant` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_matMengenfluss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_matMengenfluss` (
  `fluID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `matID` smallint(6) unsigned zerofill NOT NULL,
  `gerID` smallint(6) DEFAULT NULL,
  `fluss` smallint(5) NOT NULL,
  `vorgang` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `projekt` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bemerkung` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fluID`),
  KEY `vorgang` (`vorgang`),
  KEY `projekt` (`projekt`),
  KEY `tabID` (`matID`),
  KEY `gerID` (`gerID`),
  CONSTRAINT `fluss_material` FOREIGN KEY (`matID`) REFERENCES `mpi_material` (`tabID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_matMengenfluss_ibfk_2` FOREIGN KEY (`projekt`) REFERENCES `list_projekt` (`projekt`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_matMengenfluss_ibfk_4` FOREIGN KEY (`vorgang`) REFERENCES `list_vorgang` (`vorgang`) ON UPDATE CASCADE,
  CONSTRAINT `mpi_matMengenfluss_ibfk_5` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_matMengenfluss` WRITE;
/*!40000 ALTER TABLE `mpi_matMengenfluss` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_matMengenfluss` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_material` (
  `tabID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `eigenschaft` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `beschreibung` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `min` smallint(5) unsigned DEFAULT NULL,
  `einheit` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nachricht` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seriennummer` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestelldatum` date DEFAULT NULL,
  `lagerort` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`tabID`),
  UNIQUE KEY `name_sn` (`name`,`eigenschaft`),
  KEY `lieferID` (`lieferant`),
  KEY `einheit` (`einheit`),
  KEY `lagerort` (`lagerort`),
  KEY `nachricht` (`nachricht`),
  CONSTRAINT `mpi_material_ibfk_1` FOREIGN KEY (`nachricht`) REFERENCES `list_nachricht` (`nachricht`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_material_ibfk_2` FOREIGN KEY (`einheit`) REFERENCES `list_einheit` (`einheit`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_material_ibfk_3` FOREIGN KEY (`lieferant`) REFERENCES `mpi_lieferant` (`lieferant`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `mpi_material_ibfk_4` FOREIGN KEY (`lagerort`) REFERENCES `list_lagerort` (`lagerort`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_material` WRITE;
/*!40000 ALTER TABLE `mpi_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_material` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `mat_after_del` AFTER DELETE ON mpi_material FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'matID' AND tabID =  OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'matID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'matID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'matID' AND tabID =  OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `mpi_material__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_material__history` (
  `tempID` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_material__history` WRITE;
/*!40000 ALTER TABLE `mpi_material__history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_material__history` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_medien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_medien` (
  `medID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `typ` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `seriennummer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ablageort` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benutzer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statID` smallint(6) unsigned zerofill NOT NULL,
  `beschreibung` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestelldatum` date DEFAULT NULL,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`medID`),
  UNIQUE KEY `name` (`name`),
  KEY `lieferant` (`lieferant`),
  KEY `ablageort` (`ablageort`),
  KEY `statID` (`statID`),
  KEY `typID` (`typ`),
  CONSTRAINT `mpi_medien_statID` FOREIGN KEY (`statID`) REFERENCES `list_status` (`statID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_medien` WRITE;
/*!40000 ALTER TABLE `mpi_medien` DISABLE KEYS */;
INSERT INTO `mpi_medien` VALUES (000001,'HDD-0001','HDD','1234567',NULL,NULL,000002,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2017-02-07 07:24:49');
/*!40000 ALTER TABLE `mpi_medien` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `med_after_del` AFTER DELETE ON mpi_medien FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab = 'medID' AND tabID = OLD.medID;
    DELETE FROM con_ablage    WHERE auswTab = 'medID' AND tabID = OLD.medID;
   END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
DROP TABLE IF EXISTS `mpi_medien__history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_medien__history` (
  `history__id` int(11) NOT NULL AUTO_INCREMENT,
  `history__language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__comments` text COLLATE utf8_unicode_ci,
  `history__user` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `history__state` int(5) DEFAULT '0',
  `history__modified` datetime DEFAULT NULL,
  `medID` smallint(6) unsigned zerofill DEFAULT NULL,
  `typ` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seriennummer` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ablageort` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benutzer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statID` smallint(6) unsigned zerofill DEFAULT NULL,
  `beschreibung` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestelldatum` date DEFAULT NULL,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`history__id`),
  KEY `prikeys` (`medID`) USING HASH,
  KEY `datekeys` (`history__modified`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_medien__history` WRITE;
/*!40000 ALTER TABLE `mpi_medien__history` DISABLE KEYS */;
INSERT INTO `mpi_medien__history` VALUES (1,'en','','admin',0,'2017-02-07 08:24:49',000001,'HDD','HDD-0001','1234567',NULL,NULL,000002,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2017-02-07 07:24:49');
INSERT INTO `mpi_medien__history` VALUES (2,'en','','admin',0,'2017-02-07 08:25:36',000001,'HDD','HDD-0001','1234567',NULL,NULL,000002,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2017-02-07 07:24:49');
INSERT INTO `mpi_medien__history` VALUES (3,'en','','admin',0,'2017-02-07 08:25:43',000001,'HDD','HDD-0001','1234567',NULL,NULL,000002,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin','2017-02-07 07:24:49');
/*!40000 ALTER TABLE `mpi_medien__history` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_notiz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_notiz` (
  `noteID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `sysID` smallint(6) unsigned zerofill DEFAULT NULL,
  `gerID` smallint(6) DEFAULT NULL,
  `matID` smallint(6) unsigned zerofill DEFAULT NULL,
  `stichwort` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `notiz` text COLLATE utf8_unicode_ci NOT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`noteID`),
  KEY `sysID` (`sysID`),
  KEY `gerID` (`gerID`),
  KEY `matID` (`matID`),
  CONSTRAINT `mpi_notiz_gerID` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE SET NULL,
  CONSTRAINT `notiz_material` FOREIGN KEY (`matID`) REFERENCES `mpi_material` (`tabID`) ON DELETE SET NULL,
  CONSTRAINT `notiz_system` FOREIGN KEY (`sysID`) REFERENCES `mpi_arbeitsplatz` (`tabID`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_notiz` WRITE;
/*!40000 ALTER TABLE `mpi_notiz` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_notiz` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `mpi_verleih`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mpi_verleih` (
  `autoID` smallint(6) NOT NULL AUTO_INCREMENT,
  `gerID` smallint(6) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `benutzer` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ausgabe` date NOT NULL,
  `rueckgabe` date DEFAULT NULL,
  `bemerkung` tinytext COLLATE utf8_unicode_ci,
  `bearbeiter` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`autoID`),
  KEY `gerID` (`gerID`),
  KEY `benutzer` (`benutzer`),
  CONSTRAINT `mpi_verleih_ibfk_5` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE CASCADE,
  CONSTRAINT `mpi_verleih_ibfk_6` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `mpi_verleih` WRITE;
/*!40000 ALTER TABLE `mpi_verleih` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpi_verleih` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `logID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`logID`) USING BTREE,
  UNIQUE KEY `login` (`login`) USING BTREE,
  UNIQUE KEY `email` (`email`),
  KEY `role` (`role`),
  CONSTRAINT `sysUser_listRole` FOREIGN KEY (`role`) REFERENCES `list_role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (000001,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','MANAGER','','import','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `tab_ablage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_ablage` (
  `ablID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `file` longblob,
  `file_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ablID`),
  KEY `kategorie` (`kategorie`),
  CONSTRAINT `ablage_kategorie` FOREIGN KEY (`kategorie`) REFERENCES `list_kategorie` (`kategorie`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tab_ablage` WRITE;
/*!40000 ALTER TABLE `tab_ablage` DISABLE KEYS */;
INSERT INTO `tab_ablage` VALUES (000001,'Technische Information','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\"\0\0�\0\0\0�\n��\0\0\0sRGB\0���\0\0\0	pHYs\0\0M\0\0&���e\0\0\0tIME�#�T�.\0\0\0tEXtComment\0Created with GIMPW�\0\0 \0IDATx��]w@��s ��t��XP,�X���1�X�F��$\ZK�\n���h���Q�h,D�� |UPi\"\n*J����cv��\n�b�w_������gھ7�y���0\0�#@\0`\0\0b~�\0\0ӿ1\0 �0��]�6�\0\0!�1 &\0$ @���~WQ\'E9��f�\0�1�+�)@)�4}=}\nSE���i�5�\' $���}��Ž�C���xh<4\Z���8���:�����c������H��m�H^J���xh<4\Z���C���xh<�7	\r���12f�#�\nc!��B�?���1�IQ#�ؗ�B�\"��霈��F\0�wi�! R��B�x��[L�F���C���xh<4\Z���C����44T[[���~A�(�g*���iz	�i�p�O��T�M�R�򨾯34�@���O<��O<���?�0ƭ���Ӣ\n-J1?i�0P�9�xH!��\"N�b\r��aD ��6�\"W��ؐ!VZ�(]�`��#��3G\n���9�0�dh8;��%���b�^\n95h� 4�\Z0�0iѕ�/ĭGC�؊p�MaF��k<4\Z���C���xh<��%hF��.���ޭ�����\Z�dD�V�����~�Ԥ����Yy�#*1��֎�ְ��#@1�$�������:�0NS�~��!��ͭ|�cǎmڴ�%]�x�\'�x�\'��M$�J<x��ֶSǎS�����{�����0G���\'[lb��\"Q���W�X+%���cFn+�\"V^���R�%*�EM��\0�z������֖3x�\'�x�\'�x��������ߧe�V���1��a��)��\0�	��0��AX��r(V�B�P��q�ge#��`M��h](`��0[>@��D�A�\0�@ῂ82L�	\0���!/c��O<��O<���6m�\Z\Z�	�p+r���i��V�\0\0�1�:~`��hn�x�S��;�6\"�`�U\Zp�\"��}�ͥY\03.D�3)�q�g��fu9�q�`�Ј�<B��7O<��O<��O�NBa@��n=�__�,	s|?X�N!lɬ	\'76\re{(e����\0�\Z&C�FS�*LR�j7��\00´Y�`��C�\'�x�\'�x≧a�O�<y��WW�?�@___��L8����Y1!��aۙ� �m�0�BJ���l\\AFGA^�J.��m�B�>h�b���EW1�r��� �T���7|Nnn\\llnn�W���ͽ��FzzzP�B�ڵs��c4�v�a\ZiWmڌ��������֚�������ׯS�N���\'�x�\'�x��/��޼ysɒ%G��{7�ۻ[�2Ba�l{�1�@a������\'�:LQꜿ�B!�O�ؔ���o���?��>}��Y�hM\nc��8�s���ɜ���?aC�%B��ُ?vqq�ҹ�P���;��8AJ����c�PMu��-[�\\�bfffll,*++/^������_8p�ȑ#,`ޣ0W,��¨ḋ999qqqC����700�H$eee111r���˫����Q�\'��3KKK###?��S������F�jO�z�W���Z�=k��m.��nݺu��5��f������+����ͽr�JII�������ȑ#����@��p����I&�]�t);;���������O���oB���<y������-��3���KHH(--������033#�����]�VPP ��mmm��Ѵ�1�v�ڍ7***lllܭ͉Ru�ƍ�����\n\0����իW�>}�z)�=JOO/((x�򥉉I�v�|}}�t��&���:�|^^\0t��aԨQ���\Z+�h��y���-�%�>x�@(���v���Ν�Z�k�U9.{�\0�߿/66V�7W L�6m̘Ѵ�����1V�W�\r�\00�0�a����LGG�?��s�̙lu8q�\Z`�⸤�ci����0B��UU.��/^��}�v�.]��C��#��r�\"G}}�ҥK+**�����􌍍M�Lk�k����ryzz�G}T[[koo��vX�p&~\"W��؎���C�533۹s\'�3c�����f�xu!��Œ��\\OOφ.y��_F�#��3��k�,99y�����r���ŋ�O��<y�?�G�%�Vj������ڹs�\Z������?���K�.s��\rWy������\0\0�w�ޑ#Gf͚E,^���+��N��������������^�z5� \08s��xڴi���eee/^dŌ��ϗ���\Z5�����+qqqϟ?=z�z>�Ν���0`����D\"y��q|||YY���?\0H$����gϞ�ƍ��ׯ�۷o�\Z��i��76l������C�v횒�\"��\Z��by]��o���iii�Q�$$�J/^�8z�hFQp����<����A��7m���\n�R�t����{ �p �	�2��H0.X\'�_Gt ��M������Sii�;w;w�1&�\Z�x*\n\0E�HGI��h�.�`�O[��۶m�w�}�w��zz�2�4===**���X\"����J$�>)W�Y��J�6�O�\Z�߿ɒ%\0��<}��4srrF�\Z���k����;���\"v�7���7-��?��Ν��1bď?�ȏC�ݾ}{���D����A���Z�hQC�G����C.{��	\0���\0���o�1�p&�����ޑ#G��Z\n*((x���|@.����O��>�����O���ȥH$�<yrxx�F1c����occ�:X[[�޽�����...�\r\"	\r\Z��ٳ�7o���[�Y5Z�_8l�*i�^���*)y<i�$\0���311��~�Ν3��b9����ɓ�^�\nZX\'#G��3R���UX.�PINQ�6m���tss��N�<Y&��R	�Zq�.7ą\Z;�U\0\0\0�g璒�W�^��ۗ���ܽ��兕��0kt�P�ܿ�f�Mss�/�����J&������\'???55��Ɔ�(�T\ns��Ŝ W��6[sss��ZH��ӿ�{����>q3.���\'ދ�(�@�\'X����_�\"y�ɼu�N�6��C��$�+�\r׿�.8YR]�F��׾����X��/_VUU����_\"�jjj�,Yr��q�@0w�܍7�����ǎ���AAA[�n%b1I�t��\'N���\r\Z4�~�ڵ+;vپ����bŊ��$cc�I�&mٲ�[���چ2Q��Q���\0%%%�}�ݾ}�lmm\'M�fddDݺukŊ)))���\'N���MMMul����+Vdee988s�6--��D���v�ʕǎ{��)�R{=[��TnK��;��1�]ĵ����!!!��������Ֆ������۷oyy�ѣG.\\��h`��7���]�Z2�nj������w��u�ȑ������������2���VVV#F��n�x�bʔ)\ZѭY�����w�ܑ�dB�0  ��ΎM@QTC5l�]�nܸabb��k��/_��|��[�LLL�t�2t�Pv�I�R]�m��t�ƷF����\rG^����}�vMMM��#�cbb�ܹ��ի����e2Yll��۷)��ѣǨQ�حJ�M�P�4��\rU�������IFtb�8111\'\'��˗zzz������cM-�����LHH��˫��#�I\\k\"-���TRR�2I=<<N�8AČ��\0�#��/_6����t-5׸�����zzz�J���;p�@�S���d�bF��6c�h��\Z�Z� ��7rss����β�������q���<����\0���YY�2�����N��^m۶%����N�>s�\\Y��r���������l�+N���q+�c��q�ƍ?^��2�X.�fX��W3؈��7n�u떛��@ ����:u�L&����xl�r�p7�)E;oc�7q\Z�)l��]\\�����ۗ�����xzz�Q	�ە�c&@(��E\"�;���B*��p�`�޽/^trrR�[�!�QG��7�I+s�Z��o?K����m̻:�8�?����v��&u�B�\n����B\'|�����_��GGE\r������`�3��s��t2uz%}���Jx�vV�P�\0޻w�S�N��D�rѢE�޴iSyyyPPЖ-[n߾=hР���WTT�����g$�G}��[o}���\0p���aÆ]�r�}���j���\r>����ڻw/\0>|���>�&�%���^�K0`@hh�ƍ�?��_~����}�i؀���������͛7O�f��͝8q�m�ƌ���3u��:2����#F�_����\Z�g�uA��mIs��pa�It��Y��3g���=~�8**��Ĥ[�nm۶�u��\r�\nQ������\Z�=C\0���طo���Ǐ\0���QQQ�ϟ??v�X``�������###mllD\"\0�3f׮]��ެ���[�.\\����(������w߾}}����u�5l�]B������dwS��T�;���3dȐ#F������_�ti��ẗ���t�ƿ�Z5���w���:6���P��={V$�1����?�HMM}���P(6l�s��u��5�t\Z�D{;k�d�~�N�<9g�vG�Q:y򤕕Ռ3���%Iqq�իW	���Mj����={����gȐ!���O�<IIIaG����JԐ�����b����.]��ݻ���D 8;;�����d\0�۷oddd@@\0Y@JKKccc5\n*T___VVO�\"\0x��1Sg����ٳg\Z_o��f-�����ux��ѩS�F�M�byyy��GPP���s�\r-�\0���SS�֬Yݐ(ط�o߾��;_���C�Ő�4�n�s�c�T���3^J�?F\\���gO�%VHЏ?l!2���	EQqqq����(�[3�@0s���AA�-�a���v1[>)�^n�G��w###333�P�v�=}� �A����5�\0A\\�� @�aq���9EQ={�����,\0t�ر.������c�zc��K��,�Qt�K�w~e���ƫi ����ȝ�1˾\Z�Y��4����}h�0Zu�4x���믇M�8E���e�yq������?�W}{m�o����G�3K,,\0���f�hlC�b����Q��ٳ�Y�\0���z˖-6l�={6�>��/^�2��;w��s�J��yUUպu���߯�^�h�l�ҥ�����d�z�ky)���䇙�ٮ]����CČ���z�ҥ��z��ŕ��:6����W�Z5m�4��nݺ���g��f�\0ooo�-g��V���&�ے�R�-!SSӡC��:�ѣG_�t�[�n<v옷�7���d�����c�j�CC��K����ۯ_?���			�III~~~���ĂbԨQ)))�abb2f̘��h\"�EGG�9�u$U�v�ڱ����O,\'$$���{�ְ�w�#66�W�^*����ҥK�]̱c��޽�e-W{��j�Z5���gooO��X=GGG\"	�9r���Ç\'�9����Ν#\\���S!��+)�Ɋ��;w�9~�x�]����_�l�����63�+ФLHH���<x0�tuu�:u�.��%#�%\'\'������у�\'XWW��������T]]}��退\0�.u�޽ÇO�6�H\Z�.--%:4���}�rѵoߞ]{���T����\Z5Zn󆍖���\"����ݻ���\'\'\'�j��$Z?Q�۷����ХA�߿/��Y��qlPK�S(��hw��ӛ2eJ���V���>f϶U�#A��tWWW��@ ���611���*ҏX,����>}��1U�R������NNN�ǘu�����M[�������3ٲgcq�e ƛ�.zzzE��U�>��^gE%�_\nN�o�D.�_v����ŸF�}\r��?H�b#}ͶA^���o9��_u�IK�T�U.���g`g�.��,_��l�}�Q���T�\0�G�[�n555�;��������*U��?T_�����f6K3f�kR&�W��R__�y��C�=|�����ؾp�·�~����>֥Y�\\�¦�A�q\rm��\0&L��rG{=[��Tn��K}���Oƒ��Kyy9�����_���7n�066&�\\k�CC�}2\r@�=��H�	\0B���ٳ쥧��ݻw��҈�۽{w-�T�z{{���o�԰�w	���o��{�B2�,%%%++����h�kr��6�Dͳ{ny����ڋS7��=����T*�ޱ����.MפvV��ŋ���������>��{���ڒi���߯Y��!a,66vȐ!�\ZV�\n4�<x@vL�4�[2�����	\0dݸw�^\\\\�:+R]]9v�X�Pؼ�0��N /^$�GW�\\y���ԩSI�EEE111���o���v����&???..�����ё��ۼa�e\0h���ɓ\'����;���)))�:H�~\0����:u�S�rssǍ{G�QEQ/&����;�U���.�O��Wf�/\\�`ee��<!!!00P���(�tr|`@(000;;�!�(��%���^�1gBtT<b�L��H$��~�ɱ�t�r�(����355#�d2iƗ�\"}�\0`E��LMMkjj^�x!׷�o��a �H$X��W�z�3BSO�Jx\\�S2���Jk���`\"��(JN5�!2W�\n�6���h�Ps2uR�|VG+#��L��ˁ�Bs�[�ƹ��>i�4�����d7��Mƴ��=(1E�^��C�`��L�eCJ�֨��٩�{)�V����9|�������i}}=;ɟ?����-�{��Y���U�۸s�I-��~�ѥ���M*��ͥ>�[B�{\"�3W__O~8����z�200��III�*CK�dX[[��cǽ|������\Z�\00|��}�����3gj�^Jmm�.5l�]\0x������[��w7>>����\'Nl۶����L&�\n����K5�Z^�&���kRz�3�rG,7��\'V�dvv6ag����}��C��?����իW��M}�L�8166v۶mVVV...^^^���� -hj���j��t�-\'ww�ɓ\'_�t����8v����x��>x��ȑ#�5f����De����8w��}���	&�� ///##�S�Ni3؍|ooo33��\'O���.��Tu�K��mް�2\0���E����\n����V$Z?�V�~�it������\'j�?_�|YWW������i��X{�\0M�4�ڵkڏ�	�l�<��0��J�b�)j֬Y������622Kľ��S�Nc�E���e2�T*�}(�\"˕Ϯ%N$r��d�1.).���161������rrt��e�B��7�@!b�j ��:�LO�_NN��O�R��	�\0QG@q�뺎1�;Ҹ�b����J�J����@J�)ܰk�@K�q�c��^�3nG/O\'�rqkeW7��?S���=����rπS�Ԉ]�����\\��Ǐ�k�N%����z��f�z�k��Y�R\"##�^�����*7���<y��<y�D�f������j��ւ��ʪ�����(\rT5�M��l�.hR��n.��҂�}��VVVW�\\2dȕ+WLLLؽR-uВ���ɫW�����^��2�.�b9���4t����ګW�j<\"���J)\\wm����w��R���eK�Ν;���c�\0v�]�rm��Y/��VMjC��)�t���3��ѥ{b�Jr�g{{�I�&<xp���O�>e7,����ښx�>����$)))77��w��^��6����˗/Y�Y�g+��)\0�D��fw(��Z��8p�@```Kd�;���˗/��t��O#��NN555lA�/�}ZVV�P���mް�2\0��uCdffVSS�\r�QSS��\Z�D���J;ӦM�y�������r���޷o�P���p�\Zd�)��yg,qlXMR���pp9\0J��x�(�S�L\Z4#\"bvvv�6mbcc�={6o�<6|�X�\rXaτ�*�M��J���¹���ն�i[��������Q*����9`����=�S�߀W��LLL:ԭkW;;;�9�K �{��$��0B�6o655e���f�U�\"��ƱB�r��#=)%��7�\r��$��@`��\0�w�>k��\ru���aw��m{L��~��Bu1������6L�e3�СCk׮�^�f�,�1B%ف��I�U��TWW���q�J�\rv�ر/����s��a����?**��?n*.���T�V�Q�\r��l�.hR��k.-#��\0\0 \0IDAT\\___.����-!??�S�Nu��555u�رd\ri�,�СCvv6{$\0deeqّ���>}�h|7??�ŋ���{����k9!@����l���]�+��J�\\�$##��u��D�T�5jդ6�^�:55�f���So���Nvvvyyy������DDDP�`��F{�������s��[�n%���\n4�A:v옙�ټobk�yz��������lDDĈ#Z(c\0@�Ν՝@X�������\\W����7�����9\0xzzfffv�Ё}����P� ��6o�h\0Z�Z������޽{��;�EҮ�]�D��W!6��ʧ����s��\r���Wf��2�T&���3fO�s�:�/���>��g(9%�I�����y��b��/�ڵK��0�`���0P��0`\nӗ�p�(&cLQ�4�\0$���>���ܢ��������I\"�)|�i�E\'�TD�\0���եg�^�\0!�|Ŋ��X�D\"�b�sgW,_QUU����VV�i�@�<����|X�y�bEI$�&�H$*�)�S{���ɱ\\�\0�yq7<{{YmYYmYx��]�;������A��xT�HF�JkK��q�� ��]\\\\laa��[��f�ݻw�o�^VVVVV�}��;v�����	\r\r��_�d۶m�w���A�교�,YRZZ����~��u\'��nݺs�ΧO�>}�4<<��ݻ:6KhhhXXؾ}����kjj.\\�0f�]p-]����>�|�rmmm^^�W_}���s��׳���I�6����pkk����7{�6��677߻w���)����Y6dȐ�W�^�z��������ի\\�!C�\\�t)33���V\"����<x�}O�>MDM��;��u�T��={��[ʵk�X�F��wu���wG�N�bbb���kjj�\\��r<�.u��D:RkԪIm��8ujjz-c���So����#Μ9s��M��-�˫����ͥR�;w\Zz��~���z��%EQ��թ���e��\n4�A���333+++e2٣G��9o�<����\\��ŋӧO����lwDDĠA���FY�^�zݸq�֭[���������qqq��߿�ɓ\'sss������rrrN�<�J;*EDD���������/_fddDEE�{I�z�*..�|�r]]]]]]RRңG���ap��^n󆍖����|p���욚������섄�f�o�$�2�U(##�ۻ����@ ��훚��YG���\r���88s��3f͘1#hFЌ�o�\"h֬�gcb u�_�1>R�=�~ �J�A3��~������|����?����z�P�m�����L�C���UU�vvv�z��������=6K�!E�&&�����>�í[���_��_�MLjkj\r\r���\r\r������RXٵE)�-�!԰6C\"�H5h3�T�����Ϧ��Ha99�V�\0�ٽ,��k�b���q�q�,;�G!}�vf����\ZY��������а1�ڃ�޽���?\r\r������ر���P\\\\܊+V�Zedd������?�9t�P�2i�교�o���g�u�ڕ��	&�ݻ����b����+V�X�l��u�V���f�ڵ�s�V�^�駟�d�~��q]ܴ��:u�����Ν[XX�����G����޽[��l�.hR��k.-#|�ȑ�Ν;z�(��umC\"�=l�0v�o�,����1cF\\\\ܟ����߹s�q��egg�{{ӧO���?ϝ;GQ7�ٳg�w�nccC.۵k���}��ى\'j,�wމ��IHH���nnn3f�`�m�Z���=z��s�v�؁1���\Z?~<��Х\\-M�֪Im��8ujjz-\n���N�M����۷�6mZbb\"9�������������q߾}r�\\���СC�^�\Z#�J-,,:w�̎g-hj�XZZΝ;��wﮯ����{��F�$�G�=��㏊�\nkk�>}�p�s---=~���\nI�æ���?y�丸8Ҟ����&Mb�$}��555�|��ɓ\'�m۶#G�l(����ߵkע���b�������)SX+bCC�Y�f�?���ˤ�gΜِ���r�7l�\0-}������:~����$r���������=_/i�*��nݚ:u\n�d/_���������{���V߭[�<{�L�h\r!����!���<�ҥ�Q�\0�r������n\0U+tB�i�����ׯ_�7pT` ���\'`\\��젘#�D��φ�`������^����tvr�J���\\q*/#�(N�b�2��&��###322�nA.�===�>}zO�4	Pr�m���N�FF9w������\r\0Ւ�Y��̛8�ZZ�޾fml�s���?9mf`�\Z�Lc$�f��\'Nd|*��`��α���Ջ��_�bEff&���uճ�]��ۧ�#�_S�LO������߆<���k�{���Ĝ��O���O�8^\\\\ܿ���l��)S�8::FGG�lBb�8�֭�^^��z.��#� 6����׵�G���[o�~\r,+���_��\0Q>PH$�I�&M�6U*�I$Ɠ\Z���¬O�:�m����,@��hE�]S�����\"��ڕ���E\'�\r	�` \0��c}1�*EQH �4c�a�7n�F������������K�b�\"\'V��0Fq����qԽ�\0	m�=֠��Ȯ��Ý�>�駟j�������-[֣G�D����dɒo�����+��0��Y�O<��SkӣG%��.�?>z�����\'�|bee����gϞ���:$&&�r��=�I��Wa�1%3f�F�.I�R�T\n�	�\n�_)\n�ª	q���=�)��߃���Zx1�Ud��c�D��\'N��p�B�b�u�fjRW_�����w�!~\Zc���Ri=c��M	4󏺘�\'�_�nX���5G	����\\O�Ϗu�X\n\n\nZ�jUvv�\\.�֭ۆ\r�O�����><��O<��w&���G��������Ϗ0�mڴ=z���WLLL]]����dO���\r���/YIP��tW����	��\0�N����s�H�	:R b���\0E�}::��C�ZU��Tt�\0P�E������4�\"[&�X��h舶k4c#�����z�Tqɧ�\\\"�)��Sk =����aCQ�y≧�-5d����ď.�x���JR�4##��˫���9�\'O���0�<#�p��3\'�&��eC��ڨ��H.LY3,6�V�OҐ	���Ќ��sss{��~�;O<��O<��O<�H&��L����ɜF�*��>�H!�5�\r�M;r�\\;&�[aV��W���a�z!V��u\'~�(ls�=�W!<p��na��t���\r�����X��<��O<��O<���\"��\\.��ح���ӑ(H\"�(�d#\0���\0(ĸ^c��̠�*t��(}����{�\n8� ��0�16O\\L�\n(\0D1J�J�sF�{Aѡ���M�FQrss���9��t~�ѻx�\'�x�\'�x��/\'�\\~7\'���J.�\0Z��G%%%��U��NtR2��DK/tvQ��A�=�V988��P�Rp���dD;�#N$\r�B�\r]Ω\'}Ӡ!�����?{^����\r�o��/�5\Z���C���xh<4���=KK�v���Չ1{�Q+@C%�J\0�pwt���V�`<X�%�����u-�ua͞��\n�}\'��j1f���@�LQ��6���0$6V���ThH�\r\r��􁷙�\'�x�\'�x���FX&�����V���1\'���*�h��r�\\�y��ߊ ߈����B�_\n�)�4(�	�-q�eꍀ���bUa�s��i\"4�0�\r�J�_�9_W9T G�Q	r�9�����@��O����.P���p���C���xh<4\Z�Z�q��I�<�/���{�,;V?Nё�0]9v��X`L1��̈�s4/IO<.20�/PJ�10�ZQs�}SQ7�҉�C���xh<4\Z���C����<4T��Xa��,�po*V�$q�!��K�e(I`\n�5�R19㸫+L֐�c����h���C���xh<4\Z���C����04���![�9���+s�]A|(0f�PО�;�+8��#s��L&��]ցs]81���9��,U�<4\Z���C���xh<4\Z��CCs�ҙ�o7�˺�4�����6��#����Hޘ���D�+W�R��V���B�2\Z\Z���C���xh<4\Z���ơ��*��Y)F�\'�x(e�s�o ��Hs$-�(��&�(G�\Z�������n.�����?z��\rT���z�\rZ]l��ȑ|���xh<4\ZMGhV�XA�п(Ll癛Rl��\Z�/��O�J�g���$��c�0�v�\'��gGOb@혧ώ�`Bs�(l[��&L���<��C�8qcgF�f��\n�v{�0\'|8)��..��\0���򨸘��WNK�,47���������܊>l!4��??��\ZQ�!&\nb��Vh��5���â���kڡ	��>|���Zi@6-w�N{{���\r����&>,*z��M�ZRb��J2xР�\\ka�	�\0P�&t=,jj���ŷ�՗���e�F��e5��������M����߱B�@�w=���ܞz��ٞ��!�7ӗ}�EAA�â�76�0Ɠ~w��S��0%��o\"j�?����/��\'���]kmh����+�X�T,�~�M�9�L�҉�C���bE2��s��7�{)��Mž��Bn\"��PH\Z�����\\O|�A��\r�\Z���X��t��&t+**�k{Mh̀�	�P(,**��kB�[QQ�����}iǎ,�>�o`c]��<�{��0�\Z�5h�|3�b��6y@b<y�d�\Z��o�]JL<x�?��p����\\�\ZB���2\0<x���r�?�׸Ǽ����{�Z\0���\0P��}��^�>�!��6lX�jըQ���\\#�\nr,V�̰�j��d�+��>+�!I��\\Y��U-�c}t.~���M�7���ˏ�ϧ�\r塬z!�B����0�i\\/��5�<�Ь�z��G�[���u0\"./�I�8ꫦA����SE�H�]�u���EQ%��ɏ���m۶? ���{�6m�������������A�\0(**j4��_[�����M�6�\\���X�!ŀ�&�04�k�E��{���7�k>>>��g@b��@��5����\'���.*�f��cG�ЄBQ�N����L�O�0|��a�EBi�v��\r\Z4h�ڵ6�6�hqKe@��Br4������۠A����Ȧ��vh\"�����!h������{��,#��P(�B����}@�\Z5�s6��^�H��\0P_/&E��q�:u��HC����r%��А@[0��ݻ	4q}��o�߶m[�皐�nkk�={����/m�EEy|���iii���^^^.ȅ&	�_d����5��k\Z��,#�[@暖�:u�:\\�����^V}����G���A4�HH�\Z�H(,,,�D\Z�P(v��).>N\0\rSx�2m1`�HTXX�����B�s��R6u����������H `�P$,**l�O��^�@�#��kǢ�\0@�\0U��h�{������,�>�-�d��nZNN����0̓�Թ�1�(�T\nsBP\Z&h�gO�m߾-!�Rii����OϞ�f��������O��gO�m�v��RO��3g��:�������]�WB�g�!����YU	).++�{��g�)d��)q \"88���~���<���Q=�Pǀ\"r97VR�l8��$T�M��u����?�<%%eǎ.3fLLL̎;&L�0i�$\0����t��ҥK{����O?\Z���I�J�>�l%)aݡ��)Ј\\AQ�������~��{J��r���fEM�G\n\n_`\n�\\�������h�\Z����<\"�-��	z�< ���\n�{�y�0�xL���ݐ�|Yַ�z~�q���<%���%\0��fM��G��{��ɱ��F#4\0066�p����ظ8�Daa \\����o�Y�n��۶)Th�\0�\n1�ںڂ���Ǐ�>qℋ�K#�p�Ѐ�Y�4b�N�Aaa!  �CCC�y[k�\Zw@jP�\0����O�<��?D#�^�z���رc\0�����?��+�mll\0plll�޽��� �M�kPXX�TT�ػw�eˊ\n	(�HTXXHjU��?u�������mw���ݻw�:�WQa!�ľȄPmv���^�J�/�D���^S�S4��i�w=�*�_�w����#ܾx^�Ԡ)��2�H���H�\"����8N������0l�-.����f��0���A[[�;w��޽;0p��\nək�)�����V�#�Ϝ1S1��V�dk�5�+��������u8V�^`\0�gE?|�{�AckkjZ�����z��������hM�kSEa�����e\0��l��]lL�cM�J���?qb``�޽{]\\\\�����o���������ך��.-{����*���߼�N��TF�\r�7Ǎ��	�6�(�d�Dר|QYY�����$P< ���1ޥK	����fʹ��NL��nɉ��];w����ǧGHH��^B�@$�ڵ�w�>=||BB�%�b�Qrrʘ1c<==��>r��\nҨ����ЫW���H����\0�ٳ���\0,,,\"##�N�ڱcǳg�v��	��E�H$k�������g׮_Yh\"�z+��@��D�����) `ĵ�׏;������1a����L�صsg��}|||��C$	W]�\ZEQvvvE���XYYY[[������._�|�ʕ�V����k׮uuu%��B����ZHhhg/�	�#��8,��8p��G��ׯ_��<�?����c�yy�~	\0��E��K$\0(,����ӯ_www\0����x�B�>����/�(���k	�.YZZΚ5���:11���I$���`�>������+�TL%�zxx\\�v������C<==Ǐ�� /�H�H��������<==\'L������\Z)*%%y̘ў��~o�9r�����F�$�>��󔔔����O?��W/ofd�^I���lt@r���a������\0�ЀܕTV���7��5\0X�`���;ٹ�s�Ώ?�X9)_�uRR`�XO(%&�\ZL���v�\Z\Z:eʔ-[��L���~����ǻ���***J��Dڃ������x{{/X0�����������aƹXLJ��ϣ�%㡢��\"�r��1c<=<�;r��5���o����ףG�+V���(��T�988�#������{G�9�o \0H$�Ç�]�vÆ��qqr���E̹{��BR�X\"���C:XWW{��E��FVȉ\'���Zzz:EQ���$��ǏO�8���m۶�\r��ѽ���kkk��5v����z���w��Q:c����O?͜9s޼y����>={����4��Bў���������`q�8,,���׷�oXX�X\\O�	E\"���.#\0�k\0=B�C$	���������~~~=zt_�|Emm\rY!SR�ǌ������w�X$�B���[�b����w��|���w��IKK���j��0����u?{Sb�fmT��*���e���P\0���]%�b�y�9����\Z,X�`�]t�a�a�j]����u�̙͛���lhhسgϝ;w���bqXXX�޾���aaa��wS���}H���b1�\"����oO���E\"we����~�����<�\\�Q�? S��w7�旗W\0\"��@D���;u��~����#���o����������;v�ܹ3%%E*��^{���^cE�1PR���#y��d�i��䅾�\\_�L_�?��lN�͚5����e���ckV����y��Ԃ��\0��P$��B�Qg���������U\0���4ݼ�-X��9�ɓ�d?��e�����w���C�Q�}���zt�b��ښZ-�l5F��S����bHQ\nnsQ�٧�̙�hX}������q�֬^ӱc###++�aÆ�۷�W�̰��;���Ǜo�>����ޯ_�fu__�]�v�zu��(9�}�6��b^S�m˖Ǎ�f͚�:\ZY[[\r6l߾ߙư\"w�X,������oo߰��zq=�E(�3�1�H�ڵ�w�>>=|BBB$b	iޔ�ä?�:.#���Z-��1��T2F�J���\Z,ej�\0\0 \0IDAT���R�<��&@z5��z�@�WSS#�5\nM\0lB������c|�vpd�����%�$��i�n�r:4`DD�̙3\0���������1-9$�>}&����������$\'\'�9}�|}�Tf�ҥ�-�u���#�222T�bn�a�����èQ�6n�H~�������={v���\0p�ȑ>�\0�n�ې;�w�?>*:�ҥK��c\"\"�m\0�\0p������}w����cc���/##c��akV�f�>99����1�c�����Ét�\Z��m���EQ��يa���~�-//O.�oX�+T���0���7���̚=[iT2t�b223ƽ;n����ϟ߷����7�\r�z�\Zv�:99�����c ��]�մS�N��c�}�xƌ�ii�))�6o��k\"\"f̜�1\n���~ڎ��cb��^��&��ń�������ٳccc���w3����׬Y��\'\0$&&9r8�fƐ!C֬Y��>��.]�h��[��=����\0rsr\0 �rr��X��	��Ϟ={��c�SS�������V7o�F$�_`#�K*+m�ԗ��ݾm߯��А�JM��\0#sww}�ͩ��\0 00������ii�����F�T*���J�(帋\0�ߙ2eJrr2��-\n�1#5--%%���~��̀�����h�-\n\n\nJKKMIIqp�߼iۮ���g�0�9|\'��-\n\n��ʦ߼��b�K/^|++��ѣ�۳\'--���CI�I2����@Ú�)�V�ڵkIII���ǟ<~�lٲŋ>}�����uYF�ǆ�����ϙ3����j�\n������3\0|�ƍ����\0��JK�޽;�o\"���\\#��|�b׮]^�^\Z��+))��yGGh���մk\'O��/��_~�q�޽����註w�7��w�?s��Bǉ)�wϞ��i�NJ�;�d�t��ŋߺu���#���$7�PFFƊ�+�]�����칳���w�T�zճgO//�F�!����Q�X�y��s�F~[�뛋k�\0�h�\0��Oտk�\\C�ܙ�0V�)d������o����Դ���Q�F1�C�ʕ+��\Z���Q���w��\r������_v�r�޽h&����Y�իWO�:YP�J�\n������]�t��ȴ�*Ɵ,Z4#hFZZZJr�����M��qs�bBā����wǍ�={v���={�\\LHx{��m۶�T�.���}��s�̑����}�T\'??��^�X�3�ޔ�R�KA&�I^\"q�+���_�����ρ\Z\Z\Zy<�̙3���ӧO�8�.4c�dѢAA��i)�)�7m�\n�������Tf�,�$۴y3&�T�*Ƌ>�dƌii�䓷y�fR��]��]�v����I��<y�~�����?6��k�k��7f �o��l�c��)v9Ӱ�c�t���\'���b�b\0���{��;�ɋ;v�w�^Tt���蜻ww��A*��/�KJJb�Ǟ>}&�J������ݓ�v�СÉI���ҥK\'���k���մ��\'O���ȏ�����{����p��5.άH�U\0�0�n�\"�]����h5��@S��a��k*�l�^ӑ=~-���ܝ>}���׫���N�z��Y==�vhL���oаӢ�V%�b��\0gee�{g;vlVV֣���������`|��I��b�`��\'�}}���O��˝��6n���$����Hi���曔���7o�ԩS����/_�j�*�4\"\"b�����S\'O��8:8���\Z�ۅ6<cV����M(415�3gvuu���6���\Zϙ3\'��-���!!���!!&gr4��VVV���Eedd�i�^����7l`}I�&h۷m����|��ʽ��M��w���L�M�ΞS]]�a�WW7c�9s����d�!$$������!8���#48��ޞdu.�\\�����-�͗-[�������aVV�Ǝ�����a1�v��I�\\�BC���	�n&�&��̮��^�~�������ٳ������ߺ8���̛7���/�k�>���O�ʞW�;�8oܸs\r$��!�Θ�f���ш#lڶ��@�ˀ�Q7�S��Q$��-[�I�����73��d�\r%���7k�R7n��\ZmK)@���۹s��k���HOOi_#���iYXX�����E��\\�2.�0����U��������������|��剉�\Z�Ŝ;׿CCcss�e˖\'&%�Iƹ�=��!��@L�F�L� }}������ϝ��7n�H�{�ȑ��0WWK˯��*&�J�q����~���2y�z��v�����d��[���s����&���\\�(��q1�]�טR_�͘�c��N����%K�ܸ��N�81v�;�m^�w���1��5w��H$����رc�َ��?I�_TV������\n	���#�HT�)�n;::����:uJ�o`			���w�w���=�`]],,,V�ULL�=��]���]�����ڶ5400`@YY)����.]�\Z�ic��*q���W@�%\0��:SOЦ��\0w�>\"����?m�gLa�;W����ꌴ��*�5M$\"�.r����E�����.���d��ڵ��2�ϟO�=(9l78 +_�����-*�ThH���#Y!O���J8�TTH(� 444�T�+\ZLj4�2���~����駟0G��b�����������b���I�,{��w߉܄�&&s�̩��^�a������єɓsss	�+���geeeh`�����Q	�5�@�}@bֵc\"c #`H�@V21�%PWuUL}U��i���;6mڴi��͛7o���~�6� &�\\����[�/[�,1)	+w\rYF������������|���I���ӿ_?CC#Ks��˖�S�0FǏ_�n��PhaeL�>|8,,��Ս]�4~�52Z��I=�����+Q�K���ү�����0�䠶�c�NN�,4�H$rwwww\'h�N�\"k���ch�SQ�H[�:u*$$������1444**��ZTT�ڵk����B�s�����#aaa��.��_�����uh�NNl������Ʋ���!������N���889::�+���\Z�\r		�wppp��H,����q�F�WHf@6�h5�s$��黦��T>�X�����$hݺv�6mڶm����z���Ĝ��k���I��x���\r�U�[IUDD���{zy�	#�ꫯ�C���ȋB7aii��#77�{���۷o��短��BBB��^����R�^����Ϟ=\0C���?ȣ�7o���h�*1?KKK���X[[�̱�n\0��Ύ����1\0\0�������\\0��1���\\;\r2zlll(�z��azzz�޽I�=�����OQԆ\r��,�~���o���?���z��/�ٵ#�����s-�oldT__�vssô}�����-��ٙ����ڴi��۷���\0@OOOZā���r/�h9@F���RW��=\0ص�cjũ$��Lk��¯]�v$I��F}����cK++ڋ/�ݻ\'��\r\r\r{��À�O.\\xr���/M]]��76�\0�}�qΜr� ��AJ,��g�9���>3��{｟~�):*�n��_w��Q�b\0pw�������ڵk�\'�z���RxZVfmmMeݺ����{\Z���]�8�Y���\0�\Z�yIIɰa��X���.rW�\0\0�N�,����m�almeU]]��2��7](�BH��q��Dd�j^!\'�?qŊN�����W�^���bq��\'�oߎ5�mt�`�>|�bŊ۷o3~5J5���*-+cV�F�q8;;��QFr��D\"Qii�ʑ��ˈ�9��	��y�@X�ݺu�\r\"XYZ��\r�R)�c]]���	֭�\0 �\"��3�f�rx\0R����M��=\0<�yy�+җc���7ַ��ns�N\'�\0\Z�kd�A�mTttNN��ݻU|�m񷲲*-+%=������B�����J�����Ս��;3��\r+�<,�b�ĉ�����>��R]�_@(>���!k\\QQ�u�VSSS333+++333R�:42 �&�c$�\0%i\r`P2�(\0\n0��WjV\"��ڵ��0a¶m�V�^�~54}�T��yUT��[���O�<�	�@k`�CC�?w@��`3�I�%\\���Go�Y2�r�J���ʣ����q����ۓ�dT�#�2f� �ˏ��$Gg���������]��Q�|W�ƭ`�*&K:��\Z����\Z��\"�X��.�������k�k\Z�jh����&�ل�_hR�t֬Y������w�Ν\"\Zhdd�4}�u��{;X7\0��H�\\\0�w�$㭡����\\�|���O�0�/�044��v�ԉ� �K�4��֭ۯ��J:W�\\y�ڵ& ��\'��7�����:u\"��#G�L�:uϞ=����<����\r\r\r��ꌍ��ٳg�8Ӫ!�ډ�w�B���)�re�%�RE}��w�39r�HZZZxx8B�9�KG ��?���>�lǎ���^��ٳg�FKC���I�0Si%�˴���ݻ�������X���/�+_䄅	\'N�>q�6\n����������l�\'�\Z����p֬Y˖-[�l�P\\R�t��G�0@{ǩ4����8<\ZJ��������ۧ�:C3 �m��ٳgNNND>433k�ha�H�S\rԥK\08q������ٿ���!����E!$\n�n�:a�C0`��3g>�䓦������=[���B�Z�C�]C`�#a�����ɠ�s�V��ظ���I��p_��}.��j�/^ԗ\0����zH��z ���\\������u\\���ZkϜ�ދUlK�eǖ�c��Ns�@7\rB����$�})B\\�A��P.�K.i$Ni�%�q��lɒmY�X�������c�N2�Ǐ�s��ٳ��h}��k�%T����������y{����0=8T�DS`Ŀ�硇���>��F�J�H����u�]��NL|T�b;V�\r�{����������O<�į������+�tL�sݍXf̘1ǏwbDG|\n�{	�<�J\0���޹3gEBY��%K�<��_��W=�P��0�O�:UXX�<����J���z#�������~�����}�K_z饗v��q������������7�{į�x��f̘��~���+))y���#s����������]�6Rl����o�VVVfY�;p�\\w�u\'N|�G^x�U�V}�_��w����M�5k���Od{II�o��������螡.D�FD\ZO�<��s���������w���Z�5�3�<�������?��,>�q�\0��_]]=hs\r���Ao����H�������0�T$.\Zڏ�y啝��筨\0��n���m��d��O�W�.��I��m_r���뮻����D���@����<���?~���q)))������P�m��n��k_�Zyyy8.--���??�kt��Z[[���_~��~�G+3F��\r7���h޼y\00�����g��GF��������s�[�n�O_��y����w����Ph�޽��k֬y��jkkkkkx���=�ý���P��N�X���ȓ���w�СP(�;X-�:8\Z\0\Z���v)�Yv��/�<�ᆀ��{2��Kfԏ�0=8T���qQRTTt뭷���ѿ҇7.\\�������Ik}���7�x��^QQ���:v�� �������{�ӟ^}�����w����}��p�\r���ɓ\'���\"����Z�Y������>1\Z�������O���///�mmm�ׯ�f��>X_____���<]4W:|�.��-�,G�ټy��ޛ����?��_��_,X0��Df|x�7�=���w�\r���>��m���o㭷��裏F^4�W�^�r�رc���t��W^y�wN�6�\'?��O��Q4Ƿ��������/��O|�G����?��O���wߝ���r��իW/^�8���ܴiӬY�n����ոΎ�.?\Z�q��	Gc8���o:�N���|�+_پ}��I�ƿ�Q�g�\\������%%%��r�cZ���2T/�w��>��O�u�]+{C)��:�g�>z��m������oC(L�ɡС��\0�6n�P�C��_<\\y���~��w�M1~������s�~�+_�x<�?�|D�\r�qw�y�\r7��m���Ao�������~�W\\q�w���|�K_Z�fͨ��n���~衇~�_����L-C%���׵k�vvvΟ?\0�͛�#����h���n��������?�ܹs�ʕӦM����W�Zu��|�=��Y�f͚5EEE��sψ����<����~��ڤ�\'q������{��)--=�g!�L��}��1�\0����1��}߻�$�N���;.zn������#�҇(�,Y2y�䧞zꗿ��֭[#>��������~��N����}�C��ַ���n����g?;{��n�i�ҥ�[�_q�Y4�.�ד�O��	t���ܧ�z������n+))Y�|�O<�T��}��f�����W_}��U��̙c�f��{�U�@o�s�0?�L�2�/�KAA���{��.������\Z�M<���W|����������t�ICm�E�������o9�w�}���ǝh�tB�ya׮]�dL���>��ǽ��]���=��t��\"��7����k�ܒ���z�曃��=JҲ|%C�U]�;\"��K/Edd��[�ܝw�1��������f�j�y/B�R��a~�n¿�d��e�[�:�����m���?|�w�8#�k_�;d�ꉽw�Otuu�_��kv�C������X�� \"?/�52���O�{|��f�>|�wlٲ�j��ka�8�=������k���9�l������~���\"��?~<�\n���~��_�4��������^QEE������A^�\')Ɉ��Z��m�w���V��7:}��w�_]�{�P�÷j�N�f���뺎�\rrr٪O��G�)9{����M�b�b�,��m�.X���p�嗷��e���x�4�w���;�3\n}�{߻ꪫ�{�}ϙ�����!�k�;b��}�9q�D_�FDc8�ʸ�]\00�4�ȼ���_��\0hx�5\0�[����;N���u96m�ԛ\"�l5�<��/�|�Q��X��a\n�w��w�E\n�-v�Q4{ٌ%�EY��0�f�f~����S�m�s��eLX�{5���]�v�eYW\\q�}���ޫ.����;��ԩ����.��=�V���pqqqqy�``�-�C��ǭݷ-�q[�ŕ...........�C�4^...........��pqqqqqqqqqqqe������������+3\\\\\\\\\\\\\\\\\\\\\\\\\\������������r�`�����\n...........�t׍vqqqqqqqqqq9����\\\\\\\\\\\\\\\\\\\\\\\\��������������Wf�������������2�����������ŕ...........��pqqqqqqqqqqq9�2��EDD\"[�o\"Z��\0z\n���g�;����q�2Z�~g`g_�d4us�����;+�\0<�,��EXl	�-�m�03�\r,�%Z α��f���A��hf��4\0�08W\Z�E\rZճ-i�~��u�;�ȧ���uV�y����_�+���������2jF^��1�QD\0�٨U���@m�-˲,˶m��G��}���^�R��D$�\0����b��}?Z�����@ ���y��:�BD\"��6ED`�\nA�\0�A���a+��1Zl\0BAA2�x�E�k�W�!��\ZQQ�2)#��p������o��_��?�q�q�؈��UZ�A�?�5�x̳������R�,��B���$���k~	\"iM\r\r�[[Owt�����@(�ڶm-\"D(J��돍��������qq�����D�%�\0*\"f\0��G{S��g�ljjڿ�̙�MS\r�O�Y\0�P@\0�à\r�CP����#]��\0�m�8\Z�c��MA[H\0<�3��;!Μ��$>1/&\\�Q�\00\0X��.��0���ց��;;H6��u�^���D4�))��C?�7TeJ�~5�[�����#\n�A�Ԡ��j�[k����������gi����5����;�\0\0 \0IDATm\0 2���]]]����ۺm������ii��ٹ~D�pD�\0@sssCCcmM}0���+���\n���a��:�-��d\0r��̿\"\ZNJ	���D\0���b+2���X߱�#�Zs���N�=��	`(F$,aF`�p�LqZ�U���d���i���f15���[�2�8+��������ȕa4@����[5PT�0�`�̉ҥ�J�wMf0��Z+�˒�o{���1{LFJJBNNN\\\\B�q�\0䘀Z[�Hd�Ӽ���ohhlin=}�u欙�\0���7*����JD�cR:���毠�0�&\0�u���h}�����K�$(6��D�̈́�J!��M�m@BVݠu�\'f~�wYv��c|��(� �6ʱ����Y�Ӄ����{����0����\Z^jF����2\\\\\\\\\\\\\\\\.��\0\0�|���|����͛������j��ctvvvUT���8���j\Zd&\0!��B!\"\Z3f̄���ƍ��q�@������55��x�G}>�0ӥ��wBA�?YP0Ή w�?F\nB`$\"`���p���o6Z�0*�gܝh\0P\0H�\0`\"`&4��Zk6�bѤ@�O��	x�Č��d2hҚ�I ���5��aP�1��`D�}��~@<#>�j����~��2�����:1\\\\\\\\.>~��n#�\'��n��\r;b�#���7��?>��䔔�n��0�4�\":\n>\\�u��	�q�&L�^���`\0�q�����|f�\r\r��}���dg-^� ++#&�����^WW����~ѢES�NqT�c�`C���������������y\0jM2 ٶ���ɶ/�+b��\0� �hArb���hD\rL\nHX��0I���e���i����c�L���bz�V`8��_�DĘsE�6�r���vd��s��o#\r��賑���p4\"�\03#R���O0섫~Z�o�H�^v�\Z��%��F�T&���i�����;�w�n#\\�<�ԫn��\reͣ��f�\0P}}��O���������p8�c�{��5s�]w}�0\0����O�:URR��gUUU���\":6�_VV�hѼk�����}ݺg;:���n�qc�^;6�3��̳�<WQqb��k{����g������/��~��0�d�%6�z۱�\Z�5�@\Z�\0��AL,6���	�H@�����D��*�be\03��ֶm��d����\r�����Ȩ������%����={�����s�����������99��6\"vuu��7n\\ZZ�Ӏ������w�j׮]yy9\"���w�mmm������b���_��%K�����������}֬��y��7��bKJ�HGGg]]m~~��c\0Pkk�mۃ���f���*e���>|x޼y\"�o���Ą��<�TD�߿?333--\06m�2u����L9~�\"��!R��۷����:s����B�u�kZ�D�Y3��ɓrrr�~����<˲JK�ZV��U)�8qblllUUՄ	|>�Ӓ���3g�ܻw_ss3�r�/\'\'\'͜9��........g\r4j#�a�Fo��f����_~iÇ>��	\n\0ز�C�y�/}�wϘ9e��W\0HD���Pggg��cΪc�*SRRbbb\0��1&O���;>�u��g�})�hÀ>�6--���uww#����7�&�¾c�pf���pf��w,��|\0F\0Fq���S^<���&BaT\0\nXY\0�\0\n�\0@�(ԛ��*�\0�\0,��� ��Nx���� �B$\rB��Z�A����ww��|�ŗ��j,-=�}����Zf&D��kްaǉ���hPLLLMu�={����Ν{���DP䌳b׮}�Y9�\\R�����s/tw�i׮����\0��s�<��KGJ���ڠ�ϰg��W_����\n@��]\'O�XaЍ�\r��ݣӧO_�l�u/��ԈHGG����D��o�����\rܷ�~<r����4�bv�x}۶m \0�UU5\r�\r����/yr����a����+++������M�͉��S�����j���+��;R��\'Μ9c���f͞4iRJJJ{{��#�p�YY�����ۇ 7w̴iӴ-���N�:nܸQ<T�B........4���H�. ��#\'w��w�UK������~��m�Ї>�`�B�pBB����Q%$$x<Fkkkkk{KKKBBlggg8��l��K0��h߸q��9�ن�����Bg�[Y�.���~����ND��ߎ�pd�3W��!����������_�\0�I��N��e�;�	Q�3���3�����~�FG��E@�F@���)1���/��x\Z�$�%�H�8�\0����&O����5�;CA+#=�0�����`]}��/������t4B\\\\��K�U�jhh|�|��:M��DQ�Y�)YY�ӧO���Z[��4=� \"J)f@�\0PS�TX�7{Ό����t`۶W���u P�X�����Cׯ�����H���UZ;n��O��������>��YqE��Q��p$\'%�Lسw\")\0Ж�f^�X������(��������ظ����L��/\"�@�^����������ɉYYY>�OD{}f�M�D=qDii���iII�111��))I�,��]��{��>��^nӹ����� \"\'GS�|$B��Ҳ�{�]q�Ҭ�1z��C[6�((����>RVVZSS��������9R�s箶���cUo�����v��w��Ʈ�v�ruuUmm���\'.�l\"n��fJJ�I���4�!\"s�����^���@ \0@����\r�\0�*�uh���?�쳎e�\\	?�Ȁ��,BL@H�֍�W��	J��x0z4��zb�iAAB��\\V\0�芾��zöH{�2�H$��+w�?��,@%\Zـ�MA{{��W]�q�6Dlll��T%�������ںٳ�wwu54494���76\'7��G����~��Y��m�iReEMy��������0s������9�uÒ%��Ғ[O���:S�9\'\'\'%5����!(��eee���\0�����7n��jmkۿ�ॗ�NH�C�8���8���\0��z}�ٙ��5��E�01..6%5�����;��@zz����δ���~\0\0�I����m�k��WEhf�8}c3�[������=�1��p1��A�1]j�7��:�y�����/��YU|\'�F��?\Z��ԫ\"���=�gϞk��299նÛ6���ؼz͵\'��iӦVW��\'���ʆ�����f\0���OI���N���NMMM�HIIM����Pv��������۷�4�9g��m�H\03#\n3̚]R8aܳ�>=3v�w��!��R%%%�e�˗\n\0�\"(\0�`���g��:��`���\0\0L�&؎N\0��ҳ�  \"�8.\0��	T�3����1��M��1o�b}S���0���q��m��N��\n��m		q111�a$-������a;�ؽ{��\"bq����ۻC�����qӦ-�ͧ�>l�����^�Y3�Ad��\"�1�����٩�a�������r\0ùO�֗ν��������;c\rY��/���S[[�d=�����+�7l(++����XZCVV��筨8&��\r�z�ʂ��Ph��M��U}�&�v�^�E���k�%��;r����N�rqqq�@�$��+6\"�z<F�2��dx%3TƗމ�8��t����e˖��˖-;/\ZC�Ʌ������iU{&&95ni9�u�k+W^��\0�7o�x���766\0\0������<��S{�,.._0&&&������C��\rm�p8\n��w�:Us�����@fV��N�����^�l��y\0\0D��\"2c����\'<��S����1��-�i����Y�f9�u��`�\r� @h��֩\r@~�1�6�!Pb��;g��uhhM�\n\0Й�%=�=���!� \"�)@\Z4 `�\nݷ�~��.YY� ��\'��)�>m���u�t���cm��5��Ν��>��=����Ɏ�.!4�?^W�x��+��y���Q���q�������~\0�,�P�;}��y��fff��b�^�YQv�ؙ�������*�çN�[�DQ��&�����{�0\r\0�gd444*�Qs�麺z����敗���W *��������\'&&��M�\r�()))++��--�O�<>e��%KM��g�.����H9��}�C\0BD�� \"+\"B\0�p8���L�B|�7c��͍�pqqyO�h�K�uzD\n��M����x:/Ј�������GaC�d�0�c\'���h�I��Z`Њ\rs�h�ex�����?���.	�Yw�l[�_��� )D�(�O>�z��))Ḭk��pؚ?�̙S�A��^@�C��[���s�^u��C��������+��|�ܹsf̘1eʤ��)3f̸�ˮ�rE������>�n��UE���=�b�Y	a޼�)�i���:� D�����wa_�1H�R� �oO�~��\0e��\"�ѠL��řrZ�(�J�A��m@d\"Ԛ�G�D���)\ZA�\"�l0�(JcyG��\rm�\r$B���n��o7\'����̝�i�������D��ax*+N����Es�N�2s挹sf���\0tv������Ʌ))i�~�������2���p���\0�Y�\0\0�v$����ց@ �m;\0��\'/�WRRRRRr饳�:���ՈJ� ���`\\{[kmM����/y�gBA۶�o{����q/�h��\\�`�ƍ���O{<��ɓ��&dd�\0�Fd�#rfff||��l�СP��W7uv\0�I��פ��(�3	\0��&����1cr���:,b���-[\'M�ܧ�Ak��;|������t�f\\�ӾG�Gcr��l�F�bcD�0��d�s��D�w�~1�C9v\"ۣQ\ZQz����6�k&�sٲeK�.u>/]���x3F��ζ5�ͳ�Lx8a���q��E��I\0P___ZZz�m�2�>���hlA��͛7�8^��O߶o߁����W�^u���E�a}jp�\"B��NO�Z��Ç�x<�C�Jcc����c�C\0�4�r�GT��\\��?=>~|Aff\Z3\0h���o9�������[n�>K��\0%�H�Zq�I��	��`�F��P�O�Y\rZ�d��\n��\"$���gfX�ɧ+�	��K� �Him\0)P��c�ץ\'8�@Ԁ&z�;�c����d���.�bYNN��\'�;�����/Y���>\"���?��	D��nHL��<�\0R��/[�i���˗j�#�+YYY�����R\n��Ԕ���I��̙kj��i���H,���������h���,\"љ���\\2��������S�����?h^�x�ر��4���\0���Y�tюۯ�f%���Hi�~�O\0%7w�3�j���\r�Mq	>333}ٲ�?�D0\\�ty�	\"�������I�ȩ��oO?\0`nޘ��ѷ��ї^Z�������]q���±Z[�r���~���+�l���b��9��f�,�r�X�Q���I���ǻ�{��\\a���8�����:���t��V���y��g����q.�r`�蓲��]������p�\\C���m�w�5�!\'9u�Vk7.���^x������x<�,�yÆ�����~�3ϼ��w��9�p��������{�յ���h�޽��^#���ڷn޻q��w�y{ff6��#���2/��-�����;1������@ \n�l�vV\0�@w,u@b��+7F40$l���6i���ID+\0`�Ilk+E�������Y�[�ex=\0p4�ME���Ά��	��>��Lh��c���6&i��\r^�U�H?�c+�D\re��HfffFF\Z�/Z�ȹ��͛\r\0�e���%����O��\Z&N�0q�\0b	����\0���86�^��\Z�	��D�6M��>v������NG�	��O.�T���n�\01�լ�S\0&;�\0���?��;>gb�9--}ժk��\'N�X�3fP\0�|�bf@\"4���W����яހ��\n�?`���\r�\"�Pj�� ڱ��h�ҥ�63����O������4eժ+�G@��J�̶M�:���P#b���/:<�e��}����xoI�=q��hv9����Q=�1�-�o-�������a? \\���:��<6��8�������=f�78c�...���!���m��9^���ٗ���K�Mܸq{[k�\r���y�xRь�#KD��c������\0��4fffUW7dd����-������փ���KIM��7��}�����mg��􄬬�Ç�&M����Ӓ����O~�1���I�\r\0�ALe>_�Vަ�@�����pL6\"�h ��\'��1q���o�Oj\nZ=�1�d`��x�3%�ͥ���~���,\0�#��BaQhR�B NX��46�v�����X�O�յ�1\n�H�ȢQ�#9[�ҢP\0��&�\0\"��h\n�\r��%@z��=��I#�����%��A�q&��XL�ͤ$����T`3��	�� � @&��FAT\Z��6��Qv\"40!�����_��LͤDD�&Q�\'+)�69ֿ��0�)��Z	#�8K�#�\0��\06	�\n���#�P��	��D�� Ddج��A���\nh@�6*Փ�\n�Y���X�\"�*�ׇ����~.��_��~�$�Dl��&[�}�9[{����\Zr���G���_)3�A\"�����8���O�~x��?\rz�~5��}7����1�^γ6�]MpE��P Ce{w�&�+\Z�\'|�>��$�s�B2.~�&�\'�M����A0++��\Z�V��\n\0n��������vu��^sͳ�>;��)S�8	W��<�ԧw�UW�TUU%&�������v�ޟ���nݺeˮ�����\Z3e��#G�dee��\"֫%��}>_Vvze��	E�LS\r;l�s�Ȫҽ�4\0-|��SZQ�jA\'�� P&r��#�H��C@�U�s�|�R�4vO�{i(l2z��e�ԭ�Wv���׌�����c�����u-�� `Ft��i`�J��Xc�o쮺1+uL�l��|�Vv�S�lS����<\0\ZA	���DC# �Z�R^` a@��#̐��X30��E\0X!Y@$�h0\"I�Dѫ�R`2j`b�$^&4-\'B�!��$� ��L`�E+�P���̀d)T���f�\Z�II##�),�BD�b��b`�$�����\0���+�M�-\0 ���FT@v<pD$Y\0&032)���A�,�A�,D�\'ZkP��\rQN*\0A[Mbd���FQ\"����b6)�	��}�2��<���\"�\"�����]�\rc���P1Q����t����P�T̸�_�F4�m��5�ye�ʠ��hW�\\ ���\\C�O#��Սq~C2�9�����C�Ik��o��������/�r˭7EV뫩�iin����|�o3f̘2e\n3#r�1��P�Q���wtt$$�i��j�Z2i��)�&^y��ee\'�S��&Lln>���՛TJ�3���)�yy�DXUU�D�E����ݻ�2Cod��)��\"�OeSK(�G�Q\0�\0``�A)`p{qj��f�Q��\Z-�V����!B�#xc������T�������?���G�!�X�� 8�N#e!\Z�bY\"~��_T�\"�L �\0��+�g�D\r*�@(d�0 \"k�,�����ή?U6���E��0�haD�P�\"B��3�0�&tf!2�B(�I!j�A�\Z���5\ncX�b�AX Ll2�%�� �5����%D\0`C��&�\ZQ+��4����b��N�.͈��L�� j����4��D�V\0Zk��֖21��)R̢�\r�Y$�4\0���\"��\0%���\Z��FD>AF�\ZQ#h�P�\0�A��5�֤�Aă�6a����=���w��d��Q�z4����GiʟK��O����Wr�-v�N�rc����O4C���>���]4``Μ5����^�9d<x�q��Qm`�D>b��O4�.�n�[�aZf�6]o��7@�����ԯ\\y\0�������/F9!\0\0 \0IDAT��jjjR~~���߻���k�y��OΘ9����7�\n#��\0ȌD��\\�z����}޼�O?����NE�Դx\";6m���ƍ���N�6���߾��K�E������KIInnn����x��m�|���޲e�̙3����|@��M\r]�a�m�Bg%lDe	+�\"`��s�ܼ��1�T�=T^���->�|�,A@f!�\0\0$L��ɋ���ٝ�ߚ���9yw着o�ǈ�@\0\Z��A\n����9d������$��g�?�3oKAS��D@�,��\0�=ʃ��I0�	<�b�р\nA�\\\0\0d�0(��R\n4+��	D��e�\Z��a���\Z�a&�B�X)�\0�2EH1DMd�m\"L�e���2<��m�mCy��P�\"��Q-�H,Q�6��`-\"���WX\0I���NL�D�����![���e(\"�\0�-�A$Tlj\r�J	�R��8��\"�Zأ�\0���F0D��\0Q	�\0H`\0�{h��h����G����2N���1Ѓ�\Z�.��绨������Y��.:��S�R\Z�|���l�e�h>GY���������a���!\Z�5\Z�::�i:�v������������䔭[�M�p���Ԕ����6�*����L��͈��}֬]���닋\'��ٿ�eAqq�SO�m��)N�WǞ��s���\Z޽GHpAA��Mۊ��M3�w��w�ˤ���o��m��Y㙈�	w6w��P��,���W �!� `a�\Z���i9?:�r�ɚ#��B��f�fԠ{�Cb	�����.�ǹ�l+��\0\0���\"h�B%�`�Mhw���ى��f�&)e� �D\"����^�	�H{�}�ա0������d�����x�]�2<��=�w�Y�K�4}jG���E��|~bҍc��D3����ko�$Ea�Y�	��f��\rA��)�ZX)����R�vU����^�|�(�\04o<�uE1�6��\nn�\\�\"\'�s���@5���:�M��a\n��o�_*ʉ#\\��دg�fy��8Z7&ּ>;Q���-��OJ�63\r�m\"�pG���}�������nF�ܚ�NH_?X�|U�O�o�wI^q�G�����ǎ��ɬܩI>&�Jkן�zf��d������Đ�k�SP��f�_kY�f�MMQ\0\"�ZW�v��:2\re[1~�y餲��{4���L�)J��Q�����Y�[���Wo*P�Z���k��_:X[��B$O�ɟ��^�}\',]��Y���y/���8}� �Q����?p�upqqqqq����4����UN�4	\0*+OL�X�����c22�[[O[����oߡ�K	XD����R�g�嶶6��Ǭ_~yCIɔ	Ɵ<Y���X\\\\��3�\\��ʌ�d�bvw�n������?�����A\0ш�(� hIJJ��ᎎ��\"�h,G�������rv�$�E��Mm�:�V@H\"���b�	��\"h\0z��t�7��>����:Z]��)�	�Q�=�&��`0*A9S�@�A�\0�����=����-K\n)l�`XHP�E�B�<%%����xy~�\ry���	�<\\^Gd\08���&ս�&�\r:���م/.�ļq�=-�>\Z�X\0��ۇ�;\0�~������/,,zhj��\r��A�H\0\ZX5����eQю����7���L���Խ��]!��?}8,�2����C�ߟ�|�)��}mmL���W[Z�����?���-���#��\rA�]���Z�ܭ��@���m���˪\rhE�U;�Wd$^���&Q�=i���ny�DKuG`uF\n����M�__1�ˋפ��������`��R��G���+oU;�E�����<�pҖ%^n���؂@��&G_�\"��d@/��~i��/y��	�P~l��x�/�г�:��5Y�p��Z��}uɸg�߲��AB�C!���MK&�j�؏�q�b��Ζ~Źơ�ZNDB�ߵ�F��]�����\\�`���~��.?v�� \0v��}�%�M�dMM���=t�t��|>�ꍴFD�(�6�noocb�::�on�{���x}wIɔ)S&oذa�⥱����=��O=�̉�5��짮��j�\n{zV:p�ME��{n������M�$�~��iAD����uth\0�t���A��4@b���-�>��ҹz�+--K^=�����\Z\0m`�-�@ap&� �x	���\"��L���o��s��#�NV��\0A\'��2Nx�kK�����6w���gw��������u�%�Ez�vp�P\"��L�#�cf�<i0�\0�<H���<	ۆ \0�0*���Lӓ�ͤ�.		\0i@Q@Z��!��T���g��n�����/�U�\0_|������Fx��m�oCk������l\0z���/_��(%��\0bZ(I\\���Ds�!��3<\'9&�|�\0��W)D P���vVΉO���LT\ZQ�&�ٶ����˚~{��X�W\0��Ʃ����7=	>��xLcK��K�)23�����P_�o����.0A�9U �KIϚ4���.?�mZN�^\0$Q�%J6��XL����%�� m_�z�����Ƕ--0����f��H��d����^�zLJ���dC8|�j�Q�t�ŢYN��W\Z#2jo�9��ٽＩ�C�Ϫg[a�O����. ��*��q�O�jo�ADf Bf���ɬO�>��JOO/---)����9��,�*--�5�������=1!�c�Ͳ���\0@iiiNθ��@���ƍG������޸a����뮽v�����?H�\"\0�����&l���ԩS%�j���~�?|�k�nFoR]!\"�6`WtHlBP��1`|b�#��m��� B�WD3�Il3\n���5\0���!\r�h�0\00��Q�IX���NQ�M�L_���ApM�￦{<��N�+^8ֈ��D`\Z�\nö6yG}��9I}�(�������`�ъfTvŮɎc�/&�0�&ц#N\0��m���T�̄�yI��$%`�0\0Hq�Z;&ᅦΩ���2v���ꑯL�K%�ev�uka�5]������\0@�i��1O��o�κ{G�G�Nd#`��Q�����>��R��Wd���9u��L�VyKQ|����i��Z=�B)�C��<����&ؠ��b{dR\\�@X1BL�q��ӄ(��L��<W�v�،��u^��GY�\0B�V\':�.������W�+B\0`A�¦gw}��+�;�8&>2&C�*��|\"/m��ʟ��3�PO�����~[��|� ���v�؈;��q����{qꊁI`�e��l\n��\r<�F}s�z���9:^���⪣����S�&m��:����TxD�p��]\\Ν��z�m��\\�g\r�_f��5M��\'�gee>|86�?fL^||bs�ᤤ���ܘ��ޥ�ED�8���55�99�̴�s333����z덥K������I����4����/4M������Ȳ�+rr2\0 )9nǎ7-�\'�O�\"�\"\"b���\Z\Z�.��|���Y�p�2\'�E�6Ȩ\r*�5+S�Š����~4?��,�\'�:�Vu�㐻Di��,C�L�o�9����J���6l���=�vt��D �B��\n؄8�7�*��܂��&��\Z�ba����];O�m��<&�J;W�)��b]=�B4E@PP�\"�,J���0�b�H�P��(�-��� Ԉ�� =��������\0:Q�h�xI�52�f�K\0��	�8��Y���jS�u�|���`i0~�8�g�5ZTȚ�(����Çں��ޣ���ӡy�(HpRRbP�w��VH���䭭aagXB+Q�Ю��)�VԼR{eN�ֶR&�e�i��O�O�¾���=�@6\r-\Z�\0���\0	��H�4Ί7^n�:�ζ	9�� !M�7���lz��9̞�W�xbZC�E�\r\0��\nAk�0��)��u�r\"y\0l%B��	޽-86I����d�_�X,\"B#�Pi����Vu]C/QJ�A��8�j���%�\n�x-}�;\r�}(�~(a0�ס>G�Fs��\r�h�0��(/���Bs�5�Fp����װ�eFMm���S\0���M)Z�b�c�WUUk�������\"�׾W�Bg]]�uu�Ǐ���˷m۞��~�ԩ}��_}��K�.�B���.����d��}�>���O�:UUu��d��+�z������~ �=q�}��C$\00\r3���;S�z;��|�bͶRĬ\r\"r�!��@C�|q�Ҁ�u91�b�&�����+�N�������P\'�NJM�g���-�-6���m	���L����=-�o5��Ե�m1x��d*!mw�����Y��r��\Z�L�i���@��@w�z��&�T�E6j�D�l*Df�R�ωE0A@@\'\Z��;��\0��C\"�\Z\0� ���$��xߍo�h\nS|��mA���\"�ȶ���;;ϙw�b�E		�h�H�l(\n�˳~|�����L\0j�ӡ�wZ��u�4ߘ����S�-,\"d���hf�w��%��5!�T�B4DP��l2�)��eS��Q�+�Wg�iaQ$ ��3)[[�o����1�h�1iKC���d\0�5�\'���¢�K�74�y���\04��P�(��\n2f��I�c \0\0�BA�yr�oQF�0 �\0��Mͭ�7t�_6����n�57\r4�͓�Ԓ�^%�K���3�g&x2��7���0�}?��wq��sqqqq9�2���9==\0���SR�\"N�����f�z=��Y����+**R���<L��������VuWW��&����$o�el�66ۄ\0	$$H&��$�d2�3O�2ql���f`���ؒmɒ�������R��]˽�{������IbR�G��ZnݺU��z��|�����;{�l*�Z������Κ�\ZUݻ�`OO��t���3��妧�ڝ�+��-���̌��EA�����}}��\\.��&m�7\0%���[Z�����l�1u���|*@x~�+͏�U(�pAhN@���l_��i���1�f���Ԇ�������2r�\'Fsw\r����9(�J\n� I\Z\'����.�\r߹��MKҟ��}n\\�����gô�H_�o��|�؅\nu�0��, ���Fa�J��j)�Ԝ�!DF9b$ְ$1�N�y�RLǆ0]UԘ��:t�M�\r�������,��Xgc���,UՄ6�}c*p�d����g?rr47훎?��ei�qDk��N�8��;�Oo[����O��L�r�%K�Xg���|!�A�ۂ�׷�[v�O�k�B�B��XcP0y�� ~&��,}�Ү�=y�[��-u\0�1��b�E\n����w={��ǃ���?sy����ޖ�++���\\W�Ҋ\nhTp~V�L�g~td�3�:@4�y1�<�L1plb40�)�����K�ږƯ/m�{�MG�Eb��������c����Xe�s�Vl~�xkEx]c5��;�É��Q����D6�������LՑ���o�2�h���]eʔ)S��2����/��ܳ�6m�H���S��\r55�����ǎ�PPE:�\\���RU�I*ɺ��\'&戤��{p�t&�ٴi���(+W�`�466��V>��c+W���w���������zdd�T*�����L�����\Z����\\uu����(OMNGQ����G�BN�>��o��ʕ��\r\0�xh����xms��kZߵ�z{}5�=2W.��������ӹ��Mmpⱱ�\'G���f��0�\r)�J c� \06d�3����O��f��g�[������%��_n����Ʒ�9�W��޶�vE&|r�0)+{�«�p�)�(�sYmȆ��fQ�d�c��/�K��)X�$�m��ʲ��J��uԿ��&1�,��U5�eSA����TUe�a�I\"We����>ezҩ��+߽�	�aR�0��*�]�zuTVvWq�����MU�R*�2�Z^�1OF�Wg�C���=S�]�ic7֤�k���Ƭ�N/	Ck��J����6�fM6S��f[M��d��Bl3�˫+�VWVvsuu��\n�e��gl�3��^�����!�4��l�-���_��ؗM�ݙ`CE�nL�u�as:P�Ė)&Hch[*Æ��Yb+����iK�\Z*RM���T]9%�T��V�g�Z�xi��	��2���,\raŦl�\n�;�Y��BS�qkuzCue��K�W4o��+����2eʔ)�S@/1=�s��w����zjff��gIss�s�{�ަƆ�E�W�����0ĖO�^v��������ɓ�\Z����e�Y�\n�����o����8p�@:vv��v��(I�;}�������[n�%�NOOO�:uj���I��Y*����\'\'/\\}�����\r���s����ˑT9a�}E�q�A\'��B��t��s�w�Lf)�\Z�Df>�|!&O�����B\n!�,�@1�v�����)���G��f�g�,��9���c/��\Z��P��+z�83v�Ф�aaADl�;�Ɛ��dC�ne[��QN�SX ��H�$H���i�cR#Y���(V	�\n������B0⽒a��$�\Z$U��m�< ^���|f�x3�����QeeU%K\"Lp���BL\ne��0j<�86� [�\0�y�\na%&Ҙ<�mLHx��@��J\n�9�U`UA�bY!0�z5\nG�*9Q�Ɠ\Z��01�`xQ\"eb�&�\"�\Z8�I|y$bP(\'�C�;��PYVf����2eʔ)S�L����4�8��9�ti����8��ss����o�������0E�s��7���׽�ƕ+W��]��f��U�k_��m���ԩSS��o������瞉�(�JѡC�D��o޳�T��H���Z眵��Z��N�M1QǸ��{�ܑN�/���d��1F�P0Lѹ��te�� ���ݧg^ݜ��՝�V�q޹�s�?:x��1�2@Da+4v`0�j�\"B�B�o�����[������C�l<?W�V�����/�ư)�<8����t���W������0��@��=+�>��	j�bkY�a��dm�I�19Q,|>�U��\n��<��o����(�A�CH��\r���2�c��CT�)1�a0T|��aհ*H�\nN�A�\"L$\n�*j	\n09\"C�F�6IE�	4\0�1اժQ(T5$�;V e��5P!��Vj@����ȃ�!@�4�QI�ao\0�`e����B`��WZ;�4��@�D\"́I�VUo���\0���?��+S�L�2eʔ���K��.����1&	�����~���<�C3V���ο��_���ں���G�-Y�\r��\'p�k^[*�x�ѷ��[FF������\0�[�remm�]��ٚD/TUU���:t��x�1?�(�)�?t����eY���B�����������G����|6��{j���x�뻫����z�g��l��+f�z��6v4�+���Kd���P/�X�IT�B8Q�&.����:?��>}ۓC�g�@�cت������%�[��*��!��7���ں�x���F�`U/�����82�W�I:���y�ZxRH�I�%r�Ll}���d>\"��J`ǒ$�3�(SR�\nU�`TI)&@��-Kd4PR�2�+1T^�1l���!dE\rԓ%�vY 0�DI=X�DՀ��*��ֳU%����A@F}2{�T=)Ŝ�E<\"���A�Fa��s�	dABQ�\"aUGʢB�h\"�\n�*Y�#0<�\',�����D���H@B��E�e�Q�L�2eʔ)�cd�|5Cĉ(��ɉݻw����q�{��Θ��K�b]]����ƍ�:z�w�����q��\\a||��Ri����n޼~h�t~��fͦ��\\\Z@�1��ռc��W�^�ThLEsbX���D�E�\r����bql|2��f\0Qx���@��>qx����v��������37n{Cw7Q��X����{v�s��׷�\rܴ���WWF�\'�,y�(�%0�U	�!�\0)�ykG�[�\Z�egGeUF���)�\n�J��{��+�W65�\"K�R�_�Wo*[������lrް\Z�HrR�zCĊ��F�\0c�\n�0J �T-ؑ(1� ���plAJB��|<��x!��L\Z�W��\0�\r�����\0\0 \0IDAT\0�)�(��+	��B\0�zP@j����ĈJ��b��\"�B\"e�H��BULD�䒘&�d�������=�]X�*T��uH@�\Z�\0(i�Y�yW��-�1+;�\Z0+%%Cd\0�{!(��3A����B\06�PV��A9���ʔ)S�L�2e���քI5�;��s#O<�몫�r~�c!���(�}�������ZTT�Dn۶mg�^���|�����������Ǐ�={�ڠP�_�u��C���j�A��0�^>Ib77553���X�GD\0%#t)��t\Z����o022��>X*�D���e.�@��Z(,ƀ+�%������ޙ�;�h��uM˲YP�Lx���������<s����Z��*�@FAVx#(����E]�P�ɍ��]���m���%�\0���T(-%��ѣw�kyUg����o]�d������G� ��fKnA�)C	\"P/|>*��ύ�0\Z�F\n��H�t��F��lQ�K�rN\r�c�o|*	�!\'~�䓎&u�f\"UU?U!��q��Ū\n���X�I���i�����X�q�s���y\"��%�9Yh͗�8�\n&_3\Z�Et���K 㝎{-���\"���U�DF	� r����x0�Ad�g�⽐2)H9\'n<v���Q�o�8Yr�Ѕb4<\r����p>?�M�~$��).�	�N&J΋��C�H�D\\O�>V���-Dgq�,��U� 8b�G�a���2eʔ)S�L��N�:���\r֨bph�{WWװu���/�\\Zkú�jU���\nà��vϞg��;֭[��\"������]���A]]�7����o�ɹ�5k���޽�v�\"�\n\0\"2Dp�%Չ�� �����9�]mm-�?!�� �Ç��7�577]�J�d���mظ�Z���JB3`�=:Sxxx��2Ha��|�#S���_�6m�K�-艙�5Pk��㯜>O����Ck�ME���P��<��@�\Z5}ي�߱쩉��=;�|ᡩ�w-i��>8<Aƪ\n*\n(�Oť\'��>�����o��x����R������\"����23�f����\n�O��	�.\n��\n����s��LIO�]N\\_eʐ�o��s\'s����D	�kRǧ_[V��6wl���3�W4e&J�ყKޮ�I������6�d�z�s�o��!�C3��N�E������)���>{&�����<<��\\SYa����\'K���d~��эu�Ձ������3��t^�K����|��\Z;U��8z�l�Xnvs}UÈA�H�����:��{�f�~U:��\\��׷T)S����O�����&��(�(����Iw&|�Btb.�����3��XE�Y����s��3����Iܛ	�K=4*���(���YV�n�\0����g��p6�KUTX�!JD�\\n�(S�L�2eʔ��xi5����1\0�ՕA`�Z���ﶶ���	-\0���?�ӳ��L��{�>��niooM�xdd��������������G�0;�����:�@S%km��0}�����Qk9qI%3s��`x�\\gg��/��/ث�B)`�;�PCڞ����\'���l����������\r��lo{CO�˳��TU~���;����s�u��m\n��xh\n�U\r4$8(���u2�{���g��{O�l�����P��|\Z�\0\0����l���\nsb�\0!\rX8L[C�\0���$\r�i¦���ֆ�+3�\rU����qv�\'^]�Mm�JC�����\'��V��7��6���O0�����,���G.L�Ҭ��K�N�\n��x~��S���sj����������xC{���1(w����ɩ�����O��}��g���h�\Z�+�\'ǋ���B��K�W6����4W�L�K��QW���~�N����$d~{�X}~ei��\r�\'��Nĥ[j�_\Z�z�����}|vS6�+�����W�V�gIS3���#��j�˚j�\ZM��k���Z��s�BK��р+j�2���\\� �۟R�J$O�/}{pr�1�<���xus͎��5��@��yٛ��ʿ>ʔ)S�L�2e^��hoo?}�������f�f�����EQQ��d*gsUz�g��G��N�*7n\\�f��3g�TUe��TaL��͜??z���ܱ�ʚ�������d*�0�e��6PU]�b���heee.�khhhkk��^�Ч�RT����E��Şo\"���M���=�.g�R&qRq�mQ�\0(��f_��̮��6v�Ro=fU=HH�K�G���>0t[G��n�ΨQM��\Z�`*D���%[j�7<588SD�����mO\\�\\�\'ۗ�l�&��R����s�{��y�#\'��[��Up1Q���\n`�a��()H�D)���\\g`�Ԗ1KҦ!]��Ӝ^��l��XZUјJ�}�״���XU�\n�����K:�פ\rԲI�GV�)�N��˛~���Vka���٪����5����m�F0������\r�ٰ-�qi�M-@�J60@}�0�yo\\O�fR���4ԋj60+���axmc6*�Z6\"`\"�7���fB�eM��eMkOh3!}��?=p^讁�_�o�d	YK��ـ����K�=���h*�ҙ��\ZS��vUuͲ�L_&�\\a�B�\\^_�k�{�gzx��Gԓ�J\n�N��d-ug�U�t���̒�؋��~��d2?����/,S�L�2e����ҁ���-?���-�:;;�{���n���������A\":7<<5���Tg�\'�LN�o��&c��\'woڴ� ���Z[:\Z눌j����o UUUuRM\Z�M\"3��l*�:z�����eâIE�uuu�,�\'{Kzď;~�}�x��o��\\l�HZ*�޺���9���\0�@j)D\rȟ�E����7�-��%�R���i�3$�n�s�G�_�t��W�������6&?��;��Y���=?6G�W@6��ܭO>v͊�\\��t��MU_�t�gOM���3赻�v�\\�*���_�ܥ�**�DD\nI΋°�`pk��6P��������KKZ�|S�p)��d@N�G�j������hŽ{0��9�~[o��[���>�t�]���C�RX�ZU�!~_o͇����w����;�.ܴ���\n	P��tH�I�c/��������Z%ES&������!�(�_�:$��Tn��/�4�4_x��`%�ww]���=��_�wuG�_nn�S�xe6{���M�����֮j8_C�;;W���Ӿ�o�P����꽲Uxk�D\0���ぽĥ���p�Ja���CZ޿od��+](Y���PIC�1�3�R�=�Ⱥ\\!z�\r}��l!���ųkº�%Q9?\\�W�ȿ���d>^��.S�L�2e�\055���\0�	�ifffll��k���WUUNMͦR����Mo�-�����׿�5�з����^RY�V�8���?�5;٠X*TVT\0��555DI��\\4��#k�>�_-�l/�;q�����Z��1��N6X���7~���Tp���x���%�֤�b�(�d�d��\0[����]O��㲮Om^���ީ<I��\n���	�l�����?|͊k9dlڻ������4=r~�3C�0�`#�WU�\\��o�u���W��~���ڦ�7�������3�T@��%���u˞�iSH0�D>���I�	��X@����\'�c}��+�TWCKD&�$0�ȁIҚ�P�	˫R߼���*�>4��|�$���O��{�/�����K�J`�0	�pЛ�ܻe�nu=4Q���r�n1����!���\r&\'xߪ��]�E�����A	�����w�ZoG�Y�\"�+�ُ����T�W����}x�U��I�T�	-�{y�������ҕݗTW��������u�O�vGK��֎���������ܺ,�!\"h��	���~��W4�D�W�y�7�׾���JnD���́S�#���0�!�z������\0�P��i�9�b-��d~���?**���2eʔ)S�_������%K�Ξ=`Æ\r\'O���kT�1���uzzf��U�;I��\r�7\r����;���p:Q�=���u��~��R)r�/E%\"2&H4E�\r*JD̜�\r\0Ǐ�(���)f$qT%ZBD\09z�XSS�K�yQT\0H� c����*iS:՗M)V�\0T�H&����|��93�-]SY�`E@ހ��.W��z�poe��W��bԚN�Ѻ֬5��̹V=+B�9�p �Н�(ܶ��\\���-�߼�����_{�U�N�EAi���#��ZY��!�>2e�TI��Z�BA�c�\r)I/d��\Z^����.v�&���C������i86�(%�8��Z�ם1;������T\0 ��\'�&J�������yM�U:��*͊����<��_=����UV�\06�H=��#Ci�U�YFQ]�`�y%�!b����8��ol������lQ�)����\0���E�<1�,���ڌ���<(k$jY����)�!԰�S1H���Y����\'���l�Uex1J�J\0k�\0\Z	͇�-���+�6���詗�����o�||���.>�27��-��oQ�?�L�2e�����j�Z�{�С���e˺x���[�f2�������Le&��nmm��hvv�칡׼���[68p�Z2Ɯ;w����X�_u��c&�Ў��-_�TD�!P�W*���P�������ֶ��\\v�va~QE��1&����lv���h�J6c��ɩ��:,Z�ј���Ԅ����,�v*ˈ�,�\n� 3��߻w஭=���Ϟ<6)j�b�y�����ގ�_�z�P>�������\n� %D*����*i�a��Gk�lc�\\�/���u�y�l<<���a��B��u;����\rC�J%.>=]8_�<8��RoZ���}��XwE���gO�*LH���;��q�P��@(W��zI���\r��u�W�\Z�W2Px(�$�f����ySg��������\Z�yg{�_��Lh�����Ć��\'Z+C�Ȩ�K�jA$DGK��\'� �K��ӑw����+T�X�@�\ZǰrSS��[���Lo6�jEE�y�C����Ɵ?2��&�O�5��\n��E�%I���\Z/�86�L^R���\"��-i����__�	�,�\"��LC�M�N��T�զZ��������~I}��?��O�����_Pc\\\\ָ��?t�\'o���\'�O�ؗy���(S�L�2e~6e!������yc�-[w?���]u��hCC]]_��=~���N8{��\r7\\799=1q������brr���[�lnl���?unx���`۶-˗\'9h�+�$��\Z�� �t���[�\0 ��]���\\�`gWG6�Y�@�b\nx�1&\'\'��w_y�{ߓ<�h�\"���\\�Wu��g���\"� �T�*�=c�_~v�o���{Պ��=�๼Xǀ����o=s�%���<3����wıR\n�$��\Z����k;~���3\'��a�K�.�����p�\\�b��\0���fe�\n�/D�\0�0��xkw}�0�\r�mm��\Zͩ�`�0�Ե�������>�|i����;������k���ebx�Ș�mY�TX��%�z�\Zm�/�v�1� ��lr�����O�����j�v]ի�@tsW����(�;�vno�QHoֆRI\0�\"ķu�4�U�单w�)~���=��l:S�K��ӡ@�a�����oK�&���=W����zt`Ie͟�k��F�6$KWc�j�U���5����{eG�W5B�@�tK6StB�;{��(<0�`mE�oU�+C�}\Zi`~os��j���~I6\0k\0���񏏍Z�|I�������<��̋�W,?a��\n�W�h`q����?�27�������(S�L�2?�2�l6��ڲ���7oܺm�g>��۶o=�U���H�����;���~��k��Y,�kj�uu5\0��x�?�$1GQt�;��TW���ٳm�6UyU�X�.��K�G*��d2�V�\\h�H��U�������ޥA����XDUW�^�q�\r\0\0���t���3�U��U�C=��(Z�un���?�i�ڎ�8v��L�;ó���m�۪���S=ǻ.DW5W�wnn��0^%\0�	!X��U�Z�����<���iDѿ[.��^[UU��T��\r�|/���%�^�\nb�1�J���m�$�PÊ�l�\r�A��JFZ��g��%9�^�������\0�P��:���J�����Vx��a*�޽�	y�Oo��\n!����ZuM��Z\"��u�շ�W��ʂ��\r5�7R�����״����-կn�9��$����wU-�s��d)�Ǯh8P!��}]�	jAP_b.4��i&����vW�{��p\"Bj\r{Q���j$~3���+[A��`�������u� �~ki=�b�����\0D9)(���XD����S��\'��?�2�e���/=K/��e[T�2eʔ��+3T5R--̓�CQT��\r7\\w�W���w���Μ9��ӳw��g������[��孷N����IF�@���\'\'�[[��\ny��vo߾�Z�dH@����	̻�`fXD�����|[[\0�3����/�U���~�ΝD*�L��B\ryW�N��8G�D�\"�$!։�\0	�B=Rv���_�-��{�W��L��(R�*���U��݃c��.Yrϕ��OK��H�N#!r����͝�Նo}���i�E�����$X���ʆ$ >]����}\"b�B��-H_� ���E��Q!@�*Dʪb�	�\0P�����\Z8�VC��\0T\"\n�bH\0rF��s��P0�#R����;a����!&o�Sc�đ1��*P;�֧���̤$j��I�ȳ�&&�=�d��x�b�J�q\n(	X&x(C���LR/�(Pb!JR ��\"��(��1����T�2���A+�X������K_�:#Q���ї��*ǿ�zȏڜ�o6�����ߦL�2eʔ�ٔ�xٞ��\'N���[�����\r�Y�~���tCCC[[�5f�ՙ��w���M7]y/�&#���z�����W>����y͵�T����߼yCSS}���dj-/vY\0p�Ap��`\"�Nϝiok���Xl���æ�HDd̋-�$�RКM�ܓ��\'��C-a^c������42��c#�η��&��@g�ӱ�-J������U���^u������Y݊ƚ�6�l��~����̍�)��\"\r�&K���\n׷7�Q�&�	��c-�*\0,��ec���Y�)�!�\n�*�D`Ɂ���j`E+ǢBp)Cp�-���!����*�cUkE��	Ha����5D��f	� h�K���U��M�K�a�1��Dp��I@FHc\08`+0���X�<�O��B8��`%�ْ�z(;P�JP(	+)�!���9I\'���1��b�<%1)�\n5 \0BĤ��+�_���쩟bq_�\0eʔ)S��?!/�4��3C��=��C�bAU����ݻ�����l����_�n5���-Y����S����HRiE�����ܼ}��\'��ɓ�v1����h>���3A$�AD�u�����\'7nZ0��!⋭ɑ����{�I\rdQ���\'�1�����֪�\0B�$\n֤B�D0�V5���w���62��s���ѩb>�!Fcw��C��{�}��ӷl\Zy���[/y�U}�j?|x���_�{ȍE �DVY��*��4Y��̟�iK�\r�\\�1~���=�	I�ƪ\'�����a�B fR�%�	�7�J�D,B���:J��{�,���X9C ��F\"�����DP�@QO�޳�a�� J��R �\0\0Q��r��������\0Ģ�`��50�!@\r\'�\0	��Q�\0��*`�A�A %89��B�L� )[0B��HY	$�+`@DB^�A�@B �?5̀G2��Wr{�\"?:Q���%�$[�����B�@U�L�2e�����\n�}}}�\Z:�bŲ0eo���o}�;W�2���͔��E,�,��~꩑�����^��b����(*vt�\n���������xˣ�>z�ȑ뮻naB�I�6\0�}��o~�[���\'8\'�=��+w\\A�h(Z��K��rg�;�+�Ө��T����w�;��\'@6K\0\0 \0IDAT�K)�!LH���2`Y�HIM\nP�B��@X$b�[E�b��3��SϵVP} y�8�R)�a<iR)39U3��Fp�I���:�V�	VD�~��?)�[T	U�#�P�\"�\0�zU���>�B�x3s�J���ES�*�#\'1b�jɆN)P��#��V�U-T�x�(�<�iª�0is`Y��S �@�+�2�ʐt� ��D��L�lpd$���u(�����BJl<Ā!H�L V%��`\"L�.��d6.�  HPR\"\0� J�YH&F�!*�f���L\n�B�Lɨ�E��B�����-\n��̒�ѧ^� ����?ɸ�����:���^λ����X��6eʔ)S���$��u��[IU�s���߼�����Ջ�ݻ������������\r�466�FLᣏ<�������=�I��T,�{n��Wn��K�.���P��%d�䡇\Z\Z:�����Ғ��X�~��͛t�.�\0�3{�����tӫ\0�̺`.Z<���]i\"%e��$-��Ɂ�GF���&pb��ɇj=āB0��1��Lި:0D᠁�,�VV�3!4x\"8�#�B�d���W�yI���\r�49苝`�\Z!��Ӆ�>}�ֶ���~�\0Dxxt�{N~xm�{�6�\0ퟌ޸����o}k{S����_��}�a�ǁ�Ε}2����0����M;�����,�O�l82�����@<�-u�/]~���w�����Բ��˺�&�x�+g��ۺ�z�B �;9����ݗv={a����F�7fS��5��6ւ��T���gN��i�}թ���!Z�\'�X�PJ���\'2\n!@�$\0#��I�Q���\"�@RS]��[(6̷�<�_ܽ�8�^�.\n���\0�d����/����]�L�2eʔ)����^��� n����~�|>�l�l���ڵ{Y��\'�\00�\\.W(�\Z\Z\ZN�ܿ��#G����<z����]���*311[UU��[UE���Ɵ�청�zՕK��m޲��|K�1.j�HV�z�ȁ����܋i50$櫋?����d���sىrr��/�쪬%�t<��+�o\0���@\\�sl�5�\00H\0��@��P�a����d��:Uc���\"�B\\*��ݕ\r+�St�G���\"i��!�}tuC���Ǖb!�\Z��#�dHYT���B�ڃS~�T�{}db��n^;xˆ����GG��D����w�~�s�^u2\'��{��:���\\���k֝�q�/�Q�3E�����M�ݰ:��/NM��+��\\ ��\0����\"�����\rkOܰ���;���\0H�c���O����S��t���VU�5�c%F@DPe�� ��T��]I\0)�\'o�(�d�9A��B-h�G�$�>E�6N�7����}�1�4>�DC�A�(��[ �x�n�)S�L�2eʔ��eF�H��������Z��;�~��80�;���əg�}@�Tz���N�:u`��Ba���1�\n�0��Ҕͦ:����9q�PMMV4ZL�&\"@&\'��0]W�PU��`xUO��H`�ѣ�v������	\0i���~�1/�6��<x\0����ـ2�[\0�8E|��κ��x0�2Dc���`��\'�LD���1�U��^�%1c��\"�76Px�a8�۔�@Ԓa�0�������� ��O�n-�46��z��W�ol���ɀ�`�5�j�}R�.�����eG�s�KQI5B|�@�}��IL���������A-8�D�|i���/LN����`��?Wpgf�&KBD�H!��wx._�鵕�qg�;��%�,�<�Jgs����6��!�J����N_٘�z���m?���_��Ƣ\r�A��B��w]���9���H�K~q+hqN0_����S�t]t�y�^�bSeʔ)S�L�2�\"2#)#UJ:6m^�����{��O���\\���<1>������\\����*[��s������r9U腣333UU�e�zN�b\n���D�����~qam���OzЙ��c���x�ko�PD����fR�(sss=�x2�*Q5��|�I.�oj������P ����F�JC<%SS�,V�!����/�Ҙ\0�^@��B��CI�p��*J��w��~��Y����w�aRS���Ij靦i�n\Z��EYݗqCp_f��qf����(\n� *n�0��:.Ȧ(\n4`�#�*t�,�oU���?eHR�TU*����Ou��}9IΗs�p�%J/j:�PJ)��PB���}O�/���Մ�]M/I}s\\�\0����i!�]�����6Nd������,7�n�57���Ϳ�7�(a�D��r2~i\'����s��]ypu}�DG��6�+�\r�h�omÈ..�%eT�e#�$�r�HO+�Z_��v��㾯�k�Δ��-�������7��~����k�P��\0\0\0\0R0�P��A�B<��z-^����s��CN�����Y��׫�:�\\�N*..����v�o���=zt/,,��/4hcD��=<!��Y@homm$D��_/!���Jɶm?lX����C;u�$�b0� G��RUR����Z�=�r�\0�q���\'G�JO�kK���͹�$Q�E�����9�9� �P\"1&����Ϯ����$J(�{[QJ) ���n\"F\\��1r�a�D� ��#i�����\'�v�y^~�Hǋ��p��ȋ)I�$��J�$q��+\'��p/?T�s[�i�y�D�yƨ�\'쵽U߶�ݳa��������9I�bWo�F_#dta��{��B8�\nG�-�D9BH}�P�v���v�Q�y<����t=���#|�	e���g��W���ݰj/���<�;(!<�D�$��e�\\޵�=:�wB���\Z��^�/��⾵[I��;{��l d\Z\0\0\0\0i�f���B�/}�xb��}�_��u��(���;�x�5}��?���x�%a�1�����L\"�z����꛹9��z�dL�8N:ܛ�s�2l �cL$T$D D���e˶��5#F+*�|sQ<�V՚���5r�����W�\Z��f�kgJ(Ǩ$��n9�����=�rD$��xF$�c�$�E	�{.q��;W�1�_�$�0�Q�e����L��p�8��G\\R����yn@q�GEF���J�\"JT8�Ù��_�=�(��%1���]�\\�U�xiI��R\"���S�_�^\\6�8���vre~Q����%y/��ψ�_>7�0�Z���.�%�������8�uL.E�!qe��W�\\W��B$N�<��sr��f���ae}3%R%�z0 1�B�mhꝛC�h�w�{�d�˟������RvnQ��ڦf��2�8o`AF@<��\ne1\0\0\0\0H.�C�7I�}�r�\'����u����c[XX�����3�2x0���/���r�x�q�u���v�٧�9w��%:����bj�۲I��N���yyY���-ݒ$�_�]e�k������r��M~�)�b9�Dn69<D�\n?�B	\'��+��b����������&��܌#�y�8�paL��O�%qz�g���Lb�������$�\"�@�(���sL�\\�&�9> ��p�n��4�\"جA9�\\��Qal�N�����.F\\��2��ؤm���w>� [d~��&�Txy���O��${qu�Y+~��4osC;��!�HD��ύ����U��.`�EI�O������&����~=y��顃�l$$@�S�o�������ݍ���{O��������eC%���L򒮙�v�.rbv����|�Ҋ�����C����;3�PB�|�	\0\0\0\0��}����9�?�Z����������Z<hР!CN�z�ǨRBH}]˲�S�ڵ���Æ1&}��7ݻw�x<����A��|����.E�9�S]�ӕW^.�6bkK��ߥ���珐�#�g�*��\\U���^����o���|��Uw��\"�1�\'~F]T�8\"2JZl�?lin?ὄ���1�b�D$9���N~~*;�Z(��p��	\rJ	��\'��9Q�n��O);��M9�$�D���\n�ݔ�s\nm[\r!��@��T	�~.7�����fy77��P��lnMm�g-�Ȥ\"e�H�pߡ��:�������mmR����!� �-�����]Mm���L�����LG���v�����P�]R���/�o��D!���9�.�^Q\"��9ʭ8Դ���k����D\n������eYn█�������F��Ϸ�����up�����yc^���\0\0\0\0���J��q0&r�E(Y���}g�yjAA~~~��11�O��������*~�V--���-\r\r���ޯ_�@�W[��㏻7m�v�Ev-�r�U��*���K�O�Z�f��agI��q�4\\�L)�J��/I�c���(��$&�1�خ��~����JD��n�qD<r�	�(��]����p�X�q��w�KD \\���乇v�~��^����Ĺ]D��AeT���0�	�Q	�<}ʈDG		0�q�\'T\"��.p�I>�y��\":Nd����$\"�E�RF���1�8y)�#��\'1�1 Q�G�1�u��(R��$\Z���qLd�M���q\"���+2�q�0QN)�Db\"�`����$��n�!�\0\0\0\0H�4C�%q�k�D��w76ծX���;�wq��G�\r��X�k�G��pG�)µ�6��������;p��=H�$r�>\r*ɝj���0^H�{�(��8���1B�DG(�+��$QNd���`��1���e��>=к��A�ue0F)W��V&����2�F8Q���1?sh~��e�#��$@(��%�b����/\"�	�SΣx)�Qi�T\"�I\"/qG$�xJ���3�I��uSN\"�H\"��$NrKT������F�&RF%�\'D\"!���c#.9+`�0*q�Ƌ����D\"�QI���J�Q��I�Js*19�(QG��x��I�W��Db�qD��0�Q�/����\0\0\0 ���:�QcRJ��ٿkW�ZZ[=����{�n��9��K��k����۷���p�=��y�{���_)�;�y����DQ��$7eȓR�/\\wu��T�í��w3��m������MK���@8J$GGxƉ��<e�D$By�Q���/���)�]�{fa���8�Ba��ò��C��G-g̷1(oe	ބM�Q�˲�\0\0\0 ���#�ULFiii��9������������,�Ǜ�����#�mm��6Q3�������Y;v���/**��i߆���\Z�c��Ԭ\\�r�ر<�E��\r)d�DwJ���8|�yUSKy�����&��\r�u-��	/��P7e�	)��;�C��������s3��gB��o�8�\r�֭����߇��P�\0\0\0 �\\f��G<���YYY={�I����}��(\n�$�����}>�q���n����n�����|!?�V�R�����Iegg{�^���%g u�_�ӣ�H�D8Z������}�h��L�lw���j�E�#��r��^^�w^v����B3�T�!���G?�6�\\�p܈���\0\0\0\0X\"�NS�*���2D~^Б�;����R��|���E�W�\0\"�ώ�y�K�����$IQk��[8�9*2B�H�㨛cR�0�rGy&2�T�P�*1�Ѱ�)�C�\0\0\0\0�M3\0\0\0\0\0\0t�	?\0\0\0\0\0�4\0\0\0\0\0�f\0\0\0\0\0\0�\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0�f\0\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0��\\Q�3��_\'�\Z�u�YI��)�jXr,0vd�+�\0\0��fB�_q^R�޴�K�z�Srհ�X`�V���\'��Q����(W�-$\0\0���\0\0\0\0\0��i�yw?��R�E��ɲ\"I�]��\Z�\Z��D���o���X\0��ڼFc�O�K�C�rG�*�N8&���\0\0�f�w��Kf=��Jg��^쨷����cr9��z�iK�؅UT���Z�E��/��w���LF�/[��P�\0\0k�,��\\AѭdO`��B����Q����у/�����Z�{�j�v��v�EU-UD���Ҷ:�*P-g��۟~�nw3+b�d\\N����v���d`���+��R�P���u�\Z;}�v,ퟶ�&7��AX{�����a��͇z\0\0pt�Q5%8Dy\n1���>H�𪳦��ZG�ˊ&3�H#��JZ�E�	�q�1.l[͆�8�D�\0b�\"�-��vC���x�-_U_Y�f���*S9�P��!گ�`��\n3�9�~��(�aP\0��\0\0�4������:�ǯe?��g���@�!��p�)_Y���Qk\Zc	I�8�ǚ�$��ʉC�N[�7��d���/m�@v�����>Z\0\0H3,��\ZW�u��i���m�Z�,gͰ�?pN}�8�a�3�[�|���r1�\'騵S55D�m���W	�~,\0�f�-�f���\"tB�5t���*����&/�=3�]Y������ټ\"Ɨ��٬a��b3�ękgU�qF�Fr�Rl�\0 ,�[3t�UԽF��ɣ�IWU�7�����[�4��Y�x��E[N�ȇ*3&��x�ٳ�{��/m���v��.a�e3��6���NnU\Z`���/���1D�Ubw���v�Yq�w�(\n����\0\0!����D:���~�Do�뢳�n�͜>�Z5�O�I�Q�qɓ7�&W$e��re�B\0��\\�sR��gۮ^��\0\0\0�8�O��9�\0\0����Ɗ��j=�&\0\0�Z�Ѹ�\nQ\0\0\0\0\0\0q\0\0\0\0\0 �\0\0\0\0\0y���?{f\Z��c�NG�R����XH3\0\0\0\0 )%o|����\Z��	&0�۫R;����\0\0\0\0RY�.�@+k��@�!�ci���)h@wj�u��?V\r19�Xʹ��1�B����Fy��7/Ul��88P�[��)�4\0\0\0 �se%[U����a�莥�\ZE7�НZ�����UCL�\"�L�P%��?Q�Bh���j�Ԇ4\0\0\0\0t��M-������\"��VE�\0\0\0\0�`a�\\�%D��Rد\"�D���/]6�K&�[�\0\0\0��o����F��V��!va6Ck\0\0\0@jfr�#U���n<$�X�)�A�-�:M��:T�FD�0Iu{�|[E�Or�F�(���g��>}����o\0\0\0HߜD�T�t�F���e���NSf��f\0\0\0\0�WM:���i��In��}�w�\"�0�����ߞ�h��>=�YN	�C����j�vjh�\0\0\0\0\0H)r�_�],��~݁�\rZ3�-�g���њ\0\0\0\0���F�f�4\0\0\0\0 E�-��Y���\0\0\0\0\0)k���	i�@k\0\0\0\0@J�o�0�?;�i�\Z�[�\0\0\0\0@�\0\0\0\0\0΅�\0\0\0\0\0�Z3\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0\0H3\0\0\0\0\0�1\\/UlF\0\0\0\0\0�B���Q\0\0\0\0\0\0��\0\0\0\0\0ؕf:t�С��6����*$�f�.mr-?\0\0\0\0�i��>V�z\\��r�J�g\n\0\0\0\0��ePmU�P��X�@y������+�]3CB���NY7&�o��j�СڱtW08���.v��	;.\0\0\0\0\\��I�?=!ԟao��,��v݃������kյpUFa����s+���OS���(MV7�Q�H��ߪR�UC�_�J~t�k&��]\0\0\0\0��F����_���6�yE�fh/�G�L��(��D1Y㪼�)�_T�_Ʃ/�r�!�\0\0\0\0��~�y˟C�{�Zt��Z$TsTNV5G�o�K�����u[<��u�{�k���(��fF�b�����5?���\0&;Y\0\0\0�AeZ[���C�a[\0�k�0�{?,��\rU9��*�벿Lr9j�F�xb�v�Hg�j�0��[�(�`!�\0\0\0\0�H���T���k��z���������Wх\"��?G�fȕW���\r~�g�ӌb1b��rD�wy�ou�\n��V�o�����Wؙ��\0\0\0`y]\\�� l�=Q7xDM�Ra&9q��s����>f���B}�vHD3��l����\n���73S\0\0\0\0��R�;\\��*�|&�Y��6\r��NS\0\0\0\0\0Ρ�yZۂ!���e��~�OM7��b1�ݽB%��ҧ��\0\0\0\0 �������+Nc��\0\0\0\0\0�:��	!Kf=����4\0\0\0\0\0,�!\0\0\0\0\0�4\0\0\0\0\0�f\0\0\0\0\0\0�\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0�f\0\0\0\0\0\0 �\0\0\0\0\0\0�mo<\0\0 \0IDAT�\0\0\0\0\0�4\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0\0H3\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0@�\0\0\0\0\0H3\0\0\0\0\0\0i\0\0\0\0\0\0�\0\0\0\0\0�WD���q)��_��LR��j9��	�!\0\0\0\0�iT�UY�����S��N�M�J)(\0\0\0\0���\0\0\0\0\0 �ӌ(.xˣ8�J��ڠ���4�#�v	\0\0\0\0H�4#�zmR��\n���9���?��#��\'\0\0\0\0�.��\"�C�x���U��ge��kGtN�L�-g\Z�����\n6t$�~U�u�LЈ\0\0\0\0I�f(�e}W���vU��;��B��(��N�1���*\\�6�\r\0\0\0\0H�4#T��W���_���;�[0��.�=�\n\0\0\0\0 �ӌPIB�k�=��igt�\0\0\0\0H����Bw���G�ޘ\0\0\0\0���ޚ�M	�C��d�\r�nQ�dC;��(��5�9�gD��\0\0\0\0\0�Fc����J��]����������\0\0\0@z�[�\0\0\0\0�bG�f��\0\0\0\0\0DGٷE}o�ɂ1�R�q�V���=�� PIrq<�՗+��n����w�+\0\0\0�xS=Ɏ\Z#�DDBGx�ݴ���{����?���>�#����1�)Ip]r�̌=�>_Fǎ/��Ke%ݶ�������ǧ�Q�!#�o���ӯO���	�¸�t�Qqڰq\0Hgv��p<n�!Ǹ�ddf��Ik@jii]��sI�i�n�W���ׯ����cʨQ47���f��4Κ�ܞ=+�jk�N?�W]C����B�O\\�� F|T�<�Zq�n�ǎD(B%��.۸!\0Hg��ˍUb�R��\n��}tSp`rI�{0V.���t���w��2ݔ��^�\'U�FB[�xB\"F�\Z5z�ŝ;w���&���);33����+�4�{�w\r\r5����͙�I���d���r:���C��y_��?��������aj(�7 \087��O�%���cN��ﻊu��`RFff���gq�n��)�S��,//O�\Zq�������z��QQ�w��C�?���s�u�����}�6:u<G<Xc�)����,�8\\n�P}�i�2�1>z��V�7���ܴݙ��1C��Y�}U\0�d�NS>��*�5j3H� m�mn���I^��{���T�S�s\r�Yw,�Ɇ%��3��}rӵc�o�8?�	8�U�(���	hhhp��}��<��w��z^{�n׮�W]�����i��ed�7f��ݻw=���{V�)����؄*Q&#�����O��5|8��|����呕\"ma0(9NN(�5:�З����7=3��\'_�j1>|��;)P���s���X?�*Na���\r6\0\0؝fHL���[���bU]uݎC�;�v�pI�6���K���\0Ϲ��~՛�#=�\ZԘ�1�BB6�(��>iʬ�=.�Ic�X�*�E*��:Q�VuRO��v�Ν�;��)���x��]v�|�5�9sϾ��~��2�����7�XSS�}����\'�6���J1�,��f��Ç��|��9�A��O�亄�MLKGG7�8���u��Š���>�����9�ㆶ�${�\n��m|oF0	q��IiD�)�\"�#,�$\0\0����o%3I�om��n�s����6}�E�#�Q㪉��<=�<�\Z�It�hnii\rd�kH[��@{��\nVk\"�L��Q}���g_�}�|�ɁC��+*7]xς.�ad�,�����z�y�?_�Uwɣ�I#�W��wN�&*Z+���Cݘu)J��hs�sny��[����k�Ͽm�dS�P�	��P��T�\r�d��Y~���\n\0\0�M3�����P�y�ȡ��~��7��9�Ka��c�{\n����~�3_r7ugDF)!B�������6X�x\\osB�Nclĕ��e�w?Z����ߙ_|��cǍ���R8^<p� ��yyy� ���EZ<B�+e��S�\"j͈\'G�c������?�\Z��aþ���*�l��75!a�(���\n\0���u8%<!�d��u�\\�ž�C���v����yמ�Bϙ�pT�y����\r:�&E����Ys��ϛp���r��wϚ5kH�kƱ��j>��	ݚ�kzt�1����E�^����W������|�A7---Q��k\Z��n���˗����UC=iʒ%wxp�9�9�<B�}hm�a~e��P�N��s\0����\0 �ū5C��u�]��t���o�Q/VSz���*`P�\r~�n�13$��i(_�Q��겫f�C�%�o�(nQ����D�z���>�}�]�n�r������;�=n��|>����UwS��C�B\"w�:�-!�B#j�0��h��ZA�\'�c�i�0��k�.Ծ��*N���#��C\\�G~KNI\0\0i訞�#�0��c��B����ﮟU�S�e\'�|v��$It���k���E���g�>�++:w�2�H���Y��z�ߜ�h�����w�s�w��sD�3��YU]������=q��4���x�J�6\r�o�`vVy-�WQ^C�c?��#�j��,��˿��_��=�н\n\0�Ӕ鳿x����q���!e�q�N#����F�X��#��g�^}��Eg\\7d��z%ǔJ�x�\r��|��g�s�7����S��w\\�u��	���(�MMM(*�Dk������B��B��Jz�ĭ�Ʃ�T�1N\0\0\0W��ښ!��ĘD���8����4�_�����4\\�@��v{��~cK]�o/3|İ�[��g��oh�z<��-���8y[3,a�5��\\���шA\'�6gʖ�.��\n\0\0@�lm͐q�#م��6���;=W���B~���yz�ï����o���G���̚����%BQH��֌4��|m�;\n\n\0\0���j͘4i\"�H�˵jժ��r�כ������NE���<2fr��(@\0\0\0`�	~}ڠ�5c�#�\"@��Pz5{;B\0\0\0\0��4e��O�e\0\0\0\0\0�!�\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0�f\0\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0�B\0N0mqyB�;~�\0\0\0\0i���W�gwn�p	�\0\0\0vb�PJ�l�pc����s�D�V�p�)E��yB�s���|���򨇊�I�fPW���,�|��igj��[���w�%3��	\0\0\0�]rm�^�Z[[������c����v�!�R*IR}ݡW�>�v��Q$�xJ���)��t�����$B��#Z���\\��=7����9�������y��H��|�-\r������S�����O�է��|l?��rg�,,�rg��������ʊ���~��9\0\0\0\08���C�x�o��v���UUU.���=����?z��{�T{l�_��q�pR�@s��%L���zx\Z{�s�z[�o��KL�h��8DAxpѳ�~%�/\n�7��	��uމgI�m������K��Sp�iJ�ࠛ��+�����jv���NG�<�4��w�Kf>hI��lP�6K�,.�Y.�\0\0�����֯_�y�憆�)S�|�����<��SrF����EA��(��fx���I��#L\n�����\'�e�/��$�s</�����������y���j�4c<����q�ݚ���v��D[��;d�fr��Y.�������y]�������{)�Sc�Ҍ�\Z��6�(�n&�	U_T~�7�uԲ���j���}��T���\"U��b\0\0\0`�����n���Yѧ���^:����,~���Ҳ!��n��2����mۖ����SGPF8�(GD�h�~�AX����=O>��|>[�������~B�q�1��q~��S�N�7o����4�QrL�����|�����u���Է7D�sV��\n�iik���K�NS�W�-��:��מ:t��q�Um���v�P�R���d��AzC)Uf��v��۸¦��k�O���BGB7�E:z�͑��\0\0\0�QFFF�Ν�nO�/�ϸ������H�ۓ!I\"Guj2?�޳mۏ�;�hl���r��y�Qru��ZkK�9nժo���!�NTΫo߾W^y�\r�e˖.]��Y���Nkll\\�n��]��n�E-�mJ����٫v�{�ȿ��l��������z������\'uZ3l��(k]a3�S�)G+p�uPU�8l��ɚzt�Ni�0��e22��`�(\ZТ=mn\0\0\0iH�$Q�$F6�<���\0ϻ	q��RSS�J[��r�e+�����\n~�~�ù/VI��0B�]���c~{YA�U�^{U�f0�<�cǎ���͛7:t���������iӦM�$�ԣ(�ֱ���[�uFY��W�������5J<�H��Ƿ��3ܞ�\'�hk���z�C*p��1�H����4���Ԟ5=���t�7ƲѶ�zs\0\0\0@�\\�s��Ýأ���H\"��0&4�u-��QQ����\Z�J�u�8z�uW�v��k����$oF�O��;u�ԭ�1O�jN�z�jii�ӧ�֭[;w�\\XX���ҷoߦ�&9�8z�H��.^�767}�n�-�/u��vn����]vr��Cz�I�4�|H�����cE1�P׉�S9v�ke�)r�}\Z6g\Z�rK�Tt�Ƙ	ĸ\0\0�q�E���\n�^/a�r�V�,�v��(q�ݢ(*G����@�f��P�����y�1&B�N�~�[�⒮T��СC۷o���ۼysmmmKKKMMMcccEE�|��vɺt(����_聋nzx��\'�<�ⁿy���zt�{f��\r-���d&��݊���Nƿ4_��&\'��<1v�R>c*ޞ*�H`Jj`�Z�ƒ\r��z�\0\0�!�$Aݼxz���W���1��݅��{Q��x�^�W9���۾}��]?x���@ @)�8����l&I;w����ݱc��vTZZ:p�@��O:餝;w���mذaРA>���?Ծ7#73� ���V���~ޱ�Os�Y����{~\\����c��p{R��P�㚜ngt�ϑ�BQ}er��C�.p��񤩨�������Q������i�W����,=Ts_t3\0\0\0BHNN������eocYFF��G	!�����R\"��2����UU����u\rhhhmm�MM�ǹ=ee=���mjj::�a�v�Z�dI}}��={��ڶo��r�ZZZ���y�EQU��$��|������]��{�˝�{�@����#O<㗺�wJ?��L��?�����M6O��	L<�۸����`JQkX�`te��67\0\0�4�v��7�|k���<�n�����&ֶv�PP�$!33�_�(o���|��ٙ��(,鑕����Ny/��q��O:u��ݻ���;:��4�����^7��Z�s�h���s��:B�Yg���>:3���:�\n���jko�� 1������-��gx��$qH3��z����-0�)Eў�L�?x�1;o�����n3�*D1�䏣n�\0\0\0���q��=e=J	)U�R:B�8���]P���ޭd���:������v;�*�8�1��J\n2�?��=�l�Q�\rW���v��xdD�F	���u����*ys;g�u�|LaIYQ�q]�ۧ����*V|�WH3�&[QBex%_B6\0\0\0�|�%�3��uB4WNe�������?�WTe}�hW�_d�0�$�2��.~��g�����S�K�0�z�Q{����{z���\'t�ٵSQ�9��b��$7�em��C��\0\0����_��|vVFvV��B2�Bt���L��H���\Z~��a\rD4XL�ic��r3??fb\0\0\0\0��!\0\0\0\0\0`-�f�SL[�A\0\0\0\0@��V����\0\0\0 ���\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0\0H3\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0@�\0\0\0\0\0H3\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0@�\0\0\0\0\0��B�ش��)����A�!�\n�Lj��8�\0�H�3��r�p	�IZ�PfRc���\0�f6��k�~��y��C���x�SSM֒IE��:��p(����\"��Rtː��ю4�\0�7Sc�9�?>D}H\0H�4C[�׭��R��5��kF]x��遝)��Db�I%Q�M�L\0�fZm�x���\0�4#֊{�QBN�m�ϴCL�E���NY�9��$	Q�!�pH�:�곙S��g�D��&/�G13bzV5B���_�۵ }�Z�N�@���^�y&\0��x=i*XMW���M0\"�1T1�r���Ә�O�[��ς�\r�5�`�#R\Zj����m�_R�q�I�]\"����\"�~�ئ\0�>i�ܝ)NUs����!�l!�y)G�z:	g挥{-ʛF(e�9��6|�p�0|��HC\Z���q�-�B�qN[1�t�7��i�+�?�۸�K���\0�f�J�����G�����j\"�[8�X�XF�˹�����`�ĉF�iDl50H�Ml��=sH���\0���@[K����f��k�g��w�/�B�l�g\Z>|��a�V�XA6l�%W�t雷�VK�8��x�4�7�d�Xr�N��-\0$5��i�ѻe\"8�[!tG��eC�箶�\Z�.k��kI�9��x��Zԗ�B���:nh�@CQI�M�M���@�Ž5#�@�`�=�G�j\'bf`��|\rU��)��<���όG�0�5���|�M�`�vT=Q*��bR5�i˒����c7�����Cmno\0i�QϥP/�0�xxطs���YXuP�u�o��g�oMV�#�ĨZ��7#�[2LFX��3�S2�i+���4�7�b�7s(6��GzLH��-\0 ̀X3\r�{*������/`x?�}�\0�f@���3#�K�7(�#!D����}�M �qDV\nqn>��g�8�`�\0�\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0Rn��-\\�  ΀2ئ\0\0H3�2��Ag@�lS\0����\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0�\0\0\0\0\0\0�D�ތi��8\0\0\0\0\0�p��w�|=��gN����j�땆����@ \rㆂ�p!2N���\n��\0\0\0\0\0\0i\0\0\0\0\08�˞��T��������@ ���5���E��\0\0\0\0�4ô�Z޷O����ow�޽fՊ!g\r����m�7U��?��o�G��!^���O?ɟ{�����QDK��^MA�X���@ůD��:�H:_�6G\\˘s\"�!%~�؁qӖ3�.����!G�H�BQ��\\b\\����F���VN��QU$�f0I***b�1�(������v�ލ�!\n�B]��n\nm�5������6�j����ɓ\'�v)�j�������#P�XEs�n�D�m�#�A)MJ�nd�\nW��qԑ*�sY���v��bydTaqHH�3W��7����|�P�3�.S��Ct�R�����s�5�;�J���l��G�z��G-<�(�D��Q�P�����̈q\r�\rg�Pkg&�����L�p�氤�%K��,)`�0F}$L`ܴ+�]T3{��8%׮a�(�]���zر3\\��b�`G]Nb�k�U%!�u1���ǜP;f\\�\\�U$�lj�>����\r�q)H���\\?��u��	!n��S���ɟ�ҽ*��x\\�O��P%��w\'��D �Aɪ&�P��_tQ�?n�+bf/SF#�v\r�ͭݸq��aO��B��I�{�\r��z]̬��1\'��*�>p��}��\"�\"�����jOe��\'BZ|�=@�v����G1VP�.P�U�P+��X��V�1GE ������q�\rTؘ�/��5,���Fy����J��ط����s�^�����4̿<$yk\\H3���U���wȐ!_�wAߡW�v�Y������?\'0Ŋ�g)(����E�!ݥ�zs$W�>(ţ�YU��7+����w�X2�x��.�����,��xDFwk�YHe�;��`���䤔���#��\\H3RP�#T�2���нW����~��^�m��b9碎C��B�&���I3զx0����KN쫙J��\r�3��n\r;�G�X�g[d\"]�Raa91?)9ӈ�%�I�{��|)�o���YP4}�+򿒒�Q]v�,[T��٨wQ�]V�^�٬�D�\r�����4k8s�;P��@��4H��5�bG�e5���Jbg���p�> ��Ct���ȄݠQ!�5�=��I]� ��f�`�Q�X~h6���3�̂���g�>٢\"B�˰C�o*WG�3/�0�3��C-L�ґ�\0\0 \0IDAT��s|D�ʪ��PQH�氼�93aˏ�,��?K�fի�W3��G�9R�=�kWВ�Zb˃j�B\r4�Y�����ڽ&N��e]b_�Hc��g.��I��&M��ȽaǙ��<�Tl�gӽ{YY����+:��K#�f��N�\"y׫��f�W�ߕu�n��aѭ����g%Y(pT�uZC�Dܰ�\"�õ�:~̐�/ؤ)3\'L��ӦNS�N=�z_��_��Ɨ_~Y���3�����g�U��ݾ�\'\"_pBy1�q@�$P��#~eq�\n\"\\���p6u�����;�Q���s|Fv\nMʔ�̂�X&�Kq�]̒@A�o���1H�\r+�p!2�>h#��H3�nw~ǎ����h�  P��\0�Dy\0D&ͣ�\'M\0\0\0\0\0�\0\0\0\0\0p�(�4��\0\0\0\08��4���?1�k�R�f\',�+\r��́@\Z�\r�Bd�U.:M�š�ڼ�]�=y��-�\0\0\0\0���[�j�֝�_|bzUM���~��\0\0\0\0@�q!֚��܍[7��شsN;������#\0\0\0\0@��v6l�X���u߯������ZZ\\r�1e����dfdF4��V.ݸuӋOL��}��=3&O���E�\0\0\0\0iF\Z�k�7��\'�I)%��=�q˦ş|XZ�u攧���p�S{u��k7�����ֶ�ѿ�b�� �\0\0\0\0\0�q$��7[�[}�<ϫ�\n��g�;���k�u��o[�POv�9+V�`��Ғ��Ǆ����������=���g|ة-�����k�<9����ο�7o����b/\0\0\0\0@�G_�[ջ_����ow�޽�����i�j767]}Ǎu\r���z�;���fL]0{ޅ�\"�|��;%���e��G�N��櫏ϜVX���o6�1���\'g��x�m7�g��r�l[��ӧ���������q#\Z�Zy������WE4b��*�����ͧ�d���߆*Z�-x��k\\z\rJf�Zy�UK�r���8M\'ޣ�Vr��B5$��u��bfM\r�e~+k����(�M����00�� �đfĝH���bI�(��1��=z�����s��O������q{���Q~^�������������}���~���9���.xsu��ُ?[��p븻�����9�2��\Z��CU4U)����z\0���\'��w(Ql�P�\0��q���éQ/T��u~�\0�N\'ޣ�_r�\\����jӧ��%)r�H7���I��#���9�h������C�/I���}�-w�y㭯<3k��Y���n�믓������<nwvf�ڍ�n���G��w�]�����ek7��yr�q�������ٻ��T���r��3U�^�_�!�?S�,�c�O&��Ϻ? G7F��\n5y���Q�Iˠx[2��j��/�N��R����$K%F�̪#�n��/B�C��$\r��������?KF���:�UG��aSkF �v���� ع���4��c�N�|���G^��G�u;�wVV���,��1v���[���n%��&���/o�R��������ƙS����r~\r�d\rRۙ*Q]h�^��� �.k7M��*�9¶k鎫;	FB�w�n\01^�M��l�]�����D^��య<�\'�T�Ӊ�8D��ԅ�i�z��fBca���t�g�.����._����DQD!3#s͆uF��ޫ�6�1��j���ZA�yx��/�ag_����[鴭M���t۫����f������(H<\"-�$t��`�>���ꋺ�r�N��FUq�#e��r��\0��*K���%#�L��Pm��HH\':��0Yf��M�5��#ꩥR\n��i�3q��������rI�	B��sі�k�v5���k]ņ����|��Wϛ>\'�r��~lgES��V��r��O��ntոg��}��.F;C\\q�N��E�̕���	o�	[r�yiZu7.����|A�\rr�Cc���g�P9F\\�bU��4�\r�_s���DP���\'\rlmk�����_xϮM�i8T-�B��آ]A5n�ۓ0!�?����֢[3N�j���b��:\'nQ\'�)��x��\'B��m[��=���S���x���w��6��#���c�׷�-=��7�Q�c���{�?�je�߶$��R��:U��q��T����Rm~�x�+KNm�0s�W\n9F��Ԛ�oQ�(jw��ֳO9���C\\�7){�+o�09P����p��5��D;���Dj�n�T�v~~��7���O�?��i��QO*˫G�N>ڙjwp���a��;���\r�FIx9I�Z���:\\����Q��I!Tb����x��~�bER�3���a�G�?��j2��!Ty���I�&<ro�q�-.������{����1����t�����iw��\\�4_���b$�TZ�T�FNZ!Pn�͙+�n�~�!26Gu��!	_�ISfN�0!��M��?u��\rUUU�R�W� x\\���@\0\0\0\0H\r6�y��3��|��z��1\0b��\0p&�\'@�n�� �#�\r\0\0\0\0��)\0\0\0\0\0@�\0\0\0\0\0H3\0\0\0\0\0\0i\0\0\0\0\0\0�\0\0\0\0\0H�4��)gGSUb�w�bX�*�W�n#fl���@ܰk \\�S�.��[��~z�}��o��X�4�F6\"\0i7��qrT�pt�����y�z��S�y�\0\0\0\0�i���,_�e�ֹϿ���jч�# \0\0\0\0�n\\����3{C��93^z�9m�m��\01\0\0\0\0�ig��\r��Y��|]UuuKkKi�Ҟeǎ��K/\Z���Ѥ>Y�ن��s�����t�g=3#/7\0\0\0\0�i����޿�{{�B�OJ)!�����������u-�=}��#�39������|���_lmm���o�19\0\0\0\0 ͈#�H_~�MK[+���@NV����r�{�Ȗm[G\\ra]}!���[��w�\\Э��/�}㦊���=U{{����G��Pة��h�7߭~��9��ͷ�s�[/��Z\\��\0\0\0\0H3����_�ЧwI�~�{��ݟ�Xz��l[�ƦƱ�_]W_w\\�^,xw�㏽�ւ�/����{�(ٿ뗇&>:k��S/,,��یs��+����Q/���E���!��m�r�S�O~�x`𫰿�t�V�����;�ш������!��Ǫ6�%��pj�B��J5�P�3��Lr�J�\\̔E׸�\Z��$-�򺫖<�p݁q�N���ߠ�.9�R�\Z�J�:�u1���2_0�/��\'�q���$� `<�̑\n�K3I,..�$�R�S�ߣG���J;W�������\'�ǳ�e���V�--�\Z�6/\'��?��S�~����?>5��4c�k�~���Μ]�P����z�9�sm�Kw�n����ӌ�4�ß�K���I����ff�%0Tb�n-3�W��F�PU	�M���X;��Gѭj$�䘹4&�/�#TiI��KR��:4s�[�BH{{�n�!�/I���}���v��q�Y�L�9c�#����벏>#����<�Ǔ���ݺ�n���\'�b5�O��|M�ڗ_�C���[��������^&��Xх�זt/S����.ט\r.��*�D�\n��oB�(�l!��E�TO~$�sOȵ:��U�x_f��Z@��19��5;$�\"�����U�H/���Ju�6kGz�<���I{Ϫ����D��@ ��1c� ع��x��T��C=u�ɗ^4�?���|l��,�˵�%����|��o/�^�-�D^x����oxq���\r�c��z��3�c�5�x_ӵ�t�@)N�ơ��mMʊ�D¸	K��8���t�<l��¦�e6��R�.8��$W���d���G���긦��;o����8R!� ��X��mF)����K������ʬ�DQ!33�۵k��<��w�3�1>_����ZA����z�ξR��Q�{��ތ���WG\Z��Ef�i�]�7/�򘙬n�s`�F�;��rt�/���!;��\ZET��V���2��%�Hb�UG}OZ22X�n�&�]���u�d�17����a6ꍋ.�i�3q��;�_��[c����rK�$BqQ��u�j�yo���|ݜ/457]8���^���C�N�>j�a���� ���ˁ#��u�5�o\'��G�8��m��$�;.��pH�I�E��ʺ�Y6ԩ���C�1ƠstQ5s%;��}�|�`^�V]��V����6m݂\Z�^���2��+�π�n��J���H3��x�\r7^w���/|oѐA�[[[�ߘ�,Ϯ\rV�n�u&�Xu�I�2l��B�XX^�V�	E7��j�^�\'aB���9�X�q�AOU�R/�{T�j}���Ȕ?ZU��W$�BB��7W<��S��{n�3ɒc�=�劗��FI��c|���i5�oSF\rU�C���q�H$��L�R�l�*�ιICuӽ�Q�2��o�{_)kg�������=����Rm~�x�+KNm�0Mr�8�����z�m�;�EQL����;�9���N>��Jps��-��0?�\r�f�*^�{a�]��w�+�&��d�����!T�ڹ�F׭�붇�-$����*����{�b,��2m���\"�Iey�H��G;S����7�tt\Z߹�(�,\'It@U��O�����Q��I!Tb����x��~�bER�3���a�A ���?�!�*～4i҄G�\r;δ���?=�����Eݻw/++�������~���+\"�����F�I!��+U��́@\Z�-�L�k�؏��:~̐�/Ƥ)3\'L��Ӧ��#��j�ꪪ*J��+A��y�G\0\0\0\0H\r6�s�r�(꿃��ܜ\\l\0\0�T��$\0H3���vt,@�\0\0\0\0��4\0\0\0\0\0H3\0\0\0\0\0\0i\0\0\0\0\0 �\0\0\0\0\0\0@�\0\0\0\0\0��O�81�Ǌ+�\r�o�U��ߺ�2_�&����v\',����� e�+����#}�Ǟ��a�@���$]T��S��b����3ʷ����Ą��K���X�4�F6\"\0i7��qrT�pt���Cu�y���z��[\r\0\0\0\0H7H3��t�ʭ;���������?�\0\0\0�t�B�5����nz�i�vf[{��G\"&\0\0\0\0�4#�l�\\�����__]���������c�.;���#/��ȌhR��\\�q���������{fL�����\0\0\0\0Ҍ4R�P?n�C?~O��RJ9X{h�M�?������)O����&���7�n,��mm���^A�\0\0\0\0H3�Hr�o�����y�W}�32��w2\'0;�|����0����2�sV��j��y�%]���	[7�����Wu���{�O<t���S[��{����yrFsK˝�o��9%E�(^\0\0\0\0�4#��X��w�>%%��ݽ{����p�m����t�7�5��*���ܷ�1u��y�;�����>[�:yΛ�>>sZaA�ۮ��8�X���s��!�����9�w�ͳm]�N�.Xt����_��e�ӴJ^﮿n��U�ظ�J�_9{��F�d,ʃ�����\Z�^�����V^wՒ��;0NӉ�(��m�P\rI�c��YS�p���ځ�w�x��UL�#��628��� `�H��\"����%I��2�T���ѣ�����?�~�����Y��G�y���Z�k\n�����z���W.����g�<o�f������kf?�l}cí��~n�S6��J^��@U�0��\r���bDD��ؽ[�Ҝ�ҫ͇S�^����&��W��N�G��䘹4&�/�[9Ԧ���i.T��A���\nlM3!����9���$Iv������1?��SϜ1wքq�w�]���g�z�2�?oy���̬���n�S�NaR��\\��\\�vc��\'gBnw����3;++��DB���=��%��T򟩺�k�䊾<<�j7�j�v,ݟ�D^am�7.�\\���e�H�D=�3J�]>I�i�V�\"�<�:��*��.�)�+ԊDz����I���1.�vt��cHG�f�@ *�`�	�`�j�{�ԇOb�=:u���y�;�����YYY.�_������3n��󻕔��ȋ���aK��S��ol���gNy��9��Z��3T)��s�]0�nD�Ϫd#��*�t�k�w�^1^�M��lƽ#Px��AC[2uw�zBJu������w���;>8�`�#U\"�Bc,��6����l������K^���(��(dfd�ٰn��3�{��9�_���]+�=���7��+�ʫ��[;�v4O۽Z�uB}�b`�+��{�O�����d�,��G]_ԝ�Cv\n�5��6������~�ם����VrR��\n�s�M����_�O�{���Z3G���8�[�ʿ�zw�Uw��v�$�!PԹh��5]\n;�\Z�w���b��Mkjn����M���C�:��3��S��^�2N�NՕ�(j3��Z8��k�a�O��ntU;VD5c��e��N��E���׈����7C���Ҵ��l\\n\r��\r����d�:�dL�e#��V%`H-�f���kn����z�|0����mm�<<~��$˳k��Se��|51�ʈ\Z��!�pB-9FE7��j�^�\'aB���9`I�E�f�29���b��&c�bI5��vI��C�P�m�S�g�~�3O�c�����T�T�l$�@k��f*^Q_�JQo�H7���c�xӽ�Q�2��o�{_)kg��y����5E����Rm~�Ė+�m�f�3��6��Z�#~lj�{��(�I�;�٧�~��!.W���t�И�Wc~t�ꨎ}�;I�]��w�G�!t*���;���Tq-iRYտȠ��80 m\'�L�;���߰��h|�U�$��$׳�t��IS��4>��<)�J��߯��P1�hvN+�&����X�G\'�v~�*～4i҄G�\r;δ���?1���7�w?�{YY���?����#��K�#]���J땪�(5�	�[D\0q�s��1�#D�樎3$�1i��	&���b�����޶����R��J��=|�(\"\0\0\0\0\0���4#ϛ}�qC����\\o66��Д\0vB\0�q�v��;\"�\0\0\0\0\0�\0O�\0\0\0\0\0�\0\0\0\0\0�4\0\0\0\0\0�f\0\0\0\0\0\0 �\0\0\0\0\0�dO3��rv4U%��,�����z��6Ba��H���\r�=%��@Q����\'|�g.��	���J�m�@as ��qC�@�\'GՁoG���8x萷0�g�>�7!\Z\0\0\0\0�n�fXo��[�m����{��}�>\0\0\0\0�ƅX��9�7Tl�3ㅡg����vѨ\0\0\0\0@��v�o�0��k��UUW����v-�Yv��K.���љ��M�%�m��8��}>�Mw�q�33�r�a\0\0\0\0@��F�����븷-����B<����﾿�[����g�?�<�S��ګk���}�����Q����c\0\0\0\0\0Ҍ8���wߴ���<��*�de=�,��7�lٶu�%���B��Ͱe_�x���JK����7n�hko�S����^��_|�o���ۋ~���_����|�=w����%(^\0\0\0\0�4#�>]��	}z���׼w���ي�;϶�nlj{��u�u������w\'=�ػo-���	!���QA����~yh⣳�Ι<�����;o��8�X�r��/�����=>��n��lH�!$��$\n^~�T���VZ�T+�b����F�*Z��+�>\"&�[(7QPnT����@.���D���.&����ا���rvvvfvv��~嵯�����9gf���3�~���_�\\��Mn�����SBo6L� ����9�Ҳ�7�?�N��i�K�χ^#�p��f�`�\r��M��,��K��d&i�\r��l��	9]u�E�z�J��TȦ��\\\'�={*H.������\0�b��ZaR�aT�Hp.��$��L��_����<O0��v�ҥ������qu�����k���mڞ<u� �c����9���_�:}���?���ҙ�0c��w����\r�\Z�\r/�3�5�c�p.�^u���\'X<�9-:-�tB������Ɔ�&���^e<�z���\Z��[�]�Ջ�_r�4�@��E��ZY���i�U�-=	�?S��0C����f�#�\Z�����ݮ]�~w��Q^6yB�ا��~����o�$i��%���٭��V����xu��` ����[7�ٿpv�$IÆ��hivv6�ʔ��mK��T��z�+�B����p�))�tN4�~�R��+�m��xZa�nf��-��zt~���$���ZE��yXv�X��\0\0 \0IDATV�*�ɘ\\���_Z;k���CJ�f�87U��Y�1�ф�^����j��`���ٹ۷v�1���`08~��;z�~��WV����N٭Z�����`K0��\'�_Uٹ���Jfϟw���Jg5�k�ؐ���;<ƈ�,�5g�.W��C��e\r�ʬ��:;��v��ןjVo�Qq�º��M<:�\\\Zʒ�z���	)��U�e,��Ňyԓ����9�p�Jd�!IR0��j3��sd��<���͙���}>_VV�G��u��c��u�c����g��|�cF�}k��c����:�D �p��N��y��&��]��F���	�d�*�\Z�/�~�C\n�=��6���Z|�ދWhC%��=i�H��zr֞,P�V�ŭ�.�̊���ѳrT����~�ڥo-{豟g�g�ϗס��<��k��Z���5�e�/\\�0`Ѓ�.q~��ZՋ	�4^)Tm�2P�qkh�����\"�����s[����\\J�&�U=���#�l,U����t�W]�`�/9�4�ڞ��.�T5��T�����L<��j�F�\\=\r<?�ĕf��a���P��YW�붞���#ƌZ���dyv�j �ZtNȡ�\0V���HÁ5x�������8�����+��$L��Oܟ�JI¿]�f�C|����ɘn����q��퉒F��ک3�_�M�M��,1Ɔ�B�z��9m8o�xn�r\r���l�ՙ�0�f����ʿmH�c�������sf���5E����R��Ė�}�z�S�j��O-#ư�]��v����O�T{ꉑ}����޷��\'�;Hu��q5V�iV�u(�r�ȑn��pF�e������m9��^�گ|TL#�L�\0)�(�Ty�+o���Չ�;7�Z$��$���T��IS�s���:/\nZ���#��Z�)���1}��ʹ����\"�����I�W;?�y�uIII��Q����ft�1}���+;w�ܵkW�O���>��G�:˫>�u3����˭yDB��\0R0����Tk3�8\"elN�q�J�f�L)/..�kSc����U��������>��|-33��-��\0\0\0\0�`S��.\'��������עEZN�2\0\0�b<	@�a����+�]Ir\0\0\0��\'M\0\0\0 �\0\0\0\0@�\0\0\0�0\0\0\0\03\0\0\0\08F�I�&��ٱcGa��?����S��=�U][���54;a3L��꟮�/7����H�W�\\ҍC���HI�T��[~��;���5�+�ûߜ�=�_{�	��~�`�Pd)�L7\n�E�89U�+���ę��ʽ�c�~�?Jj\0\0\0 �f�oۮ���v|�����zw�\0\0\0�&�$0���:vx��3�~�OSs�}���&\0\0\0 �H9�ԮxwM��N��˯�\Z���V�t��{�˺\"+�Umڹ�б��^�|��gF�M���:�\0\0\0aF\n9{�a��k6����x$I:��3��^���y˧���{�:׶xś��̛Z���4�W���^D�\0\0\0��=���4^jnѢ��#�כ}E�]7�N����c;>`蠳�\Z$I��}w��p��%������cG���O~Y���|�ߏ5.���lX��f_��e��z��c��V�wȣx\0\0�0�B[�w�xS��|���8qb[�_���c�n��xaȈag�5\\���w,{�lڊ�K|���$�_�:��\r_�?6q��7�T>���W=�د�1F�_�R�r�����ЕK���ڶ/�JKCo*�~Z<1�Q�9Ce�\n�i��;~�G��cZ�����k�����Cb��Yd��^A�L�B�wٖ�$r��D���\"�3�꒣,�)n:׉�EϞ\n�K�Hly���й���i�yC��V��R ///x<�`0({�ҥK]]���=�䏟��\"3#s���msۜ��TǼoB���9����_ܼs����ͬ�%3�xsO;�/��p�\\��\'_�<��#\\��W�(�.�gNK/01�Csrl#���^e<�z��*�\Z��[�]O���y>�\'f���\\_�#!�A��_�\"�;�a�yC�LE�#�a�$I��ͪ1F�5عۣ3�]۶}��S�`N���c�~���	��Z\'IҒ��232��Z�?T���1}�`@s4������x�L����O.}�O٭Z%{�P\r!�nAW�:ۖT��B���CCR�nR�5E���\0R���ړ��6+��\r��ꑱ9ͪT��O�~ڨU4q�pԳ�V�N���GA��I\n�DV]���Ǭ͋��C8��0���z�^�#�|>;w�Ƿo���-�`p�ɽ{�x�}�ׯ����V�Z��h�e�{�`��?��\\��S~��J�-]x�h��)�4�?7dİ�)�8<�P��pAc�r�5�棤��J6�2,�Z�\0�����g+�F&�2dixfz�4��യ�y�\\�m(ZW.َGM\n���l���Y?-8��t����a�$I�`0��<ρM�~���U[�O{�������+����qo�u���[?�zv����¸U�߰s��5W���г��jG���ך3QNXQn%�����}��w�ꋪ��K�VÁ�ZidB	�r����B�*���IKF�ԓ��dAb�C����lk�N��qfG����3��M�N3�)--m��������#�e���A�����G��]��j���X�Vu���/θp�⃿�����1��\r��j�D\Zʶs�N���-������J|�̭\n���\n��8`eMH�zd��96Cp���T�ӒS3��\n��(D-9�4�r���\n>UM.�5���܄rMa0%F�:�\0a���?2��G��_��{=o����i���^�,Ϯ\rW(�nx���]R���Ge���%�]cU�_u$aB.����RqQ���Z�z��3d�	�â�pv�H\ZI`@�\'G��-������^J�#44_6T&�&p���zN��۷�D��O�P�6_)U��c�V\Z(�� �2���Kϙ�?)���	k�ֿ��˃��K���~Y\Zc0�B��ތ�wx���$J���*���;o��Wzz���\";EvP�|X�꜑�����iV�u(�rA���\\F����r\'2����GQq����E��E���D+*7�A>�/U��{U��Gu���\r�Ix9I��V�V>iJuN�Y]�EA+�w�qd8�	%�qǗc#(�1��-��\'�o�ƃ$I��{�KJJ�\'���̌�5û�[�h�����ܵkW�O���>;Q7��9��H���ܴ_n�#�� �����ڸ�#R��T�P��oFɔ�����65���~ǞO���{<�G>�/3=���RD\0\0\0\0w�)��m���[�~�/---�e6�\0�[1� ̰DFFƕmۑ�\0\0\0@*�IS\0\0\0\03\0\0\0\0f\0\0\0\0 �\0\0\0\0\0�\0\0\0\0�f����_�O�C6��W7�[��Lv��� �84H.���KU2�+��L���W}��`�R0�H(��@\n���\"e����pMY���3-��^׽[�ä\0\0\0R\ra���Tm;�ɱ��<U_���$\0\0\0RM:I`�Ys��(����M�M?��}�	\0\0\03R΁C��^�����ԩ��.�Xp]�k���~<0+++�U}�e���Cfͻt���||�̲ܜ\\R\0\0\0�)�l���ώ]U�&����$韧O���;��Ա`ni��~�_����x_����566��W���\0\0\0��I�����uSc�-dy��֭���yW��7����~2�l�YI����{��e�;o��TP���:\\�����O��?{��&�a|Ե��\\�{�+.^�X4j䲅K:��S�\0\0\0@�a��U[��ۍ���5�\'Nlڱ��{�۶��/��ؐ�\rgo����V�S�ҋ�,[q�}$Iڴn��]����\'M���b�ڷo?�7O�c�m;�ή����bp��mr�ض/�O�z�a����GQ�TNO�Z�)Z��������1-x����k��pS\n�E���bYtťWP2��І�]������:Ѣ�X��m%GY*dS�t�=$��\\VN��\'0},���@vXzP��R<R:���yyy�@����A�k�.]������1W��g����6moۦ��S\'�;�?�m�s�{�թ�7n����N/�)3��x��=*��p�a��93_�9ƈ�B�U\'�F��ĢhL��М�H =�W��^(�ʪ��V\0s�c�\"��=M3�y}�Vֻ���1F��a�I@�LE�#�a�$I��ͪ1F�5ع�O?9�]�v���;��l��O����g���$I�¥K233�[e�ޟ�������@Pk=�n�W���\nI��\r/zk�����d/�k�ڶ���ζ%�f�п����(������,�D�l��а�:*.���J����`x=:�(��hQE0�i�V����Q��Z�:y��u6��9c�_���c���y�!�pz���z�^�V�}>���}k�3�O��\'M��W�~<pe�ꫯ�ݪUzz������{��U��:i�d��y>>8�tVù�A�\r��j��c��\Zj<kpZc�r�5k�,�tF�\'\nfb��V�{���.I�Z[r�W�/<�ա�,��������Rm� :q��i���8��t���\\�!IR0��j3��sd��<���͙���}>_VV�G��u��c��u�c����g��|�cF�}k��c�����NIH�2��5Վ��LQfP�,3�b*�������>��;c�E��rڥW�Fe�V\Z�P��\\b:�h5�S\'3|OZ2젞��\'T7#!�����̈�M�\05����q:s_h��gJKK[�z�ҷ�=���3�3���������p�5ZK-Y����ꊲ�.^0��e�8?�Pm��~�z�wBZ�U[��fR�2h��_�\n;�O�!\n�T�UkB���I�q���RUOKvL�A���G-9�4��r!.��OU�KAUMd��:���W@�#���c�0CݰG�{t���׬��u[����cF�zcy�<�6\\ь�W:8xF|ˮ\n����z*\0�,��\n���\"�H\\��90��Z3v_�R�`�g�~Br<��yW^�*��K#	��H�ԙӯ���צ�L�#4�^��(��4,z�T�3������Ԍ7t�iBr<�([���1U+\r�����J���n�CB��S��s�K��e�.W�\\ڌ%`2�v������]��v����O�T{ꉑ}����޷��\'�;Hu0��2q.ni�u(�r�ȑ�\\O�F{g�8�ܞb�i��H*ӫG�A>�/U�����Gu���\r�Ix9I��V�V>iJuN�Y]�EA+�w�q$�R�C�di%�b<��a���b��g<H�䉼�����x����X[3��Θ������;w�ڵ��uuu��٣�i��UźI�M���<\"��R\0)�nq�`�5�p�26�긇z%|3J������1�}����S__��xd�|������RD\0\0\0\0w�)�h��{��}�~���k�\"-�u�\0�[1� ̰DFFƕ��$�\0\0�T���\0\0\0\0f\0\0\0\0 �\0\0\0\0@�\0\0\0\0�\0\0\0\0�ŤI����ر��_�L��ɩ�r������e_�\Z�����V�O�엛���Lv��+G.�ơArq�$]���-?�qŎ�{��\Z����oN��̯=��`�R0�H(��@\n���\"e����pMY�����ر[�އ�%5\0\0\0�j3̷m��c;>oji�?N��q	\0\0�T�N�k��;<��}�ӧ�����{I\0\0\0f���GjW����������M�y��~��?���e]�Ӫ6��v���ySK/]���3��&O�m�C\n\0\0�0#��=�0v��5օ��x<�$��יCG��������S^���B�k[�����j�M-mlj\Z����g/\"�\0\0\0\0a�����j\Z/5�h�B����;\"뮛z���v����0t��s\r�$��ݾ;�|�b����Wq��#M��\'�����_>���Ǐ\Zumk6��S���岋_=��1KJ+�;�Q�\0\0\0@�a��ջn��[~��}O�8����{��m��_�0dİ��\Z��z�;��X6m��%��CI��/]���/��8mrś�_*���ʫ�x������x����~b�ʊ�mrrmۗA���7�O?-��(꜡��e�4K��ɣ��1-x�x}�5r\nG�j����\'+���+(�IZhC�.��p�DNW�h�z�^Ķ��,�)n:׉�EϞ\n�K.+\'�?��/�7�:7�Uć���ƺ��2�.����@^^^ �x<�`P�ڥK���:;w{\\�?;�EfF��U���9�婎y߄@9�s.{��\'��y��O?��Y1Kf,^�枚}s_z������O�6y��1F�v~�:Q*D]\\Ϝ�^`b:9����6�Ä��W��^(�ʪ��V\0s�c�\"��=M3�y}�V��Vt~]�\\�d��\'�3Տć�$577����@ `�n��͈vm����Oق9�cǏ)����\'lzk�$IKV.�����j��PMFF��S���\\�wn�����2I���>���?e�j��\Z�\r1���%�f�пn��Pv+EF��XQ�菊\\*�Y��h��R�$.���J����`x=:�Ȭ}tΩ#I�i�V����Q��Z�:��+r��\')TYu=O{�h��cHX���z�^�V�}>�����۷L{�`08q���=z������u�}c�V��[�ز�`0���V.\\�)�@k%�.<x�v֔W\ZΟ2bX��Wc�vS���\\ьژ���e\r��L����.�:=�c0�xku���\n낑�IQJx�4��യ�y�\\�-\r�Uc$�D��Zg�t=��~ZpΙ�iۓra�$I�`0��<ρM�~���U[�O{�������+����qo�u���[?�zv����¸U�߰s���W�\\�+���){TG��{�D��\'����J�cã�T���U��i�^����Ȅ���	Dk��D8��IKF�ԓ��d��f$0\\\'�β��;I�ƙ���#��MX��Liiik���?1,#==�|�Ww8Z���Wk-��귪k�~qƅ����%�Ώ1Tc�ȑ6Q#\r�ꚪ-[j3n\r-,����x:�R��ۚ�T�q�ʚ�V����#rl��vc���%;�fZ�\n�i�Zr�qiJ=�B\\n��&������NN�T��i�q�����>��Ї��*��{=o����i���^�,Ϯ\r���f���UKH�HC:Z�~�;7`��\Z+�����#	r	������j��}�JՃ�}7�X�P�1,Zg\'����~rt�ܲ�\no�9�d�1*�~:�\'���k��S���-�0�e:�F�a�N��?�j���o��X)s�:���������]����j��:�<K�$�1엥1|��7#��~�?�Rm䯊����o핞��� �!.��ը����n�XI{i��ʧg��.p�!{��e��9)E0�HP��hE�F9�G���\\y�j���N߹a�\"	/\'ItB�*��\'M��)>��(h�<�g� ��8��rl%8f�޾%8���-��x�$�y�uIII��Q����fx��c�����[��v���i]]�g\'���:���u3����˭yDB��\0R0����T��qD�؜�����(�R^\\\\�צ��~��������z��#����e�g��]�\0\0\0�6��-���p��o𥥥��&3\0\0p+Ɠ\0����ȸ�m;�\0\0\0H<i\n\0\0\0\0a\0\0\0\0�\0\0\0\0�\0\0\0\0@�\0\0\0 �Ì�W�����}u�f����rkQ��w�����ő�t��@|t�	��򪏜��W\n�	Ev�H�t�`�\\���SՁ�Π)K�>s�e���w�=r��\0\0\0@�!�0ߖ�mG?9�`ּ���+��\0\0@�I\'	�5�b���Ce���ݷ����?��4\0\0\0aF�9p����+��Tן:�u����z���<���feeŴ��l:X{h��y�.]�ϑ�ϙY���K\n\0\0�0#��m8;�ٱ�*ׄ��x<�$����z�ݵ�:�--����\\ۂ��ٿ`ּ����t�7�c\0\0\0�0�B>)𗽻�njlѢ��#��ۺUv�;�J��F�����O�m8+I���w����x���\n\n�y��C�k����o�ɟ��g/<�܄?����U�kv�ݳpv�ŋ�F�\\�pIǼ|�\0\0\03,��j�t�1?_��}�ĉM;��O�v����r���\r�_�ފwJ^z�e+�o�$I�֭��K����\0\0\nIDATW�b���sTL��R���G��	q��mg���~���_�\\��Mn�����SBo6L� ��(�ʉ�)Z�5E�����\\:}>�/�>z�����邉��Ć�Yd��^A�L�B�wٖ�$r��D��c�\"��e��MqӹN�/z�T�\\�sY9Q�y���й�\"><�5֝��ou� ̈�������e�]�t����s���a\\��eff�ڴ�m��\'O�,���4�u�e��W�N߸u��O|:�t� �X����{���|nù�aË��|��#2�W��\Z-�_\\��.01�Csrl#���^e<�z��*�\Z��[�]�Ջ�_r�4�@��E��ZYo[��u)r�3���T�TT?fH���ܬ\Zc�^��������۵k�﮾3��&O(�Ԙߏv��M�$-\\�$333�U�����N��ֳq��}5�ή�$i��-���vw���	\\y��l[Rm�\n���\rվ�\r�\'���cEI��s���:Р��((���J����`x=:�Ȭ}tΩ#I�i�V����Q��Z�:��+r��\')TYu=O{�h��cHX���z�^�V�}>���}k�3�O��\'M��W�~<pe�ꫯ�ݪUzz������{��U��:i�d��y>>8�tVù�A�\r��j��c��ʢ�b�83�3���X+�:�\"���UQ����t�Ag���v���#	��2��i2x��i_Y�T[\Z���HZ�5)��*��zFg9����3�Ӷ\'��I���`�W�y<�#{<0��?o޸h�|��������>ڿ�[�V����o\r=����3j�[��+eE�5yO){TG�����wAYn%�����}��w�ꋪ��K�VÁ�ZidB	�r��\"X�\r�0���%#���Y{�@u3��Mg��ڝ�P�̎���g��&,�p�������.}k�C��<#=#�|�������\\��Ԓeo쯩�(�}���\\�p��c�6�ȑ0Ή4T[��f�\ZZ8sx��\"U�ԟd�]cUu��&�U=����!���X��iɎ��V�B{jQKN8.M��\\�˭�S���_PU��I�\n�?�#���c�0CݰG�{t���׬��u[����cF�zcy�<�6\\�9�WU8F|ˮ\n�i$<G[A�5VP��QG&�(��Iƚ��Nz�R侐#�Y�}7�X�P�1,Z�I#	��H�ԙӯ���צ�L�c��	�?I�1�����9E\Zn�r\r�Yf[^ӭ��\Z���S��@���\0����_�t1��sf�OJ��vA�_��/������K���~Y\Zc��7�h�o�ߟD���#����;�oOOOpw��\Z��j�\\��H#�P�Z7ɹ��ܑ=*���;v(��	�	�����(�(�Ty�+�U��Չ�;7,\"9얉�;�il哦T���u^�{G�3Z�PRw|96���h�� ��?�A�$O��%%%�FG]f�ښхw��5o���ܹs׮]U?�������}`pL�,��(��H\nn�/��	Ev�H�t�sSm\\\n�)cs��{�W�7�dJyqqq�_�\Z�Ч߮�=����G����k��ٿo!E\0\0\0p�v9������W�\r�-�rZ�\0\0��I\0�Kddd\\��J�\0\0\0H<i\n\0\0\0\0a\0\0\0\0�\0\0\0\0�\0\0\0\0@�\0\0\0�1ZL�4)�ώ;\n�E���ݟ��-�������[&𵮡�	�a�k�W�t�~�i_(�dG�r�n\Z$GJҥ�]��W�ع���0���_�������#N��+�\";H�`�Q0H.R�ɩ��_gД%Μ�W����}��QR\0\0\0��0�|�v�<�������Ի7� \0\0\0H5�$��澾�бó_���;}����+��4\0\0\0aF�9x�vŻk�?>p�_~��X�����>����{_�Y1�j��m���7����K�?3�l���9�0\0\0\03R��s\rc\'�_�a]�_��#I���9t����/��X>���+Թ��+���f���Ʀ���\Z�|�\"b\0\0\0fX(���}���Rs�-dy���+�w�/h����C�=� I�=���cχ+�.)�����;���|����\r��sO�~��qQ׶fú=5�*^.����#����\"�C�\0\0\0��Z��ƛ���?��ĉ�j�ڿG�v���CF;{����׽�`ًe�V�]2��?�$i����=o�b����&W����������~-�1��������~��\'���X�&\'׶}TZ\ZzS������s��hMW��,�7v�&���Ǵ�������)᪙n�R)%\\�\"����\nJf��о˶<� ��U\'Z���(G��e��MqӹN�/z�T�\\��r��<�� {���s�YE|x�9j�;o(���A��_\n�����e�]�t����s�Ǖ��_dfdn_��mn��_���M��:��;}⋛wn����a��o��7��WΟ+\Z��k���cD����UC�9U��ӢLL\'�М�H =�W��^(�ʪ��V\0s�c�\"��I`���4��q��*-��ʕG�����K�Zg�Z�{�mss��˗�^����=�7#F+Z4sNق9�$�)����\'�>Z�rYfFFvV���j222�O�2��Z�ټs��C5/�����}r�k������EA��au���]zCZS����<��4�\'�#\'�fP��\\J��0��+�ׂ�m�D�G��4�Re���9�Y�&YbT�6��̪ET���:�k��N.�j��I\n�)��8�*i�m�yJ6�fx�^�׫����|>;w�Ƿo���-�`p�ɽ{�x�}�ׯ����V�Z��h�e�{�`��?��\\��S~��J�-]x�h��)�4�?7dİ�)�d�j���i��O�� ���s�Dm�P��c�|���\n���.�7�/e�u�[��=�VX�$�t�\nOr��Fi�ʳzBJ�xȍ�!�`��Τ�:�Ȧ����8�!��I�0C��`0��f����]?��ƪ-����}~_�Y�V���Ϻ�ob���=����zaܪ�o�9Vʊj��\Z�j@���5e��Ȍ�zo`b�#eDJ����J�cã�T���U��i�^����Ȅ���	$�U���%#�&0#dY��	�ŉ��lk�N��qfG����3��Bf8SZZ��Eo/{g��G�HO�>�����V�����ZK�����ڃ�_�q���=dIi��c�(\"���=Mڪ-[j3n\r-l�fUd�]cU<2$�O|��I�q���RUOKvLʹ	���Zr�qiJ=�B\\n��&������NN�T��i�q�����>��Ї�����{=o����i���^�,ϮU��;���Q8�ZB*D\Z��&D-��\n���\"�H\\��90��Z3v_�R�`q�M8%��b�����\"i$����>����gNz)Yb�ʧ��I�Z����W���[�a8#T�-���N��?�j���o��X)s�ڭqH�OJ��vA�_��/��re,��4�0�_��<�M��ތ�wx���$J���*���;o��Wzz���T���WcŜfE\ZQ��(ԺI�MgDxA�]���S\"�Ȟ�rR�`|����9ъʍr���K���^ը�Q�(�s��\"�ۓ�r�D\'4��-;]k�)>��(h�<�g� ��8��rl%>f\r�b��g<H�䉼�����x����X[3��ͱՍvo����]�vU�����u���i��k�ĺI�M���<rGB1�rK\n�nv�`��K�8\"elN�q�J�f�L)/..�kSc|��w���`}}���}���2�3\no�.E\0\0\0p�ܖ�}n�U�7����rZf�����\0`\'Ɠ\0����ȸ�m;�\0\0\0H<i\n\0\0\0\0a\0\0\0\0�\0\0\0\0�\0\0\0\0@�\0\0\0�1�<	\0\0\08�K~��	�\0\0\0��4\0\0\0�0\0\0\0\0a\0\0\0\0�\0\0\0\0 �\0\0\0\0��$�u��ɿ�G���Q���)\0\0\0���z��l*�\0\0��1h\n\0\0\0����HO�r��-�I%\0\0\0$#�������o�����X���5ǩjRaդB���H�G��sT���K\'��m�r�u�M<�u��P���KBd^�٤D�\0\0@Y՛���5�\\D��[N��z#��n)N��\Z��ʕ��`ݡg�*�>�8Isaa�UE,L����E.4�W�U5\Z�Й�\0\0�{3��3���P<�>\'�^�����B�R��FN�mC��#�����e-VVǕ����GNT~jV���Ց���s�崿>�g�E�rf�^D�kJ�N(��<WDNY(I�t�-���RX�a��\0\0@�Y{oF(��=B�XE6]uq�7��u~�8�Dߟ�����Ҥ�*囪*�k��@T�[������7K��#y�Μ����c�f�7�]����>�΄㊚�����T$I51�ٶS\0\0\0	g듦�u�pH`VOB���=ô&����eU�͈���#�jnJ����w?�\\ߞ7�j����G�V�5�mZ�Y�.�����$IZ�0�\0\0\0a�{Ҕj��`~�n�*̈�����z{�s|����o&HՕ�\Z�k�~ho�6����;�k|gz�_�P�z�cĳ��\0\0�v�>�~�Ì��ph�]�~z��pf�����+�{ϱ*1e#��3��ݱ0����c�����\"���b�\r��S\0\0�$b|�T�=�{0�zB��<z>5w~����_��lbᤪ�Cv�����ۢF�P?���}q���%Zv�zL�m�������\nE\Z�FD�7�n\r\0\0�n��f�@·���UNW�D�S����m�:��,��0Л!���+���e76���挧�<�~�\rl�r���d7L�g����G\Z�}\Z��!�q�O��)\0\0������?%%%���i��X[3�^�S\"�4�%|���_4��N��H�����o_�~�~��fi�k�TX(���C9ř�	\0\0�9J���Cj餈͢�f�.ĕ{)���_��\\;�X���ޏ�(��ȈB9\0\0\01!̰=̈���8��Չs�=�\Z�zE��>2u�&s��9�F�)\0\0\0 �pp��7Á���\r\0\0\0�G��f\0\0\0\0��e;w�\0\0�0q����\rf�\0\0\03`��{��+���C`\0\0�F\0\0\0\0 �\0\0\0\0@�\0\0\0�0\0\0\0\03\0\0\0\0f\0\0\0\0 �\0\0\0\0\0=��8B�~A�_�\0\0\0 ̀������f��\0\0 ��O�U��}gL_�r��/5��m�a�G�,IҖ��(�\0\0\0 ̐,\r-�W�ͭ��,c\0\0\0\0I���{ү\n�ENQ���ͩ:���*7C�u�-T�\'�>�c�Չ��Ԏg�NZiB�\0\0�T��{3b�pϠ�����~��[TQ]��>*�cD~���;f������\n�A��t\0\0\08�ݽ����e��\'6ס�q��-ٖD��w7��t���	�[dt�`[U�Wk��wH�>Z�_�t��X�ʎx�M\0\0�H.�7#�;�3��X��ٌĆ����w��6:k��:��S���Kgʄ�]�:�,nE�\0\0@\"ÌV9�Y���=��.+_����xO���I�t�K�q�����pv\0\0\0$}����5\n�@�M����h�;�r�����o68�>\r�#\re���o�زqFqD\0\0\0If�V��vϩ�Vu3t�Pu{�4�4�T���#�!��D���e�eC���\"\0\0���1����5�G<Z]�]�:�:%��ʊ\'M�?��SQ�!��A�������g�����s_\0\0�0�%��1н�W���Tc��>.Iw�[8$��7�@F�����s\0\0 �H�HC<1jLL}J�sdϱ\r��y��>.Iw���P���3��\0\0 ̀���Eъ\"�I��d\0\0\0a���\r��g7\0\0f8ڌ�5zf�P/�\n\0\0\0��F\0\0\0\00�p�k��\0\0\0��VJ��b�\0\0��0h\n\0\0\0\0a\0\0\0\0g��*�RN�\0\0\0\0���>\0\0\0`.M\0\0\0 �\0\0\0\0@�\0\0\0�0\0\0\0\03\0\0\0\0f\0\0\0\0 �\0\0\0\0\0�\0\0\0\0�\0\0\0\03\0\0\0\0�0\0\0\0\0a\0\0\0\0�\0\0\0\0�\0\0\0\0@�\0\0\0�0\0\0\0\0a\0\0\0\0�^]]M*\0\0\0\003̸��+�\0\0\0\0�D�\0\0\0@�\0\0\0�0\0\0\0\0a\0\0\0\0f\0\0\0\0 �\0\0\0\0@�\0\0\0\0�\0\0\0\03\0\0\0\0f\0\0\0\0\0a\0\0\0\0�\0\0\0\0�\0\0���o�Kȷ��nYS\0f\0\04}z�t�/�*ܑ|]~�OO�6E\0$�t�\0\0.\\5U�#�����W�u~�,�P]s��Ђ���3k}*�r�\0 �\0\0�TW�/KdSdk�髕\"�}��#�Q��H\03\0\0���Z3Dֳ�D�a��mS~�Ψ)�-|�sO\0�\0\0��Z�#Gm�vŨά\Z-茚��\0\0��p\0H�@\"r|�V߂u�\Z\"c����jXB�\0\0G�7\0� ���s~��GZ)W��DN�#�)<r���G��-���tk\0��x85\0b���\0����\0\0�`Ht\0\0t�7\0\0\0�ɸ\0\0\0��4\0�<�I�\0(��o-�K[�����QOQש܅�?�kR$c�\03\0 ��Q�\Z��_����xkP}^�,&Q�6�ڶ�9��\0a\0@W]Y��_��[�\\g�]�A���q�lJ8�Qάɴ֠���E�B/�5\0\03\0�Y!���{��Y=�a��lJ����FY��$�tS�ȶ^ J5\0f\0���1b��++Ă���QS���U���(o&Q.(�\r�;B,\0�\0�B��ZS�E�l������D��\0��!\0�h\0�&�Qk��6�z	���+��й�xA�c�\0\0��7\0L���}BR��\\PϔX�9O��V޶������ԙn���lG�k\0\0ă_��Dw��YR\0\03\0\0f֤S���|�-\0�0\0\0\0\0��-�\0\0\0\03\0\0\0\0f\0\0\0\0 �\0\0\0\0\0�\0\0\0\0�\0\0\0\03\0\0\0\0�0\0\0\0\0a\0\0\0\0�\0\0\0\0���T���t\0\0\0\0IEND�B`�','image/png','parameter.png',NULL,'admin','2017-02-06 15:27:59');
/*!40000 ALTER TABLE `tab_ablage` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `tab_instProt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_instProt` (
  `inprID` mediumint(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `gerID` smallint(6) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `protID` smallint(6) unsigned zerofill NOT NULL,
  `schritt` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `bereich` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sequenz` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `kommentar` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`inprID`),
  UNIQUE KEY `gerID_protID_schritt_seq` (`gerID`,`protID`,`schritt`,`sequenz`),
  KEY `gerID` (`gerID`),
  KEY `protID` (`protID`),
  KEY `sequenz` (`sequenz`),
  CONSTRAINT `instProt_geraete` FOREIGN KEY (`gerID`) REFERENCES `mpi_geraete` (`tabID`) ON DELETE CASCADE,
  CONSTRAINT `instProt_protID` FOREIGN KEY (`protID`) REFERENCES `list_protokoll` (`protID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Protokoll Installation';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tab_instProt` WRITE;
/*!40000 ALTER TABLE `tab_instProt` DISABLE KEYS */;
INSERT INTO `tab_instProt` VALUES (00000001,1,0,000001,'A10','Prepare','Inventarisieren','Test','admin','2017-02-07 07:40:35');
/*!40000 ALTER TABLE `tab_instProt` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `tab_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_parameter` (
  `parID` mediumint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `auswTab` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tabID` smallint(6) unsigned NOT NULL,
  `typID` smallint(6) unsigned zerofill NOT NULL,
  `parameter` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `einheit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notiz` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`parID`),
  UNIQUE KEY `ausw_tab_typ_par_einh_note` (`auswTab`,`tabID`,`typID`,`parameter`,`einheit`,`notiz`),
  KEY `typID` (`typID`),
  KEY `einheit` (`einheit`),
  CONSTRAINT `param_einheit` FOREIGN KEY (`einheit`) REFERENCES `list_einheit` (`einheit`) ON UPDATE CASCADE,
  CONSTRAINT `param_typ` FOREIGN KEY (`typID`) REFERENCES `list_parTyp` (`typID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `tab_parameter` WRITE;
/*!40000 ALTER TABLE `tab_parameter` DISABLE KEYS */;
INSERT INTO `tab_parameter` VALUES (000001,'medID',8,000031,'1000','GB',NULL,'admin','2017-02-06 15:20:20');
INSERT INTO `tab_parameter` VALUES (000002,'gerID',1,000028,'192.168.10.1','IP-V4',NULL,'admin','2017-02-06 15:23:12');
INSERT INTO `tab_parameter` VALUES (000003,'medID',1,000028,'192.168.20.155','IP-V4',NULL,'admin','2017-02-07 07:25:35');
/*!40000 ALTER TABLE `tab_parameter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `view_favorit`;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_favorit` (
  `autoID` tinyint NOT NULL,
  `reiter` tinyint NOT NULL,
  `kategorie` tinyint NOT NULL,
  `favorit` tinyint NOT NULL,
  `history` tinyint NOT NULL,
  `bedeutung` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_inventar`;
/*!50001 DROP VIEW IF EXISTS `view_inventar`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_inventar` (
  `anlage` tinyint NOT NULL,
  `unterNr` tinyint NOT NULL,
  `invNummer` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL,
  `standort` tinyint NOT NULL,
  `wert` tinyint NOT NULL,
  `invDatum` tinyint NOT NULL,
  `kostenstelle` tinyint NOT NULL,
  `notiz` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL,
  `inventar` tinyint NOT NULL,
  `sysID` tinyint NOT NULL,
  `sysOrt` tinyint NOT NULL,
  `gerID` tinyint NOT NULL,
  `gerOrt` tinyint NOT NULL,
  `exist` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_it_licman_instInv`;
/*!50001 DROP VIEW IF EXISTS `view_it_licman_instInv`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_it_licman_instInv` (
  `tabID` tinyint NOT NULL,
  `instID` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_licman_lizenz`;
/*!50001 DROP VIEW IF EXISTS `view_licman_lizenz`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_licman_lizenz` (
  `lizenzID` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL,
  `bestellnummer` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_mainTab`;
/*!50001 DROP VIEW IF EXISTS `view_mainTab`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_mainTab` (
  `auswTab` tinyint NOT NULL,
  `tabID` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `ident` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_reiter`;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_reiter` (
  `reiter` tinyint NOT NULL,
  `table_type` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_user`;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_user` (
  `userID` tinyint NOT NULL,
  `login` tinyint NOT NULL,
  `sort` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

USE `mpidb_mpg_inv`;
/*!50001 DROP TABLE IF EXISTS `view_favorit`*/;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_favorit` AS select `list_reiter`.`autoID` AS `autoID`,`list_reiter`.`reiter` AS `reiter`,`list_reiter`.`kategorie` AS `kategorie`,`list_reiter`.`favorit` AS `favorit`,`list_reiter`.`history` AS `history`,`list_reiter`.`bedeutung` AS `bedeutung` from `list_reiter` where (`list_reiter`.`favorit` = '1') order by `list_reiter`.`reiter` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_inventar`*/;
/*!50001 DROP VIEW IF EXISTS `view_inventar`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_inventar` AS select `inv`.`anlage` AS `anlage`,`inv`.`unterNr` AS `unterNr`,`inv`.`invNummer` AS `invNummer`,`inv`.`bezeichnung` AS `bezeichnung`,`inv`.`standort` AS `standort`,`inv`.`wert` AS `wert`,`inv`.`invDatum` AS `invDatum`,`inv`.`kostenstelle` AS `kostenstelle`,`inv`.`notiz` AS `notiz`,`inv`.`bearbeiter` AS `bearbeiter`,`inv`.`zeitstempel` AS `zeitstempel`,concat(`inv`.`anlage`,':',`inv`.`unterNr`) AS `inventar`,`sys`.`tabID` AS `sysID`,`sys`.`lagerort` AS `sysOrt`,`ger`.`tabID` AS `gerID`,`ger`.`lagerort` AS `gerOrt`,if((isnull(`ger`.`tabID`) and isnull(`sys`.`tabID`)),0,1) AS `exist` from ((`list_inventar` `inv` left join `mpi_arbeitsplatz` `sys` on(((`inv`.`anlage` = `sys`.`inventar`) and (`inv`.`unterNr` = `sys`.`unterNr`)))) left join `mpi_geraete` `ger` on(((`inv`.`anlage` = `ger`.`inventar`) and (`inv`.`unterNr` = `ger`.`unterNr`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_it_licman_instInv`*/;
/*!50001 DROP VIEW IF EXISTS `view_it_licman_instInv`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_it_licman_instInv` AS select 1 AS `tabID`,1 AS `instID` from `mpi_geraete` where 0 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_licman_lizenz`*/;
/*!50001 DROP VIEW IF EXISTS `view_licman_lizenz`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_licman_lizenz` AS select '000001' AS `lizenzID`,'software : modell : anzahl' AS `bezeichnung`,'0123456789' AS `bestellnummer`,'alone' AS `bearbeiter`,'2016-07-25 12:46:00' AS `zeitstempel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_mainTab`*/;
/*!50001 DROP VIEW IF EXISTS `view_mainTab`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_mainTab` AS select (convert('sysID' using utf8) collate utf8_unicode_ci) AS `auswTab`,`mpi_arbeitsplatz`.`tabID` AS `tabID`,`mpi_arbeitsplatz`.`name` AS `name`,`mpi_arbeitsplatz`.`seriennummer` AS `ident` from `mpi_arbeitsplatz` union all select (convert('gerID' using utf8) collate utf8_unicode_ci) AS `auswTab`,`mpi_geraete`.`tabID` AS `tabID`,`mpi_geraete`.`name` AS `name`,`mpi_geraete`.`seriennummer` AS `ident` from `mpi_geraete` union all select (convert('matID' using utf8) collate utf8_unicode_ci) AS `auswTab`,`mpi_material`.`tabID` AS `tabID`,`mpi_material`.`name` AS `name`,`mpi_material`.`eigenschaft` AS `ident` from `mpi_material` union all select (convert('medID' using utf8) collate utf8_unicode_ci) AS `auswTab`,`mpi_medien`.`medID` AS `tabID`,`mpi_medien`.`name` AS `name`,`mpi_medien`.`seriennummer` AS `ident` from `mpi_medien` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_reiter`*/;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reiter` AS select (convert(`information_schema`.`tables`.`TABLE_NAME` using utf8) collate utf8_unicode_ci) AS `reiter`,`information_schema`.`tables`.`TABLE_TYPE` AS `table_type` from `information_schema`.`tables` where ((`information_schema`.`tables`.`TABLE_SCHEMA` = (select database())) and ((`information_schema`.`tables`.`TABLE_TYPE` = 'base table') or (`information_schema`.`tables`.`TABLE_TYPE` = 'view'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_user`*/;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_user` AS select '000001' AS `userID`,'mpg_local' AS `login`,'MPG, Version (mpg_local)' AS `sort` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

