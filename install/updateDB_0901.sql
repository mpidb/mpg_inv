-- !!! ONLY USE FOR MARIADB !!!
-- run: mysql -u root -p mpidb_mpg_inv < updateDB_<version>.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

USE mpidb_mpg_inv;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- initial value
IF ( SELECT MAX(version) FROM dataface__version ) = '0' THEN
 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0901');
END IF;

-- mindest version vorhanden
IF ( SELECT MAX(version) FROM dataface__version ) < '0901' THEN
 LEAVE proc_label;
END IF;

-- CHANGES V0.9.01 :
-- *****************
-- fs::rsync - it_inv

-- fs::rsync - mpg_inv
-- fs::view_mpg_licman_instInv - run view view_mpg_licman_instInv.sql

-- CHANGES V0.9.02:
-- ****************
-- db::dataface__index - build search index
-- fs::conf.ini        - build search index

-- CHANGES V0.9.03:
-- ****************
-- parameter1-9 raus
-- fs::rsync - mpg_inv

IF ( SELECT MAX(version) FROM dataface__version ) < '0903' THEN

 -- del parameter
 ALTER TABLE mpi_geraete  DROP parameter3, DROP parameter4, DROP parameter5, DROP parameter6, DROP parameter7, DROP parameter8, DROP parameter9;
 ALTER TABLE mpi_geraete__history  DROP parameter3, DROP parameter4, DROP parameter5, DROP parameter6, DROP parameter7, DROP parameter8, DROP parameter9;

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0903');
END IF;

-- CHANGES V0.9.10 :
-- ****************
-- fs::rsync - mpg_inv
-- flex parameter rein

IF ( SELECT MAX(version) FROM dataface__version ) < '0910' THEN

-- add entry table list_einheit
 INSERT IGNORE INTO list_einheit VALUES
 (4, 'GB'),
 (6, 'GHz'),
 (2, 'KB'),
 (3, 'MB'),
 (5, 'MHz'),
 (8, 'Pixel'),
 (1, 'TB'),
 (7, 'Zoll');

 -- create table list_parTyp
 CREATE TABLE IF NOT EXISTS list_parTyp (
  typID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  typ varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (typID),
  UNIQUE KEY typ (typ)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

INSERT IGNORE INTO list_parTyp VALUES
(000012, 'Aufloesung'),
(000018, 'CPU Kerne'),
(000017, 'CPU Taktfrequenz'),
(000016, 'CPU Typ'),
(000011, 'Diagonale'),
(000010, 'Festplatte 1'),
(000013, 'Festplatte 2'),
(000014, 'Festplatte 3'),
(000015, 'Festplatte 4'),
(000025, 'Interface 1'),
(000026, 'Interface 2'),
(000027, 'Interface 3'),
(000028, 'lastFound'),
(000029, 'lastFoundM'),
(000021, 'Mac Adresse 1'),
(000022, 'Mac Adresse 2'),
(000023, 'Mac Adresse 3'),
(000024, 'Mac Adresse 4'),
(000001, 'Parameter 1'),
(000002, 'Parameter 2'),
(000003, 'Parameter 3'),
(000004, 'Parameter 4'),
(000005, 'Parameter 5'),
(000006, 'Parameter 6'),
(000007, 'Parameter 7'),
(000008, 'Parameter 8'),
(000009, 'Parameter 9'),
(000020, 'RAM Speicher'),
(000019, 'RAM Typ');

-- create table mpi_parameter
 CREATE TABLE IF NOT EXISTS mpi_parameter (
  parID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  gerID smallint(6) NOT NULL,
  typID smallint(6) unsigned zerofill NOT NULL,
  parameter varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  einheit varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  notiz varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (parID),
  KEY gerID (gerID),
  KEY typID (typID),
  KEY einheit (einheit)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

 ALTER TABLE mpi_parameter
  ADD CONSTRAINT parameter_einheit FOREIGN KEY (einheit) REFERENCES list_einheit (einheit) ON UPDATE CASCADE,
  ADD CONSTRAINT parameter_gerID FOREIGN KEY (gerID) REFERENCES mpi_geraete (tabID) ON DELETE CASCADE,
  ADD CONSTRAINT parameter_typID FOREIGN KEY (typID) REFERENCES list_parTyp (typID);

 -- add table entry list_reiter
 INSERT INTO list_reiter (autoID, reiter, kategorie, favorit, bedeutung) VALUES(000251, 'mpi_parameter', 'Liste', 1, 'Parameterliste mit Typangabe');
 INSERT INTO list_reiter (autoID, reiter, kategorie, favorit, bedeutung) VALUES(000252, 'list_parTyp', 'Liste', 0, 'Benennung Parametertyp');

 -- run view wegen aenderung mpi_geraete
 CREATE OR REPLACE VIEW view_geraete AS SELECT * FROM  mpi_geraete;

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0910');
END IF;


-- CHANGES V0.9.50 :
-- ****************
-- fs::rsync - mpg_inv
-- versionsvergleich db - filesystem

IF ( SELECT MAX(version) FROM dataface__version ) < '0950' THEN

 -- conf.ini und mpi_geraete.php aendern

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0950');
END IF;


-- CHANGES V0.9.51 :
-- ****************
-- fs::rsync - mpg_inv
-- db::mpi_verleih      - umbau status ausleihe

IF ( SELECT MAX(version) FROM dataface__version ) < '0951' THEN

 -- mpi_verleih - add feld status
 ALTER TABLE mpi_verleih ADD status TINYINT(1) NOT NULL DEFAULT '0' AFTER gerID;
 ALTER TABLE mpi_geraete ADD verleih TINYINT(1) NOT NULL DEFAULT '0' AFTER typ;
 ALTER TABLE mpi_verleih CHANGE autoID leihID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0951');
END IF;


-- CHANGES V0.9.60 :
-- ****************
-- fs::rsync           - cronjob, mpi_parameter, list_reiter, list_status, mpi_medien
-- db::mpi_parameter   - erweitern fuer speichermedien
-- db::list_reiter     - erweiterung history funktion und neue tabs
-- db::list_status     - erweitern fuer speichermedien
-- db::mpi_medien      - lifecylcel speichermedien
-- db::mpi_ablage      - gemeinsame Dateiablage

IF ( SELECT MAX(version) FROM dataface__version ) < '0960' THEN

 -- mpi_parameter - add field for medien
 ALTER TABLE mpi_parameter ADD medID SMALLINT(6) UNSIGNED ZEROFILL NULL AFTER gerID , ADD INDEX (medID);
 ALTER TABLE mpi_parameter CHANGE gerID gerID SMALLINT(6) NULL ;

 -- history erweiterung list_reiter und zeige neue Tabellen in list_reiter
 ALTER TABLE list_reiter CHANGE autoID tabID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
 ALTER TABLE list_reiter ADD history TINYINT(1) NOT NULL DEFAULT '0' AFTER favorit;
 DROP TABLE IF EXISTS list_reiter__history;
 INSERT INTO list_reiter (tabID, reiter, kategorie, favorit, history, bedeutung) VALUES (212, 'mpi_medien', 'Haupttabelle', '1', '1', 'Lifecycle Speichermedien');
 INSERT INTO list_reiter (tabID, reiter, kategorie, favorit, history, bedeutung) VALUES (172, 'mpi_ablage', 'Ablage', '1', '0', 'Dateiablagen fuer Geraete, Medien usw.'
);
 UPDATE list_reiter SET history = '1' WHERE kategorie = 'Haupttabelle' OR kategorie = 'Authorisierung';
 DELETE list_reiter FROM list_reiter WHERE reiter LIKE 'mpi_%Files';

 -- list_status erweitern
 ALTER TABLE  list_status CHANGE autoID statID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
 ALTER TABLE  list_status ADD tabID SMALLINT(6) UNSIGNED ZEROFILL NULL, ADD INDEX (tabID);
 ALTER TABLE  list_status ADD CONSTRAINT  list_reiter_tabID FOREIGN KEY (tabID) REFERENCES  list_reiter (tabID) ON DELETE SET NULL ON UPDATE RESTRICT;
 INSERT IGNORE INTO list_status (`statID`, `status`, `tabID`) VALUES
  ('', 'verschrottet', 000212),
  ('', 'in Benutzung', 000212),
  ('', 'Schrottlager', 000212),
  ('', 'auf Lager', 000212);
 UPDATE list_status SET tabID = '000212' WHERE status = 'verschrottet';

 -- mpi_medien - create new table
CREATE TABLE IF NOT EXISTS `mpi_medien` (
  `medID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gerID` smallint(6) DEFAULT NULL,
  `typ` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `seriennummer` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ablageort` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `benutzer` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statID` smallint(6) unsigned zerofill NOT NULL,
  `ablageID` smallint(6) unsigned zerofill DEFAULT NULL,
  `beschreibung` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieferant` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `matNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `artNr` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bestelldatum` date DEFAULT NULL,
  `image` longblob,
  `image_mimetype` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_filename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`medID`),
  UNIQUE KEY `name` (`name`),
  KEY `lieferant` (`lieferant`),
  KEY `ablageort` (`ablageort`),
  KEY `statID` (`statID`),
  KEY `gerID` (`gerID`),
  KEY `typID` (`typ`),
  KEY `ablageID` (`ablageID`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
 ALTER TABLE mpi_medien
  ADD CONSTRAINT mpi_medien_gerID  FOREIGN KEY (gerID)  REFERENCES mpi_geraete (tabID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_medien_statID FOREIGN KEY (statID) REFERENCES list_status (statID);

 -- mpi_ablage - create new table
 CREATE TABLE mpi_ablage (
  ablageID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  sysID smallint(6) DEFAULT NULL,
  gerID smallint(6) DEFAULT NULL,
  matID smallint(6) DEFAULT NULL,
  medID smallint(6) unsigned zerofill DEFAULT NULL,
  vorgang varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  file longblob,
  file_mimetype varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  file_filename varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  notiz varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (ablageID), KEY vorgang (vorgang), KEY sysID (sysID), KEY gerID (gerID), KEY matID (matID), KEY medID (medID)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 ALTER TABLE mpi_ablage
  ADD CONSTRAINT mpi_ablage_sysID FOREIGN KEY (sysID) REFERENCES mpi_arbeitsplatz (tabID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_ablage_gerID FOREIGN KEY (gerID) REFERENCES mpi_geraete (tabID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_ablage_matID FOREIGN KEY (matID) REFERENCES mpi_material (tabID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_ablage_medID FOREIGN KEY (medID) REFERENCES mpi_medien (medID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_ablage_vorgang FOREIGN KEY (vorgang) REFERENCES list_vorgang (vorgang) ON DELETE RESTRICT ON UPDATE CASCADE;

 -- move files von mpi_*Files nach mpi_ablage
 INSERT INTO list_vorgang (autoID ,vorgang) VALUES (NULL, 'UpdateDB');
  -- mpi_anlFiles
 INSERT INTO mpi_ablage
  (sysID, vorgang, `file`, file_mimetype, file_filename, notiz, bearbeiter, zeitstempel)
  SELECT tabID, 'UpdateDB', `file`, file_mimetype, file_filename, bemerkung, bearbeiter, zeitstempel FROM mpi_anlFiles WHERE tabID IS NOT NULL;
  DROP TABLE IF EXISTS mpi_anlFiles;
  DROP TABLE IF EXISTS mpi_anlFiles__history;
  -- mpi_gerFiles
 INSERT INTO mpi_ablage
  (gerID, vorgang, `file`, file_mimetype, file_filename, notiz, bearbeiter, zeitstempel)
  SELECT tabID, 'UpdateDB', `file`, file_mimetype, file_filename, bemerkung, bearbeiter, zeitstempel FROM mpi_gerFiles WHERE tabID IS NOT NULL;
  DROP TABLE IF EXISTS mpi_gerFiles;
  DROP TABLE IF EXISTS mpi_gerFiles__history;
  -- mpi_matFiles
 INSERT INTO mpi_ablage
  (matID, vorgang, `file`, file_mimetype, file_filename, notiz, bearbeiter, zeitstempel)
  SELECT tabID, 'UpdateDB', `file`, file_mimetype, file_filename, bemerkung, bearbeiter, zeitstempel FROM mpi_matFiles WHERE tabID IS NOT NULL;
 DROP TABLE IF EXISTS mpi_matFiles;
 DROP TABLE IF EXISTS mpi_matFiles__history;


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0960');

END IF;


-- CHANGES V0.9.61 :
-- ****************
-- fs::rsync           - mpi_notiz
-- db::mpi_notiz       - gemeinsame notiz tabelle
-- db::list_reiter     - del old notes tab

IF ( SELECT MAX(version) FROM dataface__version ) < '0961' THEN

 -- mpi_notiz - create new table
 CREATE TABLE IF NOT EXISTS mpi_notiz (
  noteID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  sysID smallint(6) DEFAULT NULL,
  gerID smallint(6) DEFAULT NULL,
  matID smallint(6) DEFAULT NULL,
  stichwort varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  notiz text COLLATE utf8_unicode_ci NOT NULL,
  bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  zeitstempel timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (noteID), KEY sysID (sysID), KEY gerID (gerID), KEY matID (matID)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
 ALTER TABLE mpi_notiz
  ADD CONSTRAINT mpi_notiz_sysID FOREIGN KEY (sysID) REFERENCES mpi_arbeitsplatz (tabID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_notiz_gerID FOREIGN KEY (gerID) REFERENCES mpi_geraete (tabID) ON DELETE SET NULL,
  ADD CONSTRAINT mpi_notiz_matID FOREIGN KEY (matID) REFERENCES mpi_material (tabID) ON DELETE SET NULL;

 -- move files von mpi_*Notiz nach mpi_ablage
  -- mpi_anlNotiz
 INSERT INTO mpi_notiz
  (sysID, stichwort, notiz, bearbeiter, zeitstempel)
  SELECT tabID, 'UpdateDB', notiz, bearbeiter, zeitstempel FROM mpi_anlNotiz WHERE tabID IS NOT NULL;
  DROP TABLE IF EXISTS mpi_anlNotiz;
  DROP TABLE IF EXISTS mpi_anlNotiz__history;
  -- mpi_gerNotiz
 INSERT INTO mpi_notiz
  (gerID, stichwort, notiz, bearbeiter, zeitstempel)
  SELECT tabID, 'UpdateDB', notiz, bearbeiter, zeitstempel FROM mpi_gerNotiz WHERE tabID IS NOT NULL;
  DROP TABLE IF EXISTS mpi_gerNotiz;
  DROP TABLE IF EXISTS mpi_gerNotiz__history;
  -- mpi_matNotiz
 INSERT INTO mpi_notiz
  (matID, stichwort, notiz, bearbeiter, zeitstempel)
  SELECT tabID, 'UpdateDB', notiz, bearbeiter, zeitstempel FROM mpi_matNotiz WHERE tabID IS NOT NULL;
  DROP TABLE IF EXISTS mpi_matNotiz;
  DROP TABLE IF EXISTS mpi_matNotiz__history;

 -- add mpi_notiz und del old notes tab
 INSERT INTO list_reiter (tabID, reiter, kategorie, favorit, history, bedeutung) VALUES (213, 'mpi_notiz', 'Ablage', '1', '1', 'Ablage groesserer Notizen');
 DELETE list_reiter FROM list_reiter WHERE reiter LIKE 'mpi_%Notiz';



 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0961');

END IF;


-- CHANGES V0.9.64 :
-- ****************
-- fs::rsync           - color, stylesheet, focus
-- db::view_favorit    - neue version
-- db::view_reiter     - neue version

IF ( SELECT MAX(version) FROM dataface__version ) < '0964' THEN


 -- view nochmal neu erzeugen
 CREATE OR REPLACE VIEW view_favorit AS
 SELECT * FROM list_reiter WHERE favorit = '1' ORDER BY reiter;

 -- view nochmal neu erzeugen
 CREATE OR REPLACE VIEW view_reiter AS
 SELECT
  CONVERT(table_name USING utf8) COLLATE utf8_unicode_ci AS reiter,
  table_type
 FROM
  information_schema.tables
 WHERE
    table_schema = (SELECT DATABASE() FROM DUAL) AND
  ( table_type = 'base table' OR table_type = 'view' )
 ;


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0964');

END IF;


-- CHANGES V0.9.65 - 2016-04-05
-- ****************************
 -- fs::rsync           - inventar
 -- db::list_inventar   - neue Tabelle mit import SAP Inventar
 -- db::view_inventar   - neue Tabelle Abgleichsicht mit existierenden Inventarnummern in Geraetetabelle
 -- db::list_reiter     - add new tables

IF ( SELECT MAX(version) FROM dataface__version ) < '0965' THEN

 -- add table list_inventar
 CREATE TABLE IF NOT EXISTS list_inventar (
   anlage varchar(8) COLLATE utf8_unicode_ci NOT NULL,
   unterNr varchar(4) COLLATE utf8_unicode_ci NOT NULL,
   invNummer varchar(30) COLLATE utf8_unicode_ci NOT NULL,
   bezeichnung varchar(50) COLLATE utf8_unicode_ci NOT NULL,
   standort varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
   wert decimal(7,2) DEFAULT NULL,
   invDatum date DEFAULT NULL,
   kostenstelle decimal(5,0) DEFAULT NULL,
   notiz varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (anlage,`unterNr`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

 -- add new tables
 INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('list_inventar', 'Liste', 0, 0, 'Import SAP Inventar');
 INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_favorit', 'Programmierung', 0, 0, 'Für den schnelleren Zugriff unter dem Menüpunkt ...');
 INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_reiter', 'Programmierung', 0, 0, 'Hole alle Tabellen von Datenbank von mysql');
 INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_inventar', 'Auswertung', 1, 0, 'Zeige alle Inventarnummern und deren Verlinkung zur Geraetetabelle');

 -- kuerze inventar und add unterNr
 ALTER TABLE mpi_geraete CHANGE inventar inventar VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
 ALTER TABLE mpi_geraete ADD unterNr VARCHAR(4) NULL DEFAULT '0' AFTER inventar;
 -- splitte alte inventarnummer
 UPDATE mpi_geraete SET unterNr = SUBSTRING_INDEX(inventar,'-',-1), inventar = SUBSTRING_INDEX(inventar,'-',1), zeitstempel = zeitstempel WHERE inventar LIKE '%-%';
 UPDATE mpi_geraete SET unterNr = SUBSTRING_INDEX(inventar,' ',-1), inventar = SUBSTRING_INDEX(inventar,' ',1), zeitstempel = zeitstempel WHERE inventar LIKE '% %';
 -- fake eintrag wenn kein import existiert
 INSERT IGNORE INTO list_inventar (anlage,unterNr,invNummer,bezeichnung,notiz,bearbeiter) SELECT inventar,unterNr,CONCAT(inventar,':',unterNr),name,'Fake Eintrag bis SAP-Import','fake_import' FROM mpi_geraete WHERE inventar IS NOT NULL AND inventar != '';


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0965');
END IF;


-- CHANGES V0.9.66 - 2016-04-12
-- ****************************
 -- fs::rsync            - inventar
 -- db::mpi_arbeitsplatz - add field unterNr
 -- db::view_inventar    - neue Tabelle Abgleichsicht mit existierenden Inventarnummern in Geraetetabelle

IF ( SELECT MAX(version) FROM dataface__version ) < '0966' THEN


 -- kuerze inventar
 ALTER TABLE mpi_arbeitsplatz CHANGE inventar inventar VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
 ALTER TABLE mpi_arbeitsplatz ADD unterNr VARCHAR(4) NULL DEFAULT '0' AFTER inventar;

 -- view auf list_inventar, beschleinigt tabellenaufbau
 CREATE OR REPLACE VIEW view_inventar AS
 SELECT
  inv .*,
  CONCAT(inv.anlage,':',inv.unterNr) AS inventar,
  sys.tabID AS sysID,
  sys.lagerort AS sysOrt,
  ger.tabID AS gerID,
  ger.lagerort AS gerOrt,
  IF((ger.tabID IS NULL) AND (sys.tabID IS NULL), 0, 1) AS exist
 FROM
  list_inventar AS inv
  LEFT JOIN mpi_arbeitsplatz AS sys ON inv.anlage = sys.inventar AND inv.unterNr = sys.unterNr
  LEFT JOIN mpi_geraete AS ger ON inv.anlage = ger.inventar AND inv.unterNr = ger.unterNr
 ;

 -- splitte alte inventarnummer
 UPDATE mpi_arbeitsplatz SET unterNr = SUBSTRING_INDEX(inventar,'-',-1), inventar = SUBSTRING_INDEX(inventar,'-',1), zeitstempel = zeitstempel WHERE inventar LIKE '%-%';
 UPDATE mpi_arbeitsplatz SET unterNr = SUBSTRING_INDEX(inventar,' ',-1), inventar = SUBSTRING_INDEX(inventar,' ',1), zeitstempel = zeitstempel WHERE inventar LIKE '% %';
 -- fake eintrag wenn kein import existiert
 INSERT IGNORE INTO list_inventar (anlage,unterNr,invNummer,bezeichnung,notiz,bearbeiter) SELECT inventar,unterNr,CONCAT(inventar,':',unterNr),name,'Fake Eintrag bis SAP-Import','fake_import' FROM mpi_arbeitsplatz WHERE inventar IS NOT NULL AND inventar != '';


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0966');
END IF;


-- CHANGES V0.9.67 - 2016-04-15
-- ****************************
 -- fs::rsync            - inventar
 -- db::mpi_arbeitsplatz - add field unterNr

IF ( SELECT MAX(version) FROM dataface__version ) < '0967' THEN

  -- default 0 per php
  ALTER TABLE mpi_arbeitsplatz CHANGE unterNr unterNr VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
  ALTER TABLE mpi_geraete CHANGE unterNr unterNr VARCHAR(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
 
  -- inventar aufraeumen
  UPDATE mpi_arbeitsplatz SET unterNr = NULL, zeitstempel = zeitstempel WHERE inventar IS NULL ;
  UPDATE mpi_arbeitsplatz SET inventar = NULL, unterNr = NULL, zeitstempel = zeitstempel WHERE inventar = '' ;
  UPDATE mpi_arbeitsplatz SET unterNr = '0', zeitstempel = zeitstempel WHERE inventar IS NOT NULL AND unterNr IS NULL ;
  UPDATE mpi_geraete SET unterNr = NULL, zeitstempel = zeitstempel WHERE inventar IS NULL ;
  UPDATE mpi_geraete SET inventar = NULL, unterNr = NULL, zeitstempel = zeitstempel WHERE inventar = '' ;
  UPDATE mpi_geraete SET unterNr = '0', zeitstempel = zeitstempel WHERE inventar IS NOT NULL AND unterNr IS NULL ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0967');
END IF;


-- CHANGES V0.9.68 - 2016-04-27
-- ****************************
 -- fs::mpi_geraete     - add garantie
 -- db::mpi_geraete     - add garantie

IF ( SELECT MAX(version) FROM dataface__version ) < '0968' THEN

  -- add garantie
  ALTER TABLE mpi_geraete ADD garantie DATE NULL AFTER lieferdatum ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0968');
END IF;


-- CHANGES V0.9.69 - 2016-06-03
-- ****************************
 -- fs::conf.ini        - change comment # to ;
 -- fs::cronjob         - update to mysqli driver

IF ( SELECT MAX(version) FROM dataface__version ) < '0969' THEN

  -- refresh view
  CREATE OR REPLACE VIEW view_geraete AS SELECT * FROM mpi_geraete ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0969');
END IF;


-- CHANGES V0.9.70 - 2016-06-13
-- ****************************
 -- fs::conf.ini        - change comment # to ;
 -- fs::cronjob         - update to mysqli driver
 -- db::mpi_gerate   - name nicht mehr unique

IF ( SELECT MAX(version) FROM dataface__version ) < '0970' THEN

  -- name in geraete nicht mehr unique
  ALTER TABLE mpi_geraete DROP INDEX name;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0970');
END IF;


-- CHANGES V0.9.71 - 2016-07-20 !!! MPG Version !!!
-- ****************************
 -- fs::mpi_geraete  - add field entsorgt
 -- db::mpi_gerate   - add field entsorgt

IF ( SELECT MAX(version) FROM dataface__version ) < '0971' THEN

  -- add field entsorgt
  ALTER TABLE mpi_geraete ADD entsorgt DATE NULL AFTER lieferdatum ;
  -- add view in reiter
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_licman_lizenz', 'View', 0, 0, 'Beziehung Bestellnummer zur DB Lizenzverwaltung') ;
  -- create view_licman_lizenz
  CREATE OR REPLACE VIEW view_licman_lizenz AS
   SELECT
    '000001' AS lizenzID,
    'software : modell : anzahl' AS bezeichnung,
    '0123456789' AS bestellnummer,
    'alone' AS bearbeiter,
    '2016-07-25 12:46:00' AS zeitstempel
  ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0971');
END IF;


-- CHANGES V0.9.72 - 2016-09-06
-- ****************************
 -- fs|db - add feature Protokollierung

IF ( SELECT MAX(version) FROM dataface__version ) < '0972' THEN

  -- list_protokoll - new table
  CREATE TABLE IF NOT EXISTS list_protokoll (
   protID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
   protokoll varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   gerID smallint(6) NOT NULL,
   bemerkung varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   PRIMARY KEY (protID),
   UNIQUE KEY prot_geraet (protokoll,gerID),
   KEY gerID (gerID)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Vorlagen Protokolle' ;

  ALTER TABLE list_protokoll ADD CONSTRAINT protokoll_geraete FOREIGN KEY (gerID) REFERENCES mpi_geraete (tabID) ;

  -- list_bereich - new table
  CREATE TABLE IF NOT EXISTS list_bereich (
   berID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
   bereich varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   PRIMARY KEY (berID),
   UNIQUE KEY bereich (bereich)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Liste Sequenzen Installation' ;

  INSERT IGNORE INTO `list_bereich` (`berID`, `bereich`) VALUES
   (000007, 'Auslieferung'),
   (000002, 'Betriebssystem'),
   (000005, 'Deaktivierung'),
   (000001, 'Prepare'),
   (000003, 'SoftwarePakete'),
   (000004, 'Update'),
   (000006, 'Verschrottung');

  -- list_sequenz - new table
  CREATE TABLE IF NOT EXISTS list_sequenz (
   seqID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
   bereich varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   sequenz varchar(60) COLLATE utf8_unicode_ci NOT NULL,
   beschreibung text COLLATE utf8_unicode_ci NOT NULL,
   PRIMARY KEY (seqID),
   UNIQUE KEY ber_seq (bereich,sequenz),
   KEY sequenz (sequenz)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Liste Sequenzen Installation' ;

  ALTER TABLE list_sequenz ADD CONSTRAINT bereich_bereich FOREIGN KEY (bereich) REFERENCES list_bereich (bereich) ON DELETE CASCADE ON UPDATE CASCADE ;

  -- tab_instProt - new table
  CREATE TABLE IF NOT EXISTS tab_instProt (
   inprID mediumint(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
   gerID smallint(6) NOT NULL,
   status tinyint(1) NOT NULL DEFAULT '0',
   protID smallint(6) unsigned zerofill NOT NULL,
   schritt varchar(5) COLLATE utf8_unicode_ci NOT NULL,
   bereich varchar(20) COLLATE utf8_unicode_ci NOT NULL,
   sequenz varchar(60) COLLATE utf8_unicode_ci NOT NULL,
   kommentar varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
   bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (inprID),
   UNIQUE KEY gerID_protID_schritt_seq (gerID,protID,schritt,sequenz),
   KEY gerID (gerID),
   KEY protID (protID),
   KEY sequenz (sequenz)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Protokoll Installation' ;

  ALTER TABLE tab_instProt
   ADD CONSTRAINT instProt_geraete FOREIGN KEY (gerID)   REFERENCES mpi_geraete (tabID)     ON DELETE CASCADE,
   ADD CONSTRAINT instProt_protID  FOREIGN KEY (protID)  REFERENCES list_protokoll (protID) ON UPDATE CASCADE ;

  -- mpi_geraete - add new field protID
  ALTER TABLE mpi_geraete CHANGE name name VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ;
  ALTER TABLE mpi_geraete CHANGE alias alias VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL ;
  ALTER TABLE mpi_geraete ADD protID SMALLINT(6) UNSIGNED ZEROFILL NULL AFTER status, ADD INDEX (protID);
  ALTER TABLE mpi_geraete ADD CONSTRAINT geraete_protID FOREIGN KEY (protID) REFERENCES list_protokoll (protID) ON UPDATE CASCADE ;

  -- list_reiter - add new entries
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES
   ('tab_instProt', 'Liste', 1, 0, 'Protokoll Abarbeitung Installationsanweisungen'),
   ('list_sequenz', 'Liste', 0, 0, 'Abschnittssequenz fuer Protokollinstallation'),
   ('list_protokoll', 'Liste', 0, 0, 'Vorlage Protokoll von einen Host aus Geraetetabelle'),
   ('list_bereich', 'Liste', 0, 0, 'Auswahl Protokollbereich fuer Sequenzschritte') ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0972');
END IF;


-- CHANGES V0.9.73 - 2016-10-14
-- ****************************
 -- fs|db - fixed geraete wartung

IF ( SELECT MAX(version) FROM dataface__version ) < '0973' THEN

  -- mpi_gerWartung
  ALTER TABLE mpi_gerWartung CHANGE autoID wartID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
  ALTER TABLE mpi_gerWartung DROP FOREIGN KEY mpi_gerWartung_ibfk_1;
  ALTER TABLE mpi_gerWartung DROP FOREIGN KEY mpi_gerWartung_ibfk_2;
  ALTER TABLE mpi_gerWartung CHANGE tabID gerID SMALLINT(6) NOT NULL;
  ALTER TABLE mpi_gerWartung ADD CONSTRAINT geraete_tabID FOREIGN KEY (gerID) REFERENCES mpi_geraete(tabID) ON DELETE CASCADE ON UPDATE RESTRICT;
  ALTER TABLE mpi_gerWartung ADD CONSTRAINT nachricht_nachricht FOREIGN KEY (nachricht) REFERENCES list_nachricht(nachricht) ON DELETE SET NULL ON UPDATE CASCADE;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('0973');

END IF;


-- CHANGES V0.9.80 - 2017-01-23
-- ****************************
-- UPDATE: Verbindung zwischen den Typen Material, Geraete und Systemen fuer einbaubare Teile in System
-- fs::rsync    - NEU con_mainTab, view_mainTab 
-- fs::rsync    - UPDATE mpi_geraete, mpi_arbeitsplatz, mpi_material 
-- db::rsync    - NEU con_mainTab, view_mainTab
-- db::rsync    - UPDATE mpi_geraete, mpi_arbeitsplatz, mpi_material 

IF ( SELECT MAX(version) FROM dataface__version ) < '0980' THEN

 -- create con_mainTab
  DROP TABLE IF EXISTS con_mainTab;
  CREATE TABLE con_mainTab (
   conID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
   auswTabS varchar(10) COLLATE utf8_unicode_ci NOT NULL,
   tabIDS smallint(6) NOT NULL,
   auswTabD varchar(10) COLLATE utf8_unicode_ci NOT NULL,
   tabIDD smallint(6) NOT NULL,
   used tinyint(1) NOT NULL DEFAULT '0',
   bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (conID),
   UNIQUE KEY ausw_tabID (auswTabS,tabIDS,auswTabD,tabIDD) USING BTREE
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

 -- create view_mainTab
  CREATE OR REPLACE VIEW view_mainTab AS
   SELECT 'System' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(seriennummer,'--')) AS name FROM mpi_arbeitsplatz
   UNION ALL
   SELECT 'Geraet' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(seriennummer,'--')) AS name FROM mpi_geraete
   UNION ALL
   SELECT 'Material' AS auswTab, tabID, CONCAT(name,' : ',IFNULL(eigenschaft,'--')) AS name FROM mpi_material
  ;

 -- update old relationships mpi_geraete
  INSERT IGNORE INTO con_mainTab (auswTabS,tabIDS,auswTabD,tabIDD,used,bearbeiter,zeitstempel) SELECT 'Geraet',tabID,'System',anlID,0,bearbeiter,zeitstempel FROM mpi_geraete WHERE anlID > 0;
  INSERT IGNORE INTO con_mainTab (auswTabS,tabIDS,auswTabD,tabIDD,used,bearbeiter,zeitstempel) SELECT 'Geraet',tabID,'Geraet',gerID,0,bearbeiter,zeitstempel FROM mpi_geraete WHERE gerID > 0;

 -- fixed double foreign keys
  ALTER TABLE mpi_arbeitsplatz DROP FOREIGN KEY IF EXISTS mpi_arbeitsplatz_ibfk_6;
  ALTER TABLE mpi_arbeitsplatz DROP FOREIGN KEY IF EXISTS mpi_arbeitsplatz_ibfk_7;
  ALTER TABLE mpi_arbeitsplatz DROP FOREIGN KEY IF EXISTS mpi_arbeitsplatz_ibfk_8;
  ALTER TABLE mpi_arbeitsplatz DROP FOREIGN KEY IF EXISTS mpi_arbeitsplatz_ibfk_9;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_11;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_12;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_13;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_14;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_15;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_16;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_17;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_18;
  ALTER TABLE mpi_material DROP FOREIGN KEY IF EXISTS mpi_material_ibfk_5;
  ALTER TABLE mpi_material DROP FOREIGN KEY IF EXISTS mpi_material_ibfk_6;
  ALTER TABLE mpi_material DROP FOREIGN KEY IF EXISTS mpi_material_ibfk_7;
  ALTER TABLE mpi_material DROP FOREIGN KEY IF EXISTS mpi_material_ibfk_8;

 -- delete old fields gerID, anlID
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_8;
  ALTER TABLE mpi_geraete DROP IF EXISTS gerID;
  ALTER TABLE mpi_geraete DROP FOREIGN KEY IF EXISTS mpi_geraete_ibfk_9;
  ALTER TABLE mpi_geraete DROP IF EXISTS anlID;
  CREATE TABLE IF NOT EXISTS mpi_geraete__history (tempID smallint(6)) ;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS gerID;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS anlID;

  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('con_mainTab', 'Zuordnung', 1, 0, 'Zuordnung von Ersatzteilen, Komponenten, Verbrauchsmaterial zu Hauptkomponenten');
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES('view_mainTab', 'Programmierung', 0, 0, 'Dynamischer Auswahlfilter fuer Anlage,Geraete und Material in depselect');

 -- loesche alten view
  DROP VIEW IF EXISTS view_geraete;
  DELETE FROM list_reiter WHERE reiter = 'view_geraete';

 -- add field seriennummer in material
  ALTER TABLE mpi_material ADD seriennummer VARCHAR(30) NULL AFTER lieferant;

 -- update seriennummer to NULL where empty
  UPDATE mpi_material SET seriennummer = NULL, zeitstempel = zeitstempel WHERE seriennummer = '';
  UPDATE mpi_geraete SET seriennummer = NULL, zeitstempel = zeitstempel WHERE seriennummer = '';
  UPDATE mpi_arbeitsplatz SET seriennummer = NULL, zeitstempel = zeitstempel WHERE seriennummer = '';

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0980');

END IF;


-- CHANGES V0.9.81 - 2017-01-29
-- ****************************
-- UPDATE: con_mainTab, view_mainTab
-- fs|db::rsync    - UPDATE con_mainTab, view_mainTab

IF ( SELECT MAX(version) FROM dataface__version ) < '0981' THEN

 -- prepare for new choice of table
  UPDATE con_mainTab SET auswTabS = 'sysID', zeitstempel = zeitstempel WHERE auswTabS = 'System';
  UPDATE con_mainTab SET auswTabD = 'sysID', zeitstempel = zeitstempel WHERE auswTabD = 'System';
  UPDATE con_mainTab SET auswTabS = 'gerID', zeitstempel = zeitstempel WHERE auswTabS = 'Geraet';
  UPDATE con_mainTab SET auswTabD = 'gerID', zeitstempel = zeitstempel WHERE auswTabD = 'Geraet';
  UPDATE con_mainTab SET auswTabS = 'matID', zeitstempel = zeitstempel WHERE auswTabS = 'Material';
  UPDATE con_mainTab SET auswTabD = 'matID', zeitstempel = zeitstempel WHERE auswTabD = 'Material';

 -- update view_mainTab
  CREATE OR REPLACE VIEW view_mainTab AS
   SELECT CONVERT('sysID' USING utf8) COLLATE utf8_unicode_ci AS auswTab, tabID AS tabID, name, seriennummer AS ident FROM mpi_arbeitsplatz
   UNION ALL
   SELECT CONVERT('gerID' USING utf8) COLLATE utf8_unicode_ci AS auswTab, tabID AS tabID, name, seriennummer AS ident FROM mpi_geraete
   UNION ALL
   SELECT CONVERT('matID' USING utf8) COLLATE utf8_unicode_ci AS auswTab, tabID AS tabID, name, eigenschaft AS ident FROM mpi_material
   UNION ALL
   SELECT CONVERT('medID' USING utf8) COLLATE utf8_unicode_ci AS auswTab, medID AS tabID, name, seriennummer AS ident FROM mpi_medien
  ;

 -- notiz
  ALTER TABLE `con_mainTab` ADD `notiz` VARCHAR(100) NULL AFTER `used`;

 -- list_reiter
  UPDATE list_reiter SET bedeutung = 'Dynamischer Auswahlfilter von Haupttabellen fuer widget:grid' WHERE reiter = 'view_mainTab';


 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0981');

END IF;


-- CHANGES V0.9.85 - 2017-01-27
-- ****************************
-- UPDATE: Alte Tabelle Parameter aus Geraete in Parameter-Tab ueberfuehren, interface aufraeumen
-- fs|db::rsync    - UPDATE mpi_geraete, interface 

IF ( SELECT MAX(version) FROM dataface__version ) < '0985' THEN

 -- exportiere alte parameter nach parameter tabelle und erhalte alte Feldnamen
  INSERT IGNORE INTO list_parTyp (typ) VALUES ('IP-Adresse'),('Mac-Adresse'),('NW-Interface'),('Speichermedium'),('RAM Speicher'),('CPU Taktfrequenz'),('CPU-Typ'),('lastFound'),('lastFoundM'),('Lizenz');
  INSERT IGNORE INTO list_einheit (einheit) VALUES ('IP-V4'),('IP-V6'),('SSD'),('HDD'),('OEM');

 -- reduziere typen und update interface tabelle
  -- mac address
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'Mac-Adresse'),parameter,einheit,notiz,bearbeiter,zeitstempel FROM mpi_parameter WHERE typID IN (SELECT typID FROM list_parTyp WHERE typ like 'Mac Adresse%');
  DELETE FROM mpi_parameter WHERE typID IN (SELECT typID FROM list_parTyp WHERE typ like 'Mac Adresse%');
  DELETE FROM list_parTyp WHERE typ LIKE 'Mac Adresse%';
  -- festplatten
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'Speichermedium'),parameter,einheit,notiz,bearbeiter,zeitstempel FROM mpi_parameter WHERE typID IN (SELECT typID FROM list_parTyp WHERE typ like 'Festplatte %');
  DELETE FROM mpi_parameter WHERE typID IN (SELECT typID FROM list_parTyp WHERE typ like 'Festplatte %');
  DELETE FROM list_parTyp WHERE typ LIKE 'Festplatte %';
  -- interface
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'NW-Interface'),parameter,einheit,notiz,bearbeiter,zeitstempel FROM mpi_parameter WHERE typID IN (SELECT typID FROM list_parTyp WHERE typ like 'Interface %');
  DELETE FROM mpi_parameter WHERE typID IN (SELECT typID FROM list_parTyp WHERE typ like 'Interface %');
  DELETE FROM list_parTyp WHERE typ LIKE 'Interface %';
  -- loese interface adressen auf und loesche inkonsistente daten
  ALTER TABLE interface DROP FOREIGN KEY IF EXISTS interface_ibfk_1;
  ALTER TABLE interface DROP FOREIGN KEY IF EXISTS interface_ibfk_2;
  DELETE FROM interface WHERE gerID IS NULL OR gerID = '';
  ALTER TABLE interface CHANGE gerID gerID SMALLINT(6) NOT NULL;
  ALTER TABLE interface CHANGE tabID tabID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
  ALTER TABLE interface CHANGE name name VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
  ALTER TABLE interface CHANGE address address VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
  ALTER TABLE interface ADD CONSTRAINT interface_geraete FOREIGN KEY (gerID) REFERENCES mpi_geraete (tabID) ON DELETE CASCADE;
  -- sichere anderes als IP nach interface address
  INSERT IGNORE INTO interface (name,address,gerID,bearbeiter,zeitstempel) SELECT 'import',adresse,tabID,bearbeiter,zeitstempel FROM mpi_geraete WHERE NOT adresse REGEXP '^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9a-zA-Z]{1,3}$' AND adresse IS NOT NULL AND NOT adresse = '';
  -- sichere moegliche ip's
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'IP-Adresse'),address,'IP-V4','update V0985',bearbeiter,zeitstempel FROM interface WHERE address REGEXP '^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9a-zA-Z]{1,3}$';
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'IP-Adresse'),ipV4Adresse,'IP-V4','update V0985',bearbeiter,zeitstempel FROM interface WHERE ipV4Adresse REGEXP '^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9a-zA-Z]{1,3}$';
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'IP-Adresse'),ipV6Adresse,'IP-V6','update V0985',bearbeiter,zeitstempel FROM interface WHERE ipV6Adresse IS NOT NULL AND NOT ipV6Adresse = '';
  -- portiere hw-adresse 
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,notiz,bearbeiter,zeitstempel) SELECT gerID,(SELECT typID FROM list_parTyp WHERE typ = 'MAC-Adresse'),hwAdresse,'update V0985',bearbeiter,zeitstempel FROM interface WHERE hwAdresse IS NOT NULL AND NOT hwAdresse = '';
  -- loesche alte felder
  ALTER TABLE interface DROP IF EXISTS ipV4Adresse;
  ALTER TABLE interface DROP IF EXISTS ipV6Adresse;
  ALTER TABLE interface DROP IF EXISTS hwAdresse;

 -- export speicher
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'RAM Speicher'),speichergroesse,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE speichergroesse IS NOT NULL AND NOT speichergroesse = '' AND NOT speichergroesse = '0';

 -- export festplatte
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'Speichermedium'),festplatte,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE festplatte IS NOT NULL AND NOT festplatte = '' AND NOT speichergroesse = '0';

 -- export taktfrequenz
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'CPU Taktfrequenz'),taktfrequenz,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE taktfrequenz IS NOT NULL AND NOT taktfrequenz = '' AND NOT taktfrequenz = '0';

 -- export prozessor
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'CPU Typ'),prozessor,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE prozessor IS NOT NULL AND NOT prozessor = '' AND NOT prozessor = 'unbekannt';

 -- export interface
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'NW-Interface'),interface,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE interface IS NOT NULL AND NOT interface = '';

 -- export ip adressen
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'IP-Adresse'),adresse,'IP-V4','update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE adresse REGEXP '^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9a-zA-Z]{1,3}$';

 -- export lastFound
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'lastFound'),lastFound,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE lastFound IS NOT NULL AND NOT lastFound = '';
  INSERT IGNORE INTO mpi_parameter (gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT tabID,(SELECT typID FROM list_parTyp WHERE typ = 'lastFoundM'),lastFoundM,NULL,'update V0985',bearbeiter,zeitstempel FROM mpi_geraete WHERE lastFoundM IS NOT NULL AND NOT lastFoundM = '';


 -- loesche alte param felder
  ALTER TABLE mpi_geraete DROP IF EXISTS speichergroesse;
  ALTER TABLE mpi_geraete DROP IF EXISTS festplatte;
  ALTER TABLE mpi_geraete DROP IF EXISTS taktfrequenz;
  ALTER TABLE mpi_geraete DROP IF EXISTS prozessor;
  ALTER TABLE mpi_geraete DROP IF EXISTS interface;
  ALTER TABLE mpi_geraete DROP IF EXISTS adresse;
  ALTER TABLE mpi_geraete DROP IF EXISTS lastFound;
  ALTER TABLE mpi_geraete DROP IF EXISTS lastFoundM;
 -- loesche alte felder in history geraete
  CREATE TABLE IF NOT EXISTS mpi_geraete__history (tempID smallint(6));
  ALTER TABLE mpi_geraete__history DROP IF EXISTS lastFoundM;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS lastFound;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS adresse;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS interface;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS prozessor;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS taktfrequenz;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS festplatte;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS speichergroesse;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS manager;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS forschungsbereich;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS spezifikation;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS minBereich;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS maxBereich;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS medium;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS grenzwert;
  ALTER TABLE mpi_geraete__history DROP IF EXISTS netzwerk;
    

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0985');

END IF;


-- CHANGES V0.9.86 - 2017-01-31
-- ****************************
-- NEw: Alte Parameter aus mpi_parameter nach neue Parameter-Tab ueberfuehren
-- fs|db::rsync    - NEW tab_parameter

IF ( SELECT MAX(version) FROM dataface__version ) < '0986' THEN

 -- new parameter table
  DROP TABLE IF EXISTS tab_parameter;
  CREATE TABLE tab_parameter (
   parID smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
   auswTab varchar(10) COLLATE utf8_unicode_ci NOT NULL,
   tabID smallint(6) UNSIGNED NOT NULL,
   typID smallint(6) UNSIGNED ZEROFILL NOT NULL,
   parameter varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
   einheit varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   notiz varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (parID),
   UNIQUE KEY ausw_tab_typ_par_einh_note (auswTab,tabID,typID,parameter,einheit,notiz),
   KEY typID (typID),
   KEY einheit (einheit)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
  ALTER TABLE tab_parameter ADD CONSTRAINT param_einheit FOREIGN KEY (einheit) REFERENCES list_einheit (einheit) ON DELETE RESTRICT ON UPDATE CASCADE;
  ALTER TABLE tab_parameter ADD CONSTRAINT param_typ FOREIGN KEY (typID) REFERENCES list_parTyp (typID) ON DELETE RESTRICT ON UPDATE RESTRICT;

 -- copy old values to new table
  INSERT IGNORE INTO tab_parameter (auswTab,tabID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT 'medID',medID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel FROM mpi_parameter WHERE medID > 0;
  INSERT IGNORE INTO tab_parameter (auswTab,tabID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel) SELECT 'gerID',gerID,typID,parameter,einheit,notiz,bearbeiter,zeitstempel FROM mpi_parameter WHERE gerID > 0;

 -- clear old table
  DROP TABLE IF EXISTS mpi_parameter;
  DROP TABLE IF EXISTS mpi_parameter__history;

 -- list_reiter
  UPDATE list_reiter SET reiter = 'tab_parameter', favorit = 0, history = 0 WHERE reiter = 'mpi_parameter';

 -- unique name + sn - fake duplicates vorher und fuege unique tabID hinzu
  -- anlage
  CREATE OR REPLACE VIEW view_dupli AS
   SELECT tabID, count(tabID) AS cnt, name, seriennummer FROM mpi_arbeitsplatz WHERE name = name AND seriennummer = seriennummer GROUP BY name, seriennummer;
  DROP TABLE IF EXISTS temp_tabDup;
  CREATE TABLE temp_tabDup (tabID smallint(6) UNSIGNED NOT NULL);
  INSERT INTO temp_tabDup (tabID) SELECT sys.tabID FROM view_dupli AS dup LEFT JOIN mpi_arbeitsplatz AS sys ON dup.name = sys.name AND dup.seriennummer = sys.seriennummer WHERE dup.cnt > 1;
  UPDATE mpi_arbeitsplatz SET seriennummer = CONCAT('[',tabID,'] ',seriennummer,' [Dublikat]'), zeitstempel = zeitstempel WHERE tabID IN (SELECT tabID FROM temp_tabDup);
 ALTER TABLE mpi_arbeitsplatz ADD UNIQUE IF NOT EXISTS name_sn (name, seriennummer);
  -- geraete
  CREATE OR REPLACE VIEW view_dupli AS
   SELECT tabID, count(tabID) AS cnt, name, seriennummer FROM mpi_geraete WHERE name = name AND seriennummer = seriennummer GROUP BY name, seriennummer;
  DROP TABLE IF EXISTS temp_tabDup;
  CREATE TABLE temp_tabDup (tabID smallint(6) UNSIGNED NOT NULL);
  INSERT INTO temp_tabDup (tabID) SELECT ger.tabID FROM view_dupli AS dup LEFT JOIN mpi_geraete AS ger ON dup.name = ger.name AND dup.seriennummer = ger.seriennummer WHERE dup.cnt > 1;
  UPDATE mpi_geraete SET seriennummer = CONCAT('[',tabID,'] ',seriennummer,' [Dublikat]'), zeitstempel = zeitstempel WHERE tabID IN (SELECT tabID FROM temp_tabDup);
  ALTER TABLE mpi_geraete ADD UNIQUE  IF NOT EXISTS name_sn (name, seriennummer);
  -- material
  CREATE OR REPLACE VIEW view_dupli AS
   SELECT tabID, count(tabID) AS cnt, name, eigenschaft FROM mpi_material WHERE name = name AND eigenschaft = eigenschaft GROUP BY name, eigenschaft;
  DROP TABLE IF EXISTS temp_tabDup;
  CREATE TABLE temp_tabDup (tabID smallint(6) UNSIGNED NOT NULL);
  INSERT INTO temp_tabDup (tabID) SELECT mat.tabID FROM view_dupli AS dup LEFT JOIN mpi_material AS mat ON dup.name = mat.name AND dup.eigenschaft = mat.eigenschaft WHERE dup.cnt > 1;
  UPDATE mpi_material SET eigenschaft = CONCAT('[',tabID,'] ',eigenschaft,' [Dublikat]'), zeitstempel = zeitstempel WHERE tabID IN (SELECT tabID FROM temp_tabDup);
  ALTER TABLE mpi_material ADD UNIQUE IF NOT EXISTS name_sn (name, eigenschaft);
  -- temps loeschen
  DROP VIEW IF EXISTS view_dupli;
  DROP TABLE IF EXISTS temp_tabDup;



 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0986');

END IF;


-- CHANGES V0.9.87 - 2017-02-02
-- ****************************
-- UPDATE: dateiablage entsprechend view_mainTab verbessern
-- db|fs::tab_ablage       - UPDATE Referenzierung flexibler gestalten
-- db|fs::con_ablage       - NEW Referenzierung flexibler gestalten
-- db|fs::list_kategorie   - NEW Kategorie fuer Ablagen

IF ( SELECT MAX(version) FROM dataface__version ) < '0987' THEN

 -- fix con_mainTab
  ALTER TABLE con_mainTab CHANGE tabIDD tabIDD SMALLINT(6) UNSIGNED NOT NULL;
  ALTER TABLE con_mainTab CHANGE tabIDS tabIDS SMALLINT(6) UNSIGNED NOT NULL;

  -- add table list_kategorie
  CREATE TABLE IF NOT EXISTS list_kategorie (
    katID smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
    kategorie varchar(30) COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (katID),
    UNIQUE KEY kategorie (kategorie)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

 -- new file ablage table
  DROP TABLE IF EXISTS tab_ablage;
  CREATE TABLE tab_ablage (
   ablID smallint(6)  UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
   kategorie varchar(30) COLLATE utf8_unicode_ci NOT NULL,
   `file` longblob,
   file_mimetype varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
   file_filename varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
   notiz varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   bearbeiter varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
   zeitstempel timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (ablID),
   KEY kategorie (kategorie)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
  ALTER TABLE tab_ablage ADD CONSTRAINT `ablage_kategorie` FOREIGN KEY (kategorie) REFERENCES list_kategorie (kategorie) ON DELETE RESTRICT ON UPDATE CASCADE;

 -- new connect table - flex erweiterung verbindung ablgen zu tabellen
 CREATE TABLE IF NOT EXISTS con_ablage (
   conID smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
   auswTab varchar(10) COLLATE utf8_unicode_ci NOT NULL,
   tabID smallint(6) UNSIGNED NOT NULL,
   ablID smallint(6) UNSIGNED ZEROFILL NOT NULL,
   PRIMARY KEY (conID),
   KEY ablID (ablID),
   UNIQUE KEY ablage_tab (auswTab,tabID,ablID)
 ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
 ALTER TABLE con_ablage ADD CONSTRAINT ablage_ablage FOREIGN KEY (ablID) REFERENCES tab_ablage (ablID) ON DELETE RESTRICT ON UPDATE RESTRICT;

  -- fill table with new and old values
  INSERT IGNORE INTO list_kategorie (kategorie) VALUES ('Angebot'), ('Auftragsbestätigung'),('Betriebsanweisung'),('Lieferant'),('Preisliste'),('Technische Information'),('Zertifikat'),('Lizenz');
  INSERT IGNORE INTO list_kategorie (kategorie) SELECT DISTINCT vorgang FROM mpi_ablage;

 -- list_reiter
  UPDATE list_reiter SET reiter = 'tab_ablage', favorit = 0, history = 0 WHERE reiter = 'mpi_ablage';
  UPDATE list_reiter SET favorit = 0 WHERE reiter = 'mpi_medien';
  INSERT IGNORE INTO list_reiter (reiter,kategorie,favorit,history,bedeutung) VALUES('con_ablage', 'Zuordnung', 1, 0, 'Verlinkung Haupttabellen mit Dateiablage');
  INSERT IGNORE INTO list_reiter (reiter,kategorie,favorit,history,bedeutung) VALUES ('list_kategorie', 'Liste', 0, 0, 'Auswahl Kategorie fuer Dateiablage');

 -- copy old values to new tables
  INSERT IGNORE INTO tab_ablage (ablID,kategorie,`file`,file_mimetype,file_filename,notiz,bearbeiter,zeitstempel) SELECT ablageID,vorgang,`file`,file_mimetype,file_filename,notiz,bearbeiter,zeitstempel FROM mpi_ablage WHERE sysID > 0 OR gerID > 0 OR matID > 0 OR medID > 0;
 -- insert original ablageID's and source table from ablage to con
  INSERT INTO con_ablage (auswTab,tabID,ablID) SELECT 'sysID',sysID,ablageID FROM mpi_ablage WHERE sysID > 0;
  INSERT INTO con_ablage (auswTab,tabID,ablID) SELECT 'gerID',gerID,ablageID FROM mpi_ablage WHERE gerID > 0;
  INSERT INTO con_ablage (auswTab,tabID,ablID) SELECT 'matID',matID,ablageID FROM mpi_ablage WHERE matID > 0;
  INSERT INTO con_ablage (auswTab,tabID,ablID) SELECT 'medID',medID,ablageID FROM mpi_ablage WHERE medID > 0;

 -- clear old table
  DROP TABLE IF EXISTS mpi_ablage;
  DROP TABLE IF EXISTS mpi_ablage__history;

 -- copy old ablage links ablageID und gerID new one
  INSERT INTO con_ablage (auswTab,tabID,ablID) SELECT 'medID',medID,ablageID FROM mpi_medien WHERE ablageID > 0;
  INSERT INTO con_mainTab (auswTabS,tabIDS,auswTabD,tabIDD) SELECT 'medID',medID,'gerID',gerID FROM mpi_medien WHERE gerID > 0;
  ALTER TABLE mpi_medien DROP IF EXISTS ablageID; 
  ALTER TABLE mpi_medien DROP FOREIGN KEY If EXISTS mpi_medien_gerID;
  ALTER TABLE mpi_medien DROP IF EXISTS gerID; 
  CREATE TABLE IF NOT EXISTS mpi_medien__history (tempID smallint(6));
  ALTER TABLE mpi_medien__history DROP IF EXISTS ablageID;
  ALTER TABLE mpi_medien__history DROP IF EXISTS gerID;

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('0987');

END IF;


-- CHANGES V0.9.88 - 2017-02-04
-- ****************************
-- UPDATE: change tabID zu UNSIGNED ZEROFILL
-- db|fs::mpi_arbeitsplatz - change tabID zu UNSIGNED ZEROFILL
-- db|fs::mpi_material     - change tabID zu UNSIGNED ZEROFILL

IF ( SELECT MAX(version) FROM dataface__version ) < '0988' THEN

 -- mpi_arbeitsplatz
  ALTER TABLE mpi_notiz        DROP FOREIGN KEY IF EXISTS mpi_notiz_sysID ;
  ALTER TABLE mpi_notiz        CHANGE IF EXISTS sysID sysID SMALLINT(6) UNSIGNED ZEROFILL ;
  ALTER TABLE mpi_arbeitsplatz CHANGE IF EXISTS tabID tabID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;
  ALTER TABLE mpi_notiz        ADD CONSTRAINT notiz_system FOREIGN KEY IF NOT EXISTS (sysID) REFERENCES mpi_arbeitsplatz (tabID) ON DELETE SET NULL ON UPDATE RESTRICT ;

 -- mpi_material
  ALTER TABLE mpi_notiz          DROP FOREIGN KEY IF EXISTS mpi_notiz_matID ;
  ALTER TABLE mpi_notiz          CHANGE IF EXISTS matID matID SMALLINT(6) UNSIGNED ZEROFILL ;
  ALTER TABLE mpi_matMengenfluss DROP FOREIGN KEY If EXISTS mpi_matMengenfluss_ibfk_3 ;
  ALTER TABLE mpi_matMengenfluss DROP FOREIGN KEY If EXISTS mpi_matMengenfluss_ibfk_6 ;
  ALTER TABLE mpi_matMengenfluss DROP FOREIGN KEY If EXISTS mpi_matMengenfluss_ibfk_7 ;
  ALTER TABLE mpi_matMengenfluss DROP FOREIGN KEY If EXISTS mpi_matMengenfluss_ibfk_8 ;
  ALTER TABLE mpi_matMengenfluss DROP FOREIGN KEY If EXISTS mpi_matMengenfluss_ibfk_9 ;
  ALTER TABLE mpi_matMengenfluss CHANGE IF EXISTS tabID  matID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL ;
  ALTER TABLE mpi_matMengenfluss CHANGE IF EXISTS autoID fluID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;
  ALTER TABLE mpi_material       CHANGE IF EXISTS tabID  tabID SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ;
  ALTER TABLE mpi_notiz          ADD CONSTRAINT notiz_material FOREIGN KEY IF NOT EXISTS (matID) REFERENCES mpi_material (tabID) ON DELETE SET NULL ON UPDATE RESTRICT ;
  ALTER TABLE mpi_matMengenfluss ADD CONSTRAINT fluss_material FOREIGN KEY IF NOT EXISTS (matID) REFERENCES mpi_material (tabID) ON DELETE CASCADE ON UPDATE RESTRICT ;

 -- menge wird nur noch dynamisch berechnet
  ALTER TABLE mpi_material DROP IF EXISTS menge;
  CREATE TABLE IF NOT EXISTS mpi_material__history (tempID smallint(6));
  ALTER TABLE mpi_material__history DROP IF EXISTS menge;

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('0988') ;

END IF;


-- CHANGES V1.0.00 - 2017-11-09
-- ****************************
-- UPDATE: add script import parameter computer from empirum DB
-- UPDATE: change parID zu MEDIUMINT
-- db|fs::tab_parameter - change parID zu MEDIUMINT UNSIGNED ZEROFILL
-- fs::cronjobs/getInvEmpirum.php - add script import parameter computer from empirum DB

IF ( SELECT MAX(version) FROM dataface__version ) < '1000' THEN

 -- wegen Parameter import von Empirum parID vergroessern
  ALTER TABLE tab_parameter CHANGE parID parID MEDIUMINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1000') ;

END IF;


-- CHANGES V1.0.01 - 2017-12-11
-- ****************************
-- UPDATE: add parameter letzte = naechste in Wartungsintervall
-- fs::mpi_gerWartung - add valuelist parameter 'letzte = naechste'

IF ( SELECT MAX(version) FROM dataface__version ) < '1001' THEN

 -- passe Intervall-Eintraege dem neuen Wert an
  UPDATE mpi_gerWartung  SET intervall = '00', zeitstempel = zeitstempel WHERE intervall = '01';

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1001') ;

END IF;


-- CHANGES V1.0.02 - 2018-04-20
-- ****************************
-- UPDATE: add parameter letzte = naechste in Wartungsintervall
-- db::list_reiter - gleichschaltung aller db's auf eine ID
-- fs::valuelists.ini

IF ( SELECT MAX(version) FROM dataface__version ) < '1002' THEN

 -- change autoincrement
  ALTER TABLE list_reiter CHANGE IF EXISTS `tabID` `autoID` SMALLINT(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
  CREATE OR REPLACE VIEW view_favorit AS SELECT * FROM list_reiter WHERE favorit = '1' ORDER BY reiter;

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1002') ;

END IF;


-- CHANGES V1.1.00 - 2018-08-14
-- ****************************
-- UPDATE: change Autorisierung von mpi_user nach sys_user mit Rollentabelle
-- db::list_reiter - Aenderung tabellen anpassen
-- fs::sys_user,list_rolle - Anpassung neue Benutzerverwaltung

IF ( SELECT MAX(version) FROM dataface__version ) < '1100' THEN

  -- create table list_role
    -- DROP TABLE IF EXISTS `list_role`;
    CREATE TABLE IF NOT EXISTS `list_role` (
      `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
      `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- insert default roles
    INSERT IGNORE INTO `list_role` (`rolID`, `role`, `description`) VALUES
    (000001, 'NO ACCESS', 'No_Access'),
    (000002, 'READ ONLY', 'view, list, calendar, view xml, show all, find, navigate'),
    (000003, 'EDIT', 'READ_ONLY and edit, new record, remove, import, translate, copy'),
    (000004, 'DELETE', 'EDIT and delete and delete found'),
    (000005, 'OWNER', 'DELETE except navigate, new, and delete found'),
    (000006, 'REVIEWER', 'READ_ONLY and edit and translate'),
    (000007, 'USER', 'READ_ONLY and add new related record'),
    (000008, 'ADMIN', 'DELETE and xml_view'),
    (000009, 'MANAGER', 'ADMIN and manage, manage_migrate, manage_build_index, and install');
    -- unique, pri
    ALTER TABLE `list_role` ADD PRIMARY KEY IF NOT EXISTS (`rolID`), ADD UNIQUE KEY IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `list_role` MODIFY `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

    -- create table sys_user
    -- DROP TABLE IF EXISTS `sys_user`;
    CREATE TABLE IF NOT EXISTS `sys_user` (
     `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
     `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
     `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
     `bearbeiter` varchar(20) COLLATE utf8_unicode_ci NULL,
     `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- unique, pri
    ALTER TABLE `sys_user`
     ADD PRIMARY KEY IF NOT EXISTS (`logID`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `login` (`login`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `email` (`email`),
     ADD KEY         IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `sys_user` MODIFY `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
    -- Constraint
    ALTER TABLE `sys_user` ADD CONSTRAINT `sysUser_listRole` FOREIGN KEY IF NOT EXISTS (`role`) REFERENCES `list_role` (`role`);

    -- mpg-version ohne externe DB (licman,inv,gfk,chem,user) - Auslieferzustand
    CREATE OR REPLACE VIEW view_user AS
     SELECT
      '000001' AS userID, 'mpg_local' AS login, 'MPG, Version (mpg_local)' AS sort
     ;

    -- create if not exist list_reiter
    CREATE TABLE IF NOT EXISTS `list_reiter` (
     `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
     `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `favorit` tinyint(1) NOT NULL DEFAULT '0',
     `history` tinyint(1) NOT NULL DEFAULT '0',
     `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `reiter` (`reiter`),
     KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=9 ;

    -- create if not exist list_katReiter
    CREATE TABLE IF NOT EXISTS `list_katReiter` (
     `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;
    -- add entries in list_katReiter
    UPDATE list_katReiter SET kategorie = 'Autorisierung' WHERE kategorie = 'Authorisierung';
    INSERT IGNORE INTO `list_katReiter` (`kategorie`) VALUES ('Autorisierung');

    -- add entries in list_reiter
    INSERT IGNORE INTO `list_reiter` (`reiter`, `kategorie`, `favorit`, `history`, `bedeutung`) VALUES
     ('sys_user', 'Autorisierung', 1, 1, 'Autorisierung und Berechtigung Benutzer'),
     ('list_role', 'Autorisierung', 1, 0, 'Liste aller Berechtigungen (Rollen)');
    UPDATE `list_reiter` SET `bedeutung` = 'Auswahlliste fuer aktive und nicht abgelaufene Benutzer' WHERE `reiter` = 'view_user';

    -- copy inserts from old mpi_users
    INSERT IGNORE INTO sys_user (login, password, role, email, bearbeiter, zeitstempel) SELECT username, password, role, email, 'import', zeitstempel FROM mpi_users;

    -- del old table mpi_users (if all done and work)
    DROP TABLE IF EXISTS `mpi_users`;
    DROP TABLE IF EXISTS `mpi_users__history`;
    DELETE FROM `list_reiter` WHERE `reiter` = 'mpi_users';

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1100') ;

END IF;


END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();

-- weil nur noch unter mariadb funktioniert, sonst kein import der DB fuer mysqldb moeglich
DROP PROCEDURE IF EXISTS proc_update;


-- trigger duerfen nicht innerhalb einer procedur ausgefuehrt werden, deshalbe hier und jedesmal separat ausgefuehrt
-- CHANGES V0.9.87 - 2017-02-02
-- NEw: Trigger fuer verwaiste Referenzen nach delete
-- db::trigger    - NEW trigger mpi_arbeitsplatz, mpi_geraete, mpi_material, mpi_medien ON tab_parameter, con_mainTab, tab_ablage
 -- trigger system
  DROP TRIGGER IF EXISTS `sys_after_del`;
  DELIMITER $$
  CREATE TRIGGER `sys_after_del` AFTER DELETE ON mpi_arbeitsplatz FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'sysID' AND tabID  = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'sysID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'sysID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'sysID' AND tabID  = OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END
   $$
   DELIMITER ;
 -- trigger geraete
  DROP TRIGGER IF EXISTS `ger_after_del`;
  DELIMITER $$
  CREATE TRIGGER `ger_after_del` AFTER DELETE ON mpi_geraete FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'gerID' AND tabID =  OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'gerID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'gerID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'gerID' AND tabID =  OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END
   $$
   DELIMITER ;
 -- trigger material
  DROP TRIGGER IF EXISTS `mat_after_del`;
  DELIMITER $$
  CREATE TRIGGER `mat_after_del` AFTER DELETE ON mpi_material FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab  = 'matID' AND tabID =  OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabS = 'matID' AND tabIDS = OLD.tabID;
    DELETE FROM con_mainTab   WHERE auswTabD = 'matID' AND tabIDD = OLD.tabID;
    DELETE FROM con_ablage    WHERE auswTab  = 'matID' AND tabID =  OLD.tabID;
    DELETE FROM mpi_notiz     WHERE sysID IS NULL AND gerID IS NULL AND matID IS NULL;
   END
   $$
   DELIMITER ;
 -- trigger medien
  DROP TRIGGER IF EXISTS `med_after_del`;
  DELIMITER $$
  CREATE TRIGGER `med_after_del` AFTER DELETE ON mpi_medien FOR EACH ROW
   BEGIN
    DELETE FROM tab_parameter WHERE auswTab = 'medID' AND tabID = OLD.medID;
    DELETE FROM con_ablage    WHERE auswTab = 'medID' AND tabID = OLD.medID;
   END
   $$
   DELIMITER ;

