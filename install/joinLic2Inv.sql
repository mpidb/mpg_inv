-- Aenderungen fuer Weitergabe einer global gueltigen DB mit Kopplung mpg_licman <-> mpg_inv
-- seperat auszufuehrende sql-datei
-- alle views zwischen den DB's sollten vorher als fake existieren, damit auf dem Filesytem nichts mehr geaendert werden muss
-- view_pc (mpg_licman), view_it_licman_instInv (mpg_inv)

USE mpidb_mpg_inv;
-- version mpg mit verlinkung ueber pc-name
CREATE OR REPLACE VIEW mpidb_mpg_licman.view_pc AS
 SELECT
  tabID, name, art
 FROM
  mpidb_mpg_inv.mpi_geraete
 ORDER BY
  name
;

-- version mpg mit verlinkung ueber bestellnummer
CREATE OR REPLACE VIEW mpidb_mpg_licman.view_inv_geraete AS
 SELECT
  tabID AS gerID,
  name,
  os,
  lagerort,
  status,
  bestellnummer,
  bearbeiter,
  zeitstempel
 FROM
  mpidb_mpg_inv.mpi_geraete WHERE bestellnummer IS NOT NULL AND bestellnummer != ''
;

