## Einleitung ##

Diese Inventardatenbank ist mal speziell fuer den IT-Bereich entwickelt worden. Es existieren hier deshalb Tabellen mit denen der eine oder andere wohl nicht anfangen kann. Beispiel waeren da Portverbindungen zwischen Switche. Mittlerweile ist sie globaler gestaltet und kann auch fuer andere Bereiche genutzt werden.<br>
<br>
Was beinhaltet diese:
* Geraetetabelle (PC,BS;Note,Switche etc.)
* Wartungsplaene mit Emailbenachrichtigung
* Systemtabelle (kann mehrere Geraete vereinen)
* Verbrauchsmaterial mit Mindestmengen-Benachrichtigung
* Medientabelle (Lifecycle-Managment fuer Speicherhardware)
* Ausleihtabelle fuer Geraete
* Dateiabage fuer alle Haupttabellen
* Protokolle fuer sich wiederholenden Prozessen (z.B. Neubetankung von Rechnern)
* Inventarisierung mit Import-Abgleich aus SAP(MPG-spezifisch)
* Parametertabelle fuer alle Hauptbereiche mit Import-Moeglichkeit aus anderen DB (z.B. Empirum)

## Projekt downloaden ##

**Evtl. vorh. DB und Filesystem vorher sichern**<br>
<br>
Wenn noch keine andere Datenbank existiert, ist es empfehlenswert die Full-Version des tar-Files herunterzuladen. 
Ansonsten von common/db_export das tar-File herunterladen oder per Befehl
~~~bash
git clone https://gitlab.mpcdf.mpg.de/mpidb/mpg_inv.git
~~~
in das Wurzelverzeichnis des Webserver klonen.

## Installation ##

siehe LIEMICH.txt in Folder install<br>
Dort liegen auch alle noch notwendigen SQL-Skripte fuer die DB-Erstellung.
Versionupdates koennen ueber install/updateDB_<version>.sql installiert werden. Allerdings ab der Version 0980 nur noch fuer MariaDB moeglich!

## Screenshot ##

<a href="install/db_inv.png" title="Ueberblick Datenbanken"><img src="install/db_inv.png" align="left" height="100" width="100"></a>
<br><br><br><br>

## Lizenzbedingungen ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.<br>
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>  
See the GNU General Public License for more details.<br>
See also the file LICENSE.txt here
